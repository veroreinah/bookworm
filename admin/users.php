<?php
session_start();
require_once '../utilidades/filter.php';
require_once '../utilidades/GestionUsuarios.php';
require_once '../utilidades/clases/Usuario.php';

$usuarios = array();
$usuarios = GestionUsuarios::recuperarUsuarios();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Usuarios - Panel de administración de BookWorm</title>
        <?php require_once '../parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/jquery.pstrength-min.1.2.js"></script>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/user-validation.js"></script>
        <script type="text/javascript">
            $(function() {
                $('#input03').pstrength();
            });
            
            jQuery("#remove01").live("click",function(){
                var idUsuario = jQuery(this).parent().parent().children().first().children().attr("class");
                var nombreUsuario = jQuery(this).parent().parent().children().first().children().html();
                var email = jQuery(this).parent().parent().children().first().next().children().html();
                
                jQuery("#nombre").html(nombreUsuario);
                jQuery("#email").children().first().html(email);
                jQuery("#removeBtn").attr("href", "../forms/e_users.php?id=" + idUsuario);
                
                $('#removeModal').modal({
                    backdrop: "static",
                    show: true
                });
            });
            
            jQuery(document).ready(function() {
                <?php if ($usuarios != 0) { ?>
                    jQuery('#users_table').dataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "oLanguage": {
                            "sUrl": "../dataTableUtils/spanish"
                        },
                        "aoColumnDefs": [ 
                            { "bSortable": false, "aTargets": [ 2, 3 ] }
                        ],
                        "sPaginationType": "full_numbers",
                        "sAjaxSource": "../dataTableUtils/users_processing.php"
                    } );
                <?php } ?>
                    
                $.datepicker.setDefaults($.datepicker.regional["es"]);
                jQuery('#input05').datepicker({
                    yearRange: "c-100:c",
                    maxDate: "c",
                    changeMonth: true,
                    changeYear: true
		});
                
                jQuery('#file').customFileInput({
                    feedback_text: 'Elige una imagen',
                    button_text: '...',
                    button_change_text: '...'
                });
            });
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "admin/users.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <!-- MODAL - ELIMINAR -->
                    <div id="removeModal" class="modal hide fade" >
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal">×</a>
                            <h3>¿Desea eliminar este usuario?</h3>
                        </div>
                        <div class="modal-body">
                            <p id="nombre"></p>
                            <p id="email"><em></em></p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
                            <a id="removeBtn" href="" class="btn"><i class="icon-remove"></i> Eliminar</a>
                        </div> 
                    </div>
                    
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Nuevo usuario</a></li>
                            <li><a href="#2" data-toggle="tab">Todos los usuarios</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="1">
                                <?php if (isset($_SESSION["insertada"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo $_SESSION["insertada"];
                                        session_unregister("insertada");
                                        ?>
                                    </div>
                                    <?php
                                }
                                if (isset($_SESSION["error"])) {
                                    ?>
                                    <div class="alert alert-error">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo "<strong>" . $_SESSION["error"] . "</strong>";
                                        session_unregister("error");
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php if (isset($_SESSION["eliminada"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo $_SESSION["eliminada"];
                                        session_unregister("eliminada");
                                        ?>
                                    </div>
                                <?php } ?>
                                <?php require_once '../parts/reg.php'; ?>
                            </div>
                            <div class="tab-pane" id="2">
                                <?php if ($usuarios == 0) { ?>
                                    <div class="alert alert-error">
                                        <?php
                                        echo "<strong>" . $_SESSION["noHay"] . "</strong>";
                                        session_unregister("noHay");
                                        ?>
                                    </div>
                                <?php } else { ?>
                                    <table id="users_table" class="table">
                                        <thead>
                                            <tr>
                                                <th id="main-column-user">Nombre de usuario</th>
                                                <th id="main-column-mail">Email</th>
                                                <th>Detalles</th>
                                                <th>Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>

            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>