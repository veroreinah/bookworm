<?php
session_start();
require_once '../utilidades/filter.php';
require_once '../utilidades/GestionCitas.php';
require_once '../utilidades/GestionAutores.php';
require_once '../utilidades/clases/Cita.php';
require_once '../utilidades/clases/Autor.php';

$citas = array();
$citas = GestionCitas::recuperarCitas();

$autores = array();
$autores = GestionAutores::recuperarAutores();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Citas - Panel de administración de BookWorm</title>
        <?php require_once '../parts/head.php'; ?>
        <script>
            jQuery("#pencil01").live("click",function(){
                // Cita
                // attr -> información del span
                var idCita = jQuery(this).parent().parent().children().first().children().attr("class");
                // html -> contenido del span
                var textoCita = jQuery(this).parent().parent().children().first().children().html();
                // Autor
                var idAutor = jQuery(this).parent().parent().children().first().next().children().attr("class");
                var nombreAutor = jQuery(this).parent().parent().children().first().next().children().html();
                
                jQuery("#idM").val(idCita);
                jQuery("#textareaM").val(textoCita);
                jQuery("#input01idM").val(idAutor);
                jQuery("#input01M").val(nombreAutor);
                
                $('#modifyModal').modal({
                    backdrop: "static",
                    show: true
                });
                
            });
            
            jQuery("#remove01").live("click",function(){
                // Cita
                var idCita = jQuery(this).parent().parent().children().first().children().attr("class");
                var textoCita = jQuery(this).parent().parent().children().first().children().html();
                // Autor
                var nombreAutor = jQuery(this).parent().parent().children().first().next().children().html();
                
                jQuery("#texto").html(textoCita);
                jQuery("#autor").children().first().html(nombreAutor);
                jQuery("#removeBtn").attr("href", "../forms/e_quotes.php?id=" + idCita);
                
                jQuery("#textareaM").attr("style", "border: 1px solid #CCC;");
                jQuery("#help-inline-textM").html("");
                $('#removeModal').modal({
                    backdrop: "static",
                    show: true
                });
            });
            
            function enviarMod(campo, tabla) {
                error = false;
                if (jQuery("#textareaM").val() == '') {
                    jQuery("#textareaM").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-textM").html("Campo obligatorio");
                    error = true;
                } else {
                    valor = jQuery("#textareaM").val();
                    id = jQuery("#idM").val();
                    jQuery("#textareaM").attr("style", "border: 1px solid #CCC;");
                    jQuery("#help-inline-textM").html("");
                    jQuery.ajax({
                        async: false,
                        dataType: "json",
                        type: "post",
                        url: "../ajax/ajaxValidation.php",
                        data: {campo: campo, tabla: tabla, valor: valor, id: id},
                        success: function(data) {
                            if (data.exists) {
                                jQuery("#textareaM").attr("style", "border: 1px solid #D90416");
                                jQuery("#help-inline-textM").html("Valor existente");
                                error = true;
                            } else {
                                jQuery("#textareaM").attr("style", "border: 1px solid #CCC;");
                                jQuery("#help-inline-textM").html("");
                            }
                        }
                    });
                }
                if (jQuery("#input01M").val() == '') {
                    jQuery("#input01M").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text01M").html("Campo obligatorio");
                    error = true;
                } else {
                    jQuery("#input01M").attr("style", "border: 1px solid #CCC;");
                    jQuery("#help-inline-text01M").html("");
                }
                if (!error) {
                    if (jQuery("#input01idM").val() == '0' && jQuery("#input01M").val() != '') {
                        $('#modifyModal').modal("hide");
                        
                        jQuery("#nameM").html("Nombre: " + jQuery("#input01M").val());

                        $('#newAuthorM').modal({
                            backdrop: "static",
                            show: true
                        });
                    } else {
                        document.modificar_form.submit();
                    }
                }
            }
            
            function Modificar() {
                document.modificar_form.submit();
            }
            
            function Cancelar() {
                $('#newAuthorM').modal("hide");
                jQuery("#textareaM").attr("style", "border: 1px solid #CCC;");
                jQuery("#help-inline-textM").html("");
                $('#modifyModal').modal({
                    backdrop: "static",
                    show: true
                });
            }
            
            function enviarInsertar(campo, tabla) {
                error = false;
                if (jQuery("#textarea").val() == '') {
                    jQuery("#textarea").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text").html("Campo obligatorio");
                    error = true;
                } else {
                    valor = jQuery("#textarea").val();
                    jQuery("#textarea").attr("style", "border: 1px solid #CCC;");
                    jQuery("#help-inline-text").html("");
                    jQuery.ajax({
                        async: false,
                        dataType: "json",
                        type: "post",
                        url: "../ajax/ajaxValidation.php",
                        data: {campo: campo, tabla: tabla, valor: valor},
                        success: function(data) {
                            if (data.exists) {
                                jQuery("#textarea").attr("style", "border: 1px solid #D90416");
                                jQuery("#help-inline-text").html("Valor existente");
                                error = true;
                            } else {
                                jQuery("#textarea").attr("style", "border: 1px solid #CCC;");
                                jQuery("#help-inline-text").html("");
                            }
                        }
                    });
                }
                if (jQuery("#input01").val() == '') {
                    jQuery("#input01").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text01").html("Campo obligatorio");
                    error = true;
                } else {
                    jQuery("#input01").attr("style", "border: 1px solid #CCC;");
                    jQuery("#help-inline-text01").html("");
                }
                if (!error) {
                    if (jQuery("#input01id").val() == '0' && jQuery("#input01").val() != '') {
                        jQuery("#name").html("Nombre: " + jQuery("#input01").val());

                        $('#newAuthor').modal({
                            backdrop: "static",
                            show: true
                        });
                    } else {
                        document.insertar_form.submit();
                    }
                }
            }
            
            function Insertar() {
                document.insertar_form.submit();
            }
            
            function borrarId() {
                jQuery('#input01id').val('0');
                jQuery('#input01idM').val('0');
            }
            
            jQuery(document).ready(function() {
                <?php if ($citas != 0) { ?>
                    jQuery('#quotes_table').dataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "oLanguage": {
                            "sUrl": "../dataTableUtils/spanish"
                        },
                        "aoColumnDefs": [ 
                            { "bSortable": false, "aTargets": [ 2, 3 ] }
                        ],
                        "sPaginationType": "full_numbers",
                        "sAjaxSource": "../dataTableUtils/quotes_processing.php"
                    } );
                <?php } ?>
                    
                $('#input01').typeahead({
                    source: function (typeahead, query) {
                        $.ajax({
                            dataType : 'json',
                            url: "../typeAheadUtils/authors.php?q="+query,
                            success: function (data) {
                                typeahead.process(data)
                            }
                        });
                    },
                    property: "nombre",
                    onselect: function (obj) {
                        $('#input01id').val(obj.id);
                    }
                });
                
                $('#input01M').typeahead({
                    source: function (typeahead, query) {
                        $.ajax({
                            dataType : 'json',
                            url: "../typeAheadUtils/authors.php?q="+query,
                            success: function (data) {
                                typeahead.process(data)
                            }
                        });
                    },
                    property: "nombre",
                    onselect: function (obj) {
                        $('#input01idM').val(obj.id);
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "admin/quotes.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <!-- MODAL - ELIMINAR -->
                    <div id="removeModal" class="modal hide fade" >
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal">×</a>
                            <h3>¿Desea eliminar esta cita?</h3>
                        </div>
                        <div class="modal-body">
                            <p id="texto"></p>
                            <p id="autor"><em></em></p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
                            <a id="removeBtn" href="" class="btn"><i class="icon-remove"></i> Eliminar</a>
                        </div> 
                    </div>

                    <!-- MODAL - MODIFICAR -->
                    <div id="modifyModal" class="modal hide fade" >
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal">×</a>
                            <h3>¿Desea modificar esta cita?</h3>
                        </div>
                        <form class="mod" action="../forms/m_quotes.php" method="post" name="modificar_form">
                            <fieldset>
                                <div class="modal-body">
                                    <input id="idM" name="id" value="" type="hidden" />
                                    <div class="control-group">
                                        <label class="control-label" for="textareaM">Texto</label>
                                        <div class="controls">
                                            <textarea class="input-xlarge" id="textareaM" rows="4" name="texto"></textarea>
                                            <span id="help-inline-textM" class="help-inline" style="color: #B94A48;"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="select01M">Autor</label>
                                        <div class="controls">
                                            <input type="text" id="input01M" name="autor" value="" onkeypress="borrarId()" />
                                            <span id="help-inline-text01M" class="help-inline" style="color: #B94A48;"></span>
                                            <input id="input01idM" name="idAutor" value="" type="hidden" />
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                        <div class="modal-footer">
                            <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
                            <a href="#" class="btn" onclick="enviarMod('texto','t_citas')"><i class="icon-pencil"></i> Modificar</a>
                        </div>
                    </div>
                    
                    <!-- MODAL - NUEVO AUTOR -->
                    <div id="newAuthor" class="modal hide fade" >
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal">×</a>
                            <h3>El autor no existe. ¿Desea añadirlo?</h3>
                        </div>
                        <div class="modal-body">
                            <p id="name"></p>
                            <p><em>NOTA: si pulsa "Cancelar", no se añadirá la cita.</em></p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
                            <a href="#" class="btn" onclick="Insertar()"><i class="icon-plus"></i> Añadir</a>
                        </div> 
                    </div>
                    
                    <!-- MODAL - NUEVO AUTOR EN MODIFICAR -->
                    <div id="newAuthorM" class="modal hide fade" >
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal">×</a>
                            <h3>El autor no existe. ¿Desea añadirlo?</h3>
                        </div>
                        <div class="modal-body">
                            <p id="nameM"></p>
                            <p><em>NOTA: si pulsa "Cancelar", no se modificará la cita.</em></p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn" data-dismiss="modal" onclick="Cancelar()"><i class="icon-minus"></i> Cancelar</a>
                            <a href="#" class="btn" onclick="Modificar()"><i class="icon-plus"></i> Añadir</a>
                        </div> 
                    </div>

                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Nueva cita</a></li>
                            <li><a href="#2" data-toggle="tab">Todas las citas</a></li>
                        </ul>
                        <div class="tab-content">

                            <!-- PESTAÑA 1 -->
                            <div class="tab-pane active" id="1">
                                <?php if (isset($_SESSION["insertada"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo $_SESSION["insertada"];
                                        session_unregister("insertada");
                                        ?>
                                    </div>
                                    <?php
                                }
                                if (isset($_SESSION["error"])) {
                                    ?>
                                    <div class="alert alert-error">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo "<strong>" . $_SESSION["error"] . "</strong>";
                                        session_unregister("error");
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php if (isset($_SESSION["modificada"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo $_SESSION["modificada"];
                                        session_unregister("modificada");
                                        ?>
                                    </div>
                                <?php } ?>
                                <?php if (isset($_SESSION["eliminada"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo $_SESSION["eliminada"];
                                        session_unregister("eliminada");
                                        ?>
                                    </div>
                                <?php } ?>
                                <form class="form-horizontal" action="../forms/i_quotes.php" method="post" name="insertar_form">
                                    <fieldset>
                                        <legend>Cita</legend>
                                        <div class="control-group">
                                            <label class="control-label" for="textarea">Texto</label>
                                            <div class="controls">
                                                <textarea class="input-xlarge" id="textarea" rows="4" name="texto"></textarea>
                                                <span id="help-inline-text" class="help-inline" style="color: #B94A48;"></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="input01">Autor</label>
                                            <div class="controls">
                                                <input type="text" id="input01" name="autor" value="" onkeypress="borrarId()" />
                                                <span id="help-inline-text01" class="help-inline" style="color: #B94A48;"></span>
                                                <input id="input01id" name="idAutor" value="" type="hidden" />
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <a href="#" class="btn" onclick="enviarInsertar('texto','t_citas')"><i class="icon-plus"></i> Añadir</a>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>

                            <!-- PESTAÑA 2 -->
                            <div class="tab-pane" id="2">
                                <?php if ($citas == 0) { ?>
                                    <div class="alert alert-error">
                                        <?php
                                        echo "<strong>" . $_SESSION["noHay"] . "</strong>";
                                        session_unregister("noHay");
                                        ?>
                                    </div>
                                <?php } else { ?>
                                    <table id="quotes_table" class="table">
                                        <thead>
                                            <tr>
                                                <th id="main-column">Texto</th>
                                                <th id="main-column-author">Autor</th>
                                                <th>Modificar</th>
                                                <th>Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>
            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>
<?php
session_unregister("active");
?>