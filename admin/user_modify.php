<?php
session_start();
require_once '../utilidades/filter.php';
require_once '../utilidades/GestionUsuarios.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/clases/TipoUsuario.php';
require_once '../utilidades/constant.php';
global $raiz;

$user = new Usuario();
$user = GestionUsuarios::recuperarUsuario($_GET["id"]);
$t = new TipoUsuario();
$t = $user->getTipoUsuario();

if ($user->getFechaNac() == '0000-00-00' || $user->getFechaNac() == 'null' || $user->getFechaNac() == '') {
    $date = '';
} else {
    $date2 = explode('-', $user->getFechaNac());
    $date = $date2[2] . '/' . $date2[1] . '/' . $date2[0];
}

if (isset($_SESSION["perfil"])) {
    session_unregister("perfil");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Perfil del usuario <?php echo $user->getNombre(); ?> (modificar) - Panel de administración de BookWorm</title>
        <?php require_once '../parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/user-validation.js"></script>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/jquery.pstrength-min.1.2.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                $(function() {
                    $('#input04').pstrength();
                });
                
                $.datepicker.setDefaults($.datepicker.regional["es"]);
                jQuery('#input06').datepicker({
                    yearRange: "-100Y:+0Y",
                    maxDate: "+0D",
                    changeMonth: true,
                    changeYear: true
		});
                
                jQuery('#file').customFileInput({
                    feedback_text: 'Elige una imagen',
                    button_text: '...',
                    button_change_text: '...'
                });
            });
        </script>
    </head>
    <body>
        <div class="container">
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span12">
                    <div class="contact-content">
                        <?php require_once '../parts/user_mod.php'; ?>
                    </div>
                </div>
            </div>

            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>
