<?php
session_start();
require_once '../utilidades/filter.php';
require_once '../utilidades/GestionAutores.php';
require_once '../utilidades/clases/Autor.php';

$autores = array();
$autores = GestionAutores::recuperarAutores();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Autores - Panel de administración de BookWorm</title>
        <?php require_once '../parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/show-modal.js"></script>
        <script>
            jQuery(document).ready(function() {
                <?php if ($autores != 0) { ?>
                    jQuery('#authors_table').dataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "oLanguage": {
                            "sUrl": "../dataTableUtils/spanish"
                        },
                        "aoColumnDefs": [ 
                            { "bSortable": false, "aTargets": [ 1, 2 ] }
                        ],
                        "sPaginationType": "full_numbers",
                        "sAjaxSource": "../dataTableUtils/authors_processing.php"
                    } );
                <?php } ?>
            });
            
        </script>
    </head>
    <body>
        <input id="enlace" value="../forms/e_authors.php?id=" type="hidden" />
        <div class="container">
            <?php $_SESSION["page"] = "admin/books.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Nuevo autor</a></li>
                            <li><a href="#2" data-toggle="tab">Todos los autores</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="1">
                                <?php if (isset($_SESSION["insertada"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo $_SESSION["insertada"];
                                        session_unregister("insertada");
                                        ?>
                                    </div>
                                    <?php
                                }
                                if (isset($_SESSION["error"])) {
                                    ?>
                                    <div class="alert alert-error">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo "<strong>" . $_SESSION["error"] . "</strong>";
                                        session_unregister("error");
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php if (isset($_SESSION["modificada"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo $_SESSION["modificada"];
                                        session_unregister("modificada");
                                        ?>
                                    </div>
                                <?php } ?>
                                <?php if (isset($_SESSION["eliminada"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo $_SESSION["eliminada"];
                                        session_unregister("eliminada");
                                        ?>
                                    </div>
                                <?php } ?>
                                <form class="form-horizontal" action="../forms/i_authors.php" method="post" name="insertar_form">
                                    <fieldset>
                                        <legend>Autor</legend>
                                        <div class="control-group">
                                            <label class="control-label" for="input01">Nombre</label>
                                            <div class="controls">
                                                <input type="text" class="input-xlarge" id="input01" name="nombre" />
                                                <span id="help-inline-text" class="help-inline" style="color: #B94A48;"></span>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <a href="#" class="btn" onclick="enviarInsertar('nombre_autor','t_autores')"><i class="icon-plus"></i> Añadir</a>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="tab-pane" id="2">
                                <!-- MODAL - ELIMINAR -->
                                <div id="removeModal" class="modal hide fade" >
                                    <div class="modal-header">
                                        <a class="close" data-dismiss="modal">×</a>
                                        <h3>¿Desea eliminar este autor?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <p id="nombre"></p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
                                        <a id="removeBtn" href="" class="btn"><i class="icon-remove"></i> Eliminar</a>
                                    </div> 
                                </div>


                                <!-- MODAL - MODIFICAR -->
                                <div id="modifyModal" class="modal hide fade" >
                                    <div class="modal-header">
                                        <a class="close" data-dismiss="modal">×</a>
                                        <h3>¿Desea modificar este autor?</h3>
                                    </div>
                                    <form class="mod" action="../forms/m_authors.php" method="post" name="modificar_form">
                                        <fieldset>
                                            <div class="modal-body">
                                                <input id="idM" name="id" value="" type="hidden" />
                                                <div class="control-group">
                                                    <label class="control-label" for="input01">Autor</label>
                                                    <div class="controls">
                                                        <input type="text" class="input-xlarge" id="input01M" name="nombre" value="" />
                                                        <span id="help-inline-textM" class="help-inline" style="color: #B94A48;"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                    <div class="modal-footer">
                                        <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
                                        <a href="#" class="btn" onclick="enviarMod('nombre_autor','t_autores')"><i class="icon-pencil"></i> Modificar</a>
                                    </div>
                                </div>
                                
                                <?php if ($autores == 0) { ?>
                                    <div class="alert alert-error">
                                        <?php
                                        echo "<strong>" . $_SESSION["noHay"] . "</strong>";
                                        session_unregister("noHay");
                                        ?>
                                    </div>
                                <?php } else { ?>
                                    <table id="authors_table" class="table">
                                        <thead>
                                            <tr>
                                                <th id="main-column">Nombre</th>
                                                <th>Modificar</th>
                                                <th>Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>
            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>
<?php
session_unregister("active");
?>