<?php
session_start();
require_once '../utilidades/filter.php';
require_once '../utilidades/GestionDenuncias.php';

$denuncias = array();
$denuncias = GestionDenuncias::recuperarDenuncias();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Denuncias - Panel de administración de BookWorm</title>
        <?php require_once '../parts/head.php'; ?>
        <script>
            
            jQuery("#remove01").live("click",function(){
                var enlace = jQuery(this).parent().parent().children().first().next().next().next().children().attr("class");
                var isbn = jQuery(this).parent().parent().children().first().next().children().html();
                var titulo = jQuery(this).parent().parent().children().first().next().next().html();
                // Usuario
                var usuario = jQuery(this).parent().parent().children().first().children().html();
                            
                jQuery.ajax({
                    dataType: "json",
                    type: "post",
                    url: "../ajax/getComplain.php",
                    data: {
                        enlace: enlace
                    },
                    success: function(data) {
                        if (data) {
                            jQuery("#usuario").html("Usuario: " + usuario);
                            jQuery("#isbn").html("Libro: " + isbn + ', ' + titulo);
                            jQuery("#contenido").children().first().html("Texto: " + data.contenido);
                            jQuery("#removeBtn").attr("href", "../forms/e_complains.php?enlace=" + enlace);

                            $('#removeModal').modal({
                                backdrop: "static",
                                show: true
                            });
                        }
                    }
                });
            });
            
            jQuery("#details01").live("click",function(){
                var enlace = jQuery(this).parent().parent().children().first().next().next().next().children().attr("class");
                var isbn = jQuery(this).parent().parent().children().first().next().children().html();
                var titulo = jQuery(this).parent().parent().children().first().next().next().html();
                // Usuario
                var usuario = jQuery(this).parent().parent().children().first().children().html();

                jQuery.ajax({
                    dataType: "json",
                    type: "post",
                    url: "../ajax/getComplain.php",
                    data: {
                        enlace: enlace
                    },
                    success: function(data) {
                        if (data) {
                            jQuery("#usuarioD").html("Usuario: " + usuario);
                            jQuery("#isbnD").html("Libro: " + isbn + ', ' + titulo);
                            jQuery("#contenidoD").children().first().html("Texto: " + data.contenido);

                            $('#detailsModal').modal({
                                backdrop: "static",
                                show: true
                            });
                        }
                    }
                });
            });
            
            jQuery(".seen").live("click",function(){
                var enlace = jQuery(this).attr("value");

                jQuery.ajax({
                    dataType: "json",
                    type: "post",
                    url: "../ajax/updateComplain.php",
                    data: {
                        enlace: enlace
                    },
                    success: function(data) {
                        if (data) {
                            location.reload();
                        }
                    }
                });
            });
            
            jQuery(document).ready(function() {
                <?php if (count($denuncias) > 0) { ?>
                    jQuery('#complains_table').dataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "oLanguage": {
                            "sUrl": "../dataTableUtils/spanish"
                        },
                        "aoColumnDefs": [ 
                            { "bSortable": false, "aTargets": [ 5, 6 ] }
                        ],
                        "sPaginationType": "full_numbers",
                        "sAjaxSource": "../dataTableUtils/complains_processing.php"
                    } );
                <?php } ?>
            });
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "admin/books.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <!-- MODAL - ELIMINAR -->
                    <div id="removeModal" class="modal hide fade" >
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal">×</a>
                            <h3>¿Desea eliminar esta denuncia?</h3>
                        </div>
                        <div class="modal-body">
                            <p id="usuario"></p>
                            <p id="isbn"></p>
                            <p id="contenido"><em></em></p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
                            <a id="removeBtn" href="" class="btn"><i class="icon-remove"></i> Eliminar</a>
                        </div> 
                    </div>
                    <!-- MODAL - DETALLES -->
                    <div id="detailsModal" class="modal hide fade" >
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal">×</a>
                            <h3>Detalles de la denuncia</h3>
                        </div>
                        <div class="modal-body">
                            <p id="usuarioD"></p>
                            <p id="isbnD"></p>
                            <p id="contenidoD"><em></em></p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn" data-dismiss="modal"><i class="icon-ok"></i> Aceptar</a>
                        </div> 
                    </div>
                    
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Denuncias</a></li>
                        </ul>
                        <div class="tab-content">
                            <!-- PESTAÑA 1 -->
                            <div class="tab-pane active" id="1">
                                <?php if (isset($_SESSION["noHay"])) { ?>
                                    <div class="alert alert-error">
                                        <?php
                                        echo "<strong>" . $_SESSION["noHay"] . "</strong>";
                                        session_unregister("noHay");
                                        ?>
                                    </div>
                                <?php } else { ?>
                                    <table id="complains_table" class="table">
                                        <thead>
                                            <tr>
                                                <th>Usuario</th>
                                                <th>ISBN</th>
                                                <th>Título</th>
                                                <th>Fecha</th>
                                                <th>Vista</th>
                                                <th class="icons-column"></th>
                                                <th class="icons-column"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>
            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>
