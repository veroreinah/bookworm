<?php
session_start();
require_once '../utilidades/filter.php';
require_once '../utilidades/GestionLibros.php';

$libros = array();
$libros = GestionLibros::recuperarLibros();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Libros - Panel de administración de BookWorm</title>
        <?php require_once '../parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/book-validation.js"></script>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/new-book.js"></script>
        <script>
            jQuery("#remove01").live("click",function(){
                // Libro
                var isbn = jQuery(this).parent().parent().children().first().html();
                var titulo = jQuery(this).parent().parent().children().first().next().html();
                // Autor
                var nombreAutor = jQuery(this).parent().parent().children().first().next().next().html();
                
                jQuery("#isbn").html(isbn);
                jQuery("#titulo").html(titulo);
                jQuery("#autores").children().first().html(nombreAutor);
                jQuery("#removeBtn").attr("href", "../forms/e_books.php?isbn=" + isbn);
                
                $('#removeModal').modal({
                    backdrop: "static",
                    show: true
                });
            });
            
            jQuery(document).ready(function() {
                <?php if (count($libros) > 0) { ?>
                    jQuery('#books_table').dataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "oLanguage": {
                            "sUrl": "../dataTableUtils/spanish"
                        },
                        "aoColumnDefs": [ 
                            { "bSortable": false, "aTargets": [ 5, 6 ] }
                        ],
                        "sPaginationType": "full_numbers",
                        "sAjaxSource": "../dataTableUtils/books_processing.php"
                    } );
                <?php } ?>
            });
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "admin/books.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <!-- MODAL - ELIMINAR -->
                    <div id="removeModal" class="modal hide fade" >
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal">×</a>
                            <h3>¿Desea eliminar este libro?</h3>
                        </div>
                        <div class="modal-body">
                            <p id="isbn"></p>
                            <p id="titulo"></p>
                            <p id="autores"><em></em></p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
                            <a id="removeBtn" href="" class="btn"><i class="icon-remove"></i> Eliminar</a>
                        </div> 
                    </div>
                    
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Nuevo libro</a></li>
                            <li><a href="#2" data-toggle="tab">Todos los libros</a></li>
                        </ul>
                        <div class="tab-content">
                            <!-- PESTAÑA 1 -->
                            <div class="tab-pane active" id="1">
                                <?php if (isset($_SESSION["insertada"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo $_SESSION["insertada"];
                                        session_unregister("insertada");
                                        ?>
                                    </div>
                                    <?php
                                }
                                if (isset($_SESSION["error"])) {
                                    ?>
                                    <div class="alert alert-error">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo "<strong>" . $_SESSION["error"] . "</strong>";
                                        session_unregister("error");
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php if (isset($_SESSION["eliminada"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo $_SESSION["eliminada"];
                                        session_unregister("eliminada");
                                        ?>
                                    </div>
                                <?php } ?>
                                <?php require_once '../parts/book_insert.php'; ?>
                            </div>
                            <!-- PESTAÑA 2 -->
                            <div class="tab-pane" id="2">
                                <?php if (isset($_SESSION["noHayL"])) { ?>
                                    <div class="alert alert-error">
                                        <?php
                                        echo "<strong>" . $_SESSION["noHayL"] . "</strong>";
                                        session_unregister("noHayL");
                                        ?>
                                    </div>
                                <?php } else { ?>
                                    <table id="books_table" class="table">
                                        <thead>
                                            <tr>
                                                <th>ISBN</th>
                                                <th class="title-column">Título</th>
                                                <th class="title-column">Autor/es</th>
                                                <th>Fecha</th>
                                                <th class="title-column">Usuario</th>
                                                <th class="icons-column"></th>
                                                <th class="icons-column"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>
            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>
