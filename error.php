<?php
session_start();
require_once 'utilidades/GestionCitas.php';
require_once 'utilidades/constant.php';
global $raiz;

$imagenes = GestionCitas::generarImagenes(1);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm</title>
        <?php require_once 'parts/head.php'; ?>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "index.php" ?>
            <?php require_once 'parts/menu.php'; ?>

            <div class="row" style="padding-top: 60px;">
                <!-- MAIN CONTENT -->
                <div class="span12">
                    <div class="contact-content">
                        <div class="page-header">
                            <h1>Error. <small>Parece que la página a la que intentas acceder no existe.<br />
                                Sigue navegando por <a href="<?php echo $raiz; ?>/index.php" style="color: #D90416;">BookWorm</a></small></h1>
                        </div>
                        <div align="center">
                            <img src="<?php echo $raiz; ?>/img/citas/<?php echo $imagenes[0]; ?>" alt="" width="850" />
                        </div>
                    </div>
                </div>
            </div>

            <?php require_once 'parts/footer.php'; ?>
        </div>
    </body>
</html>
