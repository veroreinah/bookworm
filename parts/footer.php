<?php
chdir(dirname(__FILE__));
require_once '../utilidades/constant.php';
global $raiz;
?>
<div class="row">
    <div class="span12">
        <div class="alert alert-footer" style="text-align: center;">
            <p>
                <strong><a href="mailto:info.bookworm2012@gmail.com">BookWorm</a></strong> 2012 |
                <a data-toggle="modal" href="#privacyModal">Política de privacidad</a> |
                <a data-toggle="modal" href="#conditionsModal">Condiciones de uso</a>
            </p>
        </div>
        <!-- MODAL - POLÍTICA DE PRIVACIDAD -->
        <div id="privacyModal" class="modal hide fade" >
            <div class="modal-header">
                <a class="close" data-dismiss="modal">×</a>
                <h3>BookWorm - Política de privacidad</h3>
            </div>
            <div class="modal-body">
                <div class="alert" style="background-color: #D9EDF7; border-color: #BCE8F1; color: #3A87AD; font-size: 12px;">
                    En virtud de lo dispuesto en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de 
                    Carácter Personal, le informamos que los datos personales que usted nos facilite quedarán incorporados y 
                    serán tratados en nuestros ficheros, con el fin de poder atender su petición y prestarle el servicio solicitado.<br />
                    Así mismo, le informamos de la posibilidad de ejercer, en cualquier momento, los derechos de acceso, 
                    rectificación, cancelación y oposición de sus datos de carácter personal mediante correo electrónico 
                    dirigido a <a href="mailto:info.bookworm2012@gmail.com">info.bookworm2012@gmail.com</a>.<br />
                    Esta política de privacidad es aplicable a todos nuestros servicios, tanto si has accedido a ellos online 
                    como a través de otra plataforma o dispositivo (denominados en su conjunto "el sitio web").
                    <br />
                    <em>Puede conservar una copia de la política de privacidad <a href="<?php echo $raiz; ?>/pdf/privacy.pdf" target="_blank">descargándola en PDF.</a></em>
                </div>
                <h3>Recopilación de información</h3>
                <p>
                    <strong></strong>
                </p>
                <p>
                    <em>La última actualización de estos términos y condiciones se realizo el 17 de junio de 2012.</em>
                </p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal"><i class="icon-ok"></i> Aceptar</a>
            </div> 
        </div>
        <!-- MODAL - CONDICIONES DE USO -->
        <div id="conditionsModal" class="modal hide fade" >
            <div class="modal-header">
                <a class="close" data-dismiss="modal">×</a>
                <h3>BookWorm - Términos y Condiciones de Uso</h3>
            </div>
            <div class="modal-body">
                <div class="alert" style="background-color: #D9EDF7; border-color: #BCE8F1; color: #3A87AD; font-size: 12px;">
                    POR FAVOR, LEA ATENTAMENTE LAS SIGUIENTES CONDICIONES DE USO. 
                    SI NO ESTÁ DE ACUERDO CON ELLAS, QUIZÁ NO DEBERÍA NAVEGAR POR BOOKWORM. 
                    ANTE CUALQUIER DUDA, RELLENE NUESTRO FORMULARIO DE <a href="<?php echo $raiz; ?>/contact.php">CONTACTO</a> 
                    O ENVÍANOS UN <a href="mailto:info.bookworm2012@gmail.com">EMAIL</a>.
                    <br />
                    <em>Puede conservar una copia de estas condiciones <a href="<?php echo $raiz; ?>/pdf/conditions.pdf" target="_blank">descargándolas en PDF.</a></em>
                </div>
                <h3>Introducción</h3>
                <p>
                    <strong>Sobre los términos y condiciones</strong>
                </p>
                <ol>
                    <li>Estos términos y condiciones se aplican al uso del sitio web <a href="<?php echo $raiz; ?>/index.php">BookWorm</a>. Al acceder 
                        al sitio web usted acepta que quedará vinculado por estos términos y condiciones, tanto si usted 
                        es un usuario registrado de los Servicios como si no.</li>
                    <li>Nos reservamos el derecho de actualizar estos términos y condiciones en cualquier momento sin 
                        previo aviso. Si lo hacemos, la nueva versión será publicada en el sitio web. Los términos y 
                        condiciones modificados entrarán en vigor desde el momento en que se publicaron por primera vez 
                        en el sitio web. Si usted no está de acuerdo con los términos y condiciones modificados, deberá 
                        dejar de utilizar los Servicios a partir del momento en que se hayan publicado.</li>
                    <li>A los efectos de este acuerdo:
                        <ul>
                            <li>"Política de Privacidad" se refiere a la política que establece las obligaciones, tanto 
                                suyas como nuestras, en relación a la información personal que usted nos proporciona.</li>
                            <li>"Servicios" se refiere a la página web junto con las otras páginas, información, software, 
                                aplicaciones, servicios, productos y contenidos que pueden ser operados, organizada o 
                                gestionada por nosotros.</li>
                            <li>"Materiales de Terceros" significa cualquier y todos los contenidos de terceros, 
                                información, material, software y/o cualquier otro elemento y servicios mencionados, 
                                vinculados o de cualquier otra forma accesible o disponible a través de los Servicios.</li>
                            <li>"Información generada por el usuario" significa cualquier información puesta a 
                                disposición de otros usuarios de los Servicios y/o a nosotros, incluyendo, pero no 
                                limitado a, los nombres de pantalla, notas, reseñas de libros y los comentarios, los 
                                datos de uso.</li>
                        </ul>
                    </li>
                    <li>No están permitidas las cuentas registradas por métodos automatizados, incluyendo a cuentas 
                        registradas por los robots de Internet.</li>
                </ol>
                <p>
                    <strong>Sobre nosotros</strong>
                </p>
                <ol>
                    <li>Todos los servicios son operados por Verónica Reina Hernández</li>
                    <li>Si tiene alguna pregunta, queja o comentario sobre el sitio web, por favor háganoslo saber 
                        poniéndose en contacto con nosotros en la siguiente dirección de correo electrónico: 
                        <a href="mailto:info.bookworm2012@gmail.com">info.bookworm2012@gmail.com</a> 
                        o mediante el formulario de <a href="<?php echo $raiz; ?>/contact.php">contacto</a>.</li>
                </ol>
                <p>
                    <strong>Servicios</strong>
                </p>
                <ol>
                    <li>El objetivo de BookWorm es, ante todo, didáctico, ya que se trata del proyecto de Integración 
                        del C.F.G.S. de Desarrollo de Aplicaciones Informáticas. Además, lo que se pretende es crear una 
                        comunidad de lectores en la que puedan compartir sus opiniones y sentirse a gusto. Para asegurar
                        un buen ambiente, es necesario cumplir algunas reglas, por lo que su derecho a acceder y utilizar
                        los Servicios está sujeto a su conformidad con estos términos y condiciones. Si usted no cumple
                        con estos términos o condiciones, se podrá cancelar su cuenta, sin necesidad de previo aviso y
                        sin responsabilidad para usted.</li>
                </ol>
                <h3>Su cuenta</h3>
                <p>
                    <strong>Registro</strong>
                </p>
                <ol>
                    <li>Para participar plenamente en nuestra comunidad y para recibir la mayor parte de los Servicios, 
                        debe registrarse con nosotros y crear una cuenta. Para ello será necesario que nos 
                        dé cierta información personal incluyendo una dirección válida de correo electrónico 
                        y cualquier otro detalle que se requiera como parte del proceso de registro. Además, debe estar 
                        de acuerdo con nuestra política de privacidad. Si no está de acuerdo con los 
                        términos y condiciones de nuestra política de privacidad no se debe crear una cuenta con nosotros 
                        y no podrá utilizar los Servicios.</li>
                    <li>Por favor, asegúrese de que la información personal que proporciona cuando se registra es 
                        correcta y completa y que nos informa de cualquier cambio en la misma. Puede actualizar 
                        su información personal accediendo a su cuenta de usuario en el sitio web.</li>
                </ol>
                <p>
                    <strong>Uso de su cuenta</strong>
                </p>
                <ol>
                    <li>Usted es responsable del uso de su cuenta, así como de la protección de sus datos de acceso,
                        que no debe compartir con nadie. Si usted no cumple con las obligaciones anteriores, 
                        no nos responsabilizaremos de las pérdidas que sufra como consecuencia y usted 
                        será responsable de los daños causados ​​a BookWorm debido a dicho uso no autorizado. Usted es 
                        responsable de todo el contenido publicado y la actividad que ocurra bajo su cuenta. Debe notificarnos 
                        inmediatamente de cualquier uso no autorizado de su cuenta o cualquier otra violación de seguridad 
                        en relación con las transacciones que se realicen a través del sitio web. Usted no debe usar 
                        la cuenta de otro usuario.</li>
                    <li>Nuestros usuarios registrados pueden escribir comentarios, opiniones y otra información en el sitio
                        web. Al registrarse con nosotros, usted está de acuerdo en que podemos usar, reproducir,
                        distribuir y, por otra parte, mostrar públicamente su información junto con su nombre 
                        de usuario, dentro de los límites de BookWorm y sin ningún fin comercial. El uso por parte nuestra
                        de su información de usuario se podrá realizar sin previo aviso a usted, no existiendo la obligación 
                        de pagar una indemnización que de ninguna clase.</li>
                </ol>
                <h3>Los Servicios</h3>
                <p>
                    <strong>Su uso de los Servicios</strong>
                </p>
                <ol>
                    <li>Usted acepta que no:
                        <ul>
                            <li>utilizará el sitio web u otros servicios para la publicación o transmisión de cualquier material 
                                ilegal, dañino, amenazador, abusivo, acosador, difamatorio, vulgar, obsceno u ofensivo de
                                cualquier tipo.</li>
                            <li>subirá archivos que contengan virus, "troyanos", gusanos, archivos dañados u otras características 
                                destructivas que puedan, de cualquier forma, dañar, inutilizar o deteriorar el funcionamiento del 
                                sitio web, o cualquier intento de obtener acceso no autorizado al sitio web u otros servicios.</li>
                            <li>reproducirá, duplicará o copiará el sitio web o venderá o explotará los Servicios o cualquier parte de 
                                ellos sin nuestro permiso expreso por escrito.</li>
                            <li>maltratará, intimidará u ofenderá, de cualquier manera, al resto de usuarios de los servicios o al 
                                personal de BookWorm.</li>
                        </ul>
                    </li>
                    <li>Usted reconoce y acepta que cualquiera de estas acciones pueden dar lugar a la suspensión o la eliminación 
                        inmediata de su cuenta.</li>
                </ol>
                <p>
                    <strong>Enlaces a terceros</strong>
                </p>
                <ol>
                    <li>No somos responsables del contenido o prácticas de privacidad de los sitios web de terceros que se mencionan en el sitio web o a través de los Servicios. Nosotros no seremos responsables de cualquier pérdida o daño causado por el uso o dependencia de cualquier contenido, bienes y/o servicios disponibles en dichos sitios web.</li>
                </ol>
                <p>
                    <strong>El contenido y los derechos de uso de los Servicios</strong>
                </p>
                <ol>
                    <li>Siempre que cumpla plenamente con estos términos y condiciones tendrá derecho acceder a los Servicios, incluyendo cualquier contenido, información, materiales, software y cualquier otro artículo que pongamos a su disposición. Nos reservamos el derecho a restringir en la medida que consideremos qué parte de estos contenidos puede ver o utilizar.</li>
                    <li>Salvo lo expresamente indicado en estos términos y condiciones, usted no podrá copiar, reproducir, publicar, distribuir, transmitir, ejecutar públicamente, mostrar o poner a disposición de otros, alterar, adaptar, interferir con, crear obras derivadas o falsificar ningún contenido, información, materiales, software o cualquier otro artículo y los servicios prestados o puestos a disposición a través de este sitio web.</li>
                    <li>Usted se compromete a no utilizar ningún contenido, información, material, software o cualquier otro elemento o servicio disponible en este sitio web para cualquier propósito comercial o ilegal, o para cualquier otro fin prohibido por estos términos y condiciones.</li>
                    <li>Usted acepta que tenemos derecho a rechazar, eliminar o quitar cualquier contenido que usted proporcione al sitio web siempre que se estime oportuno.</li>
                </ol>
                <p>
                    <strong>Material de terceros</strong>
                </p>
                <ol>
                    <li>Usted reconoce y acepta que no podemos controlar, que no somos responsables de y que no pretendemos avalar cualquier material de terceros de ninguna manera. Todos los materiales de terceros se ponen a su disposición, pero no aceptamos (en la medida permitida por la ley aplicable) ninguna responsabilidad por la exactitud y fiabilidad o no de dichos materiales de terceros.</li>
                    <li>Es posible que los materiales de terceros estén sujetos a sus propios términos y condiciones, por lo que debe leerlos y asegurarse de que está de acuerdo con ellos antes de utilizar cualquier material.</li>
                </ol>
                <h3>General</h3>
                <p>
                    <strong>Nuestras responsabilidades</strong>
                </p>
                <ol>
                    <li>Nada en estos términos y condiciones excluye, restringe o afecta a sus derechos legales.</li>
                    <li>No garantizamos que:
                        <ul>
                            <li>El sitio web y/o los servicio se adapten a sus necesidades específicas.</li>
                            <li>El sitio web y/o los servicio sean ininterrumpidos, seguros o libres de errores.</li>
                            <li>Los resultados que pueden ser obtenido a partir de la utilización del sitio web y/o los servicio será precisa o fiable.</li>
                            <li>La calidad de los productos, servicios e información obtenidos por usted a través de la web y/o de los servicios satisfaga sus expectativas.</li>
                        </ul>
                    </li>
                    <li>Usted entiende y acepta expresamente que, en la medida permitida por la ley, no seremos responsables de daños directos o indirectos resultantes de:
                        <ul>
                            <li>El uso o la imposibilidad de utilizar el portal y/o los servicios.</li>
                            <li>El acceso no autorizado o la alteración de sus datos.</li>
                            <li>Las declaraciones o conductas de terceros en el sitio web y/o a través de los servicios.</li>
                            <li>La terminación o suspensión de su cuenta de usuario.</li>
                            <li>Cualquier otro asunto relacionado con el sitio web y/o los servicios.</li>
                        </ul>
                    </li>
                    <li>Usted es responsable de asegurar que su sistema informático, dispositivo portátil inteligente o cualquier otro cumple todas 
                        las especificaciones técnicas necesarias para utilizar el portal y es compatible con el mismo. No garantizamos que el 
                        sitio web o sus servidores estén libres de virus, gusanos, cancelbots, "troyanos" o cualquier cosa que 
                        contenga propiedades destructivas. No seremos responsables por cualquier daño o virus que 
                        puedan infectar su equipo informático u otros bienes después de su acceso o uso del sitio web y/o los servicios. 
                        Usted es responsable de utilizar los medios que estime oportunos para proteger sus dispositivos.</li>
                    <li>En el caso de que no se nos permita por ley exonerarnos de nuestra responsabilidades hacia usted
                        bajo estos términos y condiciones, nuestra responsabilidad no excederá de 100 €.</li>
                </ol>
                <p>
                    <strong>Eventos fuera del control de cualquiera de las partes</strong>
                </p>
                <ol>
                    <li>Ninguna de las partes será responsable por cualquier incumplimiento de cualquier obligación contraída 
                        con la otra, debido a razones fuera de su control, por ejemplo, los conflictos laborales, dificultades técnicas, 
                        los fallos o retrasos en las redes de comunicación, etc.</li>
                </ol>
                <p>
                    <strong>Uso del ancho de banda</strong>
                </p>
                <ol>
                    <li>Si su uso del ancho de banda supera 250 MB por mes o excede significativamente el ancho de banda media de los usuarios 
                        (según lo determinado por nosotros) usted acepta que podremos suspender su cuenta en todo o en parte hasta que 
                        reduzca su consumo de ancho de banda.</li>
                </ol>
                <p>
                    <strong>Terminación y cambios en los servicios</strong>
                </p>
                <ol>
                    <li>Nos reservamos el derecho de cancelar o suspender este acuerdo, su cuenta y/o su acceso a este sitio web y/o los servicios en cualquier momento. La terminación de su cuenta resultará en la eliminación de su cuenta y de todo su contenido.</li>
                    <li>Usted acepta que podemos modificar, suspender o interrumpir la prestación de los servicios (en todo o en parte) en cualquier momento sin ninguna responsabilidad resultante.</li>
                </ol>
                <p>
                    <em>La última actualización de estos términos y condiciones se realizo el 9 de junio de 2012.</em>
                </p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal"><i class="icon-ok"></i> Aceptar</a>
            </div> 
        </div>
    </div>
</div>
