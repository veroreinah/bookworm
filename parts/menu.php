<?php
chdir(dirname(__FILE__));
require_once '../utilidades/constant.php';
global $raiz;

if (isset($_SESSION["usuario"])) {
    if ($_SESSION["tipoUsuario"] == "usuario") {
        ?>
        <!-- MENÚ DE LOS USUARIOS DE BOOKWORM -->
        <div class="row bw-menu">
            <div class="span10">				
                <ul class="nav nav-pills">
                    <li class="<?php echo $_SESSION["page"] == "index.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/index.php"><i class="icon-home icon-white"></i> Inicio</a></li>
                    <li class="<?php echo $_SESSION["page"] == "user/books.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/user/books.php"><i class="icon-book icon-white"></i> Estantería</a></li>
                    <li class="<?php echo $_SESSION["page"] == "user/friends.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/user/friends.php"><i class="icon-user icon-white"></i> Amigos</a></li>
                    <li class="<?php echo $_SESSION["page"] == "search.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/search.php"><i class="icon-search icon-white"></i> Buscador</a></li>
                    <li class="<?php echo $_SESSION["page"] == "contact.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/contact.php"><i class="icon-envelope icon-white"></i> Contactar</a></li>
                </ul>
            </div>
            <div class="span2" style="padding-top: 15px;">				
                <div align="right"><a href="<?php echo $raiz; ?>/index.php" style="color: #0AA696;"><strong>BOOKWORM</strong></a></div>
            </div>
        </div>
        <div class="row bw-border-bottom">
        </div>
    <?php } else { ?>
        <!-- MENÚ DE LOS ADMINISTRADORES -->
        <div class="row bw-menu">
            <div class="span10">				
                <ul class="nav nav-pills">
                    <li class="<?php echo $_SESSION["page"] == "index.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/index.php"><i class="icon-home icon-white"></i> Inicio</a></li>
                    <li class="<?php echo $_SESSION["page"] == "admin/books.php" ? "active" : "" ?> dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-book icon-white"></i> Libros <b class="caret"></b>
                        </a>
                        <ul id="menu1" class="dropdown-menu">
                            <li><a href="<?php echo $raiz; ?>/admin/books.php">Libros</a></li>
                            <li><a href="<?php echo $raiz; ?>/admin/authors.php">Autores</a></li>
                            <li><a href="<?php echo $raiz; ?>/admin/editorial.php">Editoriales</a></li>
                            <li><a href="<?php echo $raiz; ?>/admin/editing.php">Tipos de edición</a></li>
                            <li><a href="<?php echo $raiz; ?>/admin/theme.php">Temáticas</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo $raiz; ?>/admin/complains.php">Denuncias</a></li>
                        </ul>
                    </li>
                    <li class="<?php echo $_SESSION["page"] == "admin/quotes.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/admin/quotes.php"><i class="icon-pencil icon-white"></i> Citas</a></li>
                    <li class="<?php echo $_SESSION["page"] == "admin/users.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/admin/users.php"><i class="icon-user icon-white"></i> Usuarios</a></li>
                    <li class="<?php echo $_SESSION["page"] == "search.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/search.php"><i class="icon-search icon-white"></i> Buscador</a></li>
                </ul>
            </div>
            <div class="span2" style="padding-top: 15px;">				
                <div align="right"><a href="<?php echo $raiz; ?>/index.php" style="color: #0AA696;"><strong>BOOKWORM</strong></a></div>
            </div>
        </div>
        <div class="row bw-border-bottom">
        </div>
    <?php } ?>
<?php } else { ?>
    <!-- MENÚ DE LOS USUARIOS NO LOGUEADOS, MENÚ PÚBLICO -->
    <div class="row bw-menu">
        <div class="span10">				
            <ul class="nav nav-pills">
                <li class="<?php echo $_SESSION["page"] == "index.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/index.php"><i class="icon-home icon-white"></i> Inicio</a></li>
                <li class="<?php echo $_SESSION["page"] == "books.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/books.php"><i class="icon-book icon-white"></i> Libros</a></li>
                <li class="<?php echo $_SESSION["page"] == "search.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/search.php"><i class="icon-search icon-white"></i> Buscador</a></li>
                <li class="<?php echo $_SESSION["page"] == "contact.php" ? "active" : "" ?>"><a href="<?php echo $raiz; ?>/contact.php"><i class="icon-envelope icon-white"></i> Contactar</a></li>
            </ul>
        </div>
        <div class="span2" style="padding-top: 15px;">				
            <div align="right"><a href="<?php echo $raiz; ?>/index.php" style="color: #0AA696;"><strong>BOOKWORM</strong></a></div>
        </div>
    </div>
    <div class="row bw-border-bottom">
    </div>
<?php } ?>

