<?php
chdir(dirname(__FILE__));
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/GestionTematica.php';
require_once '../utilidades/clases/Tematica.php';
require_once '../utilidades/constant.php';
global $raiz;

$tematicas = array();
$tematicas = GestionTematica::recuperarTematicas();
?>
<!-- MODAL - NUEVA TEMÁTICA -->
<div id="newTheme" class="modal hide fade" >
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3>Datos de la nueva temática</h3>
    </div>
    <form class="mod">
        <fieldset>
            <div class="modal-body">
                <div class="control-group">
                    <label class="control-label" for="input02">Descripción</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" id="input11" name="nombre" />
                        <span id="help-inline-text11" class="help-inline" style="color: #B94A48;"></span>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
        <a id="addThemeBtn" href="#" onclick="addTheme('<?php echo $raiz; ?>')" class="btn"><i class="icon-plus"></i> Añadir</a>
    </div> 
</div>
<form class="form-horizontal" action="<?php echo $raiz; ?>/forms/i_books.php" method="post" name="insertar_form" enctype="multipart/form-data">
    <fieldset>
        <legend>Libro</legend>
        <div class="control-group">
            <label class="control-label" for="input01">ISBN *</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="input01" name="isbn" maxlength="13" value="<?php echo isset($_GET["isbn"]) ? $_GET["isbn"] : "" ?>" />
                <span id="help-inline-text01" class="help-inline" style="color: #B94A48;"></span>
                <span id="help-inline-text01_1" class="help-inline" style="color: #BEBEC5; display: block;"><em>Escribe el ISBN sin guiones.</em></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input02">Título *</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="input02" name="titulo" />
                <span id="help-inline-text02" class="help-inline" style="color: #B94A48;"></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input03">Subtítulo</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="input03" name="subtitulo" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input05">Autores *</label>
            <div class="controls">
                <input id="input05temp" value="" type="hidden" />
                <input id="input05id" name="autores" value="" type="hidden" />
                <input id="input05new" name="nuevos_autores" value="" type="hidden" />
                <input class="input-xlarge" id="input05" type="text" onkeypress="borrarId()" />
                <button class="btn" type="button" onclick="addAuthor()"><i class="icon-plus-sign"></i> Añadir</button>
                <span id="help-inline-text05" class="help-inline" style="color: #BEBEC5; display: block;"><em>Autores: <span id="empty">todavía no has añadido ninguno...</span></em></span>
                <span id="help-inline-text05no" class="help-inline" style="color: #B94A48; display: block;"></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="file">Portada</label>
            <div class="controls">
                <input type="file" name="portada" id="file" accept="image/*" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="textarea">Sinopsis</label>
            <div class="controls">
                <textarea class="input-xlarge" id="textarea" rows="6" name="sinopsis" style="resize: none; width:450px;" ></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input04">Páginas</label>
            <div class="controls">
                <input type="text" class="input" id="input04" name="paginas" maxlength="6" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input06">Año de publicación</label>
            <div class="controls">
                <input type="text" class="input" id="input06" name="publicacion" maxlength="4" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input08">Editorial</label>
            <div class="controls">
                <input id="input08id" name="editorial" value="" type="hidden" />
                <input type="text" class="input-xlarge" id="input08" name="nombre_editorial" onkeypress="borrarIdE()" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input07">Edición</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="input07" name="edicion" />
                <span id="help-inline-text03" class="help-inline" style="color: #BEBEC5;"><em>Ejemplo: 1ª edición, Edición de coleccionista...</em></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input09">Tipo de edición</label>
            <div class="controls">
                <input id="input09id" name="tipo_edicion" value="" type="hidden" />
                <input type="text" class="input-xlarge" id="input09" name="nombre_tipo_edicion" onkeypress="borrarIdT()" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input11">Temáticas</label>
            <div class="controls">
                <select multiple="multiple" id="multiSelect" name="tematicas[]">
                    <?php for ($i = 0; $i < count($tematicas); $i++) {
                        $t = new Tematica();
                        $t = $tematicas[$i]; ?>
                        <option value="<?php echo $t->getId() ?>"><?php echo $t->getDescripcion(); ?></option>
                    <?php } ?>
                </select>
                <button class="btn" type="button" id="themeBtn" style="vertical-align: top;"><i class="icon-plus-sign"></i> Nueva temática</button>
            </div>
        </div>
        <div class="form-actions">
            <a href="#" class="btn" onclick="enviarInsertar('<?php echo $raiz; ?>')"><i class="icon-plus"></i> Añadir</a>
        </div>
    </fieldset>
</form>
