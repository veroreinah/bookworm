<?php
chdir(dirname(__FILE__));
require_once '../utilidades/constant.php';
global $raiz;
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo $raiz; ?>/css/bootstrap.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo $raiz; ?>/js/jquery-ui-bootstrap/css/custom-theme/jquery-ui-1.8.16.custom.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo $raiz; ?>/js/jquery-ui-bootstrap/third-party/jQuery-UI-FileInput/css/enhanced.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo $raiz; ?>/css/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo $raiz; ?>/js/akzhan-jwysiwyg-4480bf3/jquery.wysiwyg.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo $raiz; ?>/js/star-rating/jquery.rating.css" media="screen" />
<script type="text/javascript" src="<?php echo $raiz; ?>/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/bootstrap-alert.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/bootstrap-button.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/bootstrap-carousel.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/bootstrap-collapse.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/bootstrap-dropdown.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/bootstrap-modal.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/bootstrap-popover.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/bootstrap-scrollspy.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/bootstrap-tab.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/bootstrap-tooltip.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/bootstrap-transition.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/bootstrap-typeahead.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/DataTables-1.9.1/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/akzhan-jwysiwyg-4480bf3/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/jquery-ui-bootstrap/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/jquery-ui-bootstrap/js/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/jquery-ui-bootstrap/third-party/jQuery-UI-FileInput/js/fileinput.jquery.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/jquery-ui-bootstrap/third-party/jQuery-UI-FileInput/js/enhance.min.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/star-rating/jquery.rating.pack.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/star-rating/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/sidebar-validation.js"></script>
<script type="text/javascript" src="<?php echo $raiz; ?>/js/search-validation.js"></script>