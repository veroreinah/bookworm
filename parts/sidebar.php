<?php
chdir(dirname(__FILE__));
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/constant.php';
global $raiz;
?>

<div class="span3">
    <?php
    if (isset($_SESSION["usuario"])) {
        $u = new Usuario();
        $u = unserialize($_SESSION["usuario"]);
        if ($_SESSION["tipoUsuario"] == 'usuario') { ?>
            <ul class="thumbnails" style="margin-bottom: 0px;">
                <li class="span3" style="margin-bottom: 20px;">
                    <div class="thumbnail">
                        <img src="<?php echo $raiz; ?>/img/users/<?php echo $u->getImagen(); ?>" alt="" />
                        <a href="<?php echo $raiz; ?>/user/profile.php" class="btn"><i class="icon-user"></i> Perfil</a>
                    </div>
                </li>
            </ul>
            <form class="well" action="<?php echo $raiz; ?>/forms/add_shelf.php" method="post" name="shelf_form">
                <h5 style="margin-bottom: 5px;">Añadir a tu estantería (ISBN):</h5>
                <input type="text" class="span3" id="isbn" name="isbn" />
                <span id="help-inline-text01" class="help-inline" style="color: #BEBEC5; display: block;"><em>Sin guiones.</em></span><br />
                <a href="#" class="btn" onclick="addToShelf()" style="color:#333333;"><i class="icon-plus"></i> Añadir</a>
            </form>
            <form class="well" action="<?php echo $raiz; ?>/forms/invite.php" method="post" name="invite_form">
                <h5>Invita a un amigo:</h5>
                <label>Su email *</label>
                <input type="text" class="span3" id="email" name="email" />
                <label>Un mensaje</label>
                <textarea class="input" id="textarea" rows="5" name="mensaje" style="width: 169px;" ></textarea>
                <a href="#" class="btn" onclick="invite()" style="color:#333333;"><i class="icon-envelope"></i> Enviar</a>
            </form>
        <?php } else { ?>
            <ul class="thumbnails">
                <li class="span3">
                    <div class="thumbnail">
                        <img src="<?php echo $raiz; ?>/img/users/<?php echo $u->getImagen(); ?>" alt="" />
                        <h5><?php echo $u->getNombre(); ?></h5>
                        <a href="<?php echo $raiz; ?>/user/profile.php" class="btn" style="margin-top:20px;"><i class="icon-user"></i> Perfil</a>
                    </div>
                </li>
            </ul>
        <?php } ?>
    <?php } else { ?>
        <!-- LOGIN -->
        <form class="well" action="<?php echo $raiz; ?>/forms/login.php" method="post" name="login_form">
            <?php
            if (isset($_SESSION["errorLogin"])) {
                echo "<p class='error'><strong>" . $_SESSION["errorLogin"] . "</strong></p>";
                session_unregister("errorLogin");
            }
            ?>
            <label>Email</label>
            <input type="text" class="span3" id="email" name="email" value="<?php echo isset($_SESSION["emailLogin"]) ? $_SESSION["emailLogin"] : "" ?>" />
            <label>Contraseña</label>
            <input type="password" class="span3" id="pass" name="pass" />
            <p>
                <a href="<?php echo $raiz; ?>/forgot.php">¿Has olvidado tu contraseña?</a>
            </p>
            <a href="#" class="btn" onclick="login('<?php echo $raiz; ?>')" style="margin-bottom:20px; color:#333333;"><i class="icon-share-alt"></i> Acceder</a>
            <p>
                <a href="<?php echo $raiz; ?>/register.php">Registrarse</a>
            </p>
        </form>
<?php }
session_unregister("emailLogin"); ?>
</div>