<?php
chdir(dirname(__FILE__));
require_once '../utilidades/GestionCitas.php';
require_once '../utilidades/clases/Cita.php';
require_once '../utilidades/clases/Autor.php';
require_once '../utilidades/constant.php';
global $raiz;

$citas = array();
$citas = GestionCitas::recuperarCitas();

$cuantos = 5;

$aleatorios = GestionCitas::generarAleatorios($cuantos);
$imagenes = GestionCitas::generarImagenes($cuantos);
?>

<div class="row" style="padding-top: 60px;">
    <div class="span12">
        <div id="myCarousel" class="carousel slide">
            <div class="carousel-inner">
                <?php
                for ($i = 0; $i < $cuantos; $i++) {
                    $quotes = GestionCitas::recuperarCitasL($aleatorios[$i]-1, 1);
                    $c = new Cita();
                    $c = $quotes[0];
                    $autor = new Autor();
                    $autor = $c->getAutor();
                    ?>
                    <div class="<?php echo $i == 0 ? "item active" : "item" ?>" style="height: 300px;">
                        <img src="<?php echo $raiz; ?>/img/citas/<?php echo $imagenes[$i]; ?>.jpg" alt="BookWorm" />
                        <div class="carousel-caption">
                            <blockquote class="pull-right">
                                <p><strong><?php echo $c->getTexto(); ?></strong></p>
                                <small><?php echo $autor->getNombre(); ?></small>
                            </blockquote>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
        </div>
    </div>
</div>

