<?php
chdir(dirname(__FILE__));
require_once '../utilidades/constant.php';
global $raiz;
?>

<form class="form-horizontal" action="<?php echo $raiz; ?>/forms/m_users.php" method="post" name="modificar_form" enctype="multipart/form-data">
    <input id="id" value="<?php echo $user->getId(); ?>" type="hidden" name="id" />
    <fieldset>
        <legend>Modificar datos</legend>
        <div class="control-group">
            <label class="control-label" for="input01">Nombre de usuario *</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="input01" name="nombre" value="<?php echo $user->getNombre(); ?>" />
                <span id="help-inline-text01" class="help-inline" style="color: #B94A48;"></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input02">Email *</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="input02" name="email" value="<?php echo $user->getEmail(); ?>" />
                <span id="help-inline-text02" class="help-inline" style="color: #B94A48;"></span>
            </div>
        </div>
        <div class="alert alert-block">COMPLETA LOS CAMPOS DE CONTRASEÑA SÓLO SI QUIERES <strong>MODIFICARLA</strong></div>
        <div class="control-group">
            <label class="control-label" for="input03">Antigua contraseña</label>
            <div class="controls">
                <input type="password" class="input password" id="input03" name="oldPass" />
                <span id="help-inline-text03" class="help-inline" style="color: #B94A48;"></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input04">Nueva</label>
            <div class="controls">
                <input type="password" class="input password" id="input04" name="pass" />
                <span id="help-inline-text04" class="help-inline" style="color: #B94A48;"></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input05">Confirmar</label>
            <div class="controls">
                <input type="password" class="input password" id="input05" />
                <span id="help-inline-text05" class="help-inline" style="color: #B94A48;"></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="file">Imagen</label>
            <div class="controls">
                <input type="file" name="imagen" id="file" accept="image/*" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input06">Fecha de nacimiento</label>
            <div class="controls">
                <input type="text" class="input date_input" id="input06" name="fecha_nac" value="<?php echo $date; ?>" readonly="readonly" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input07">Facebook</label>
            <div class="controls">
                <input type="text" class="input" id="input07" name="facebook" value="<?php echo ($user->getFacebook() == 'null' || $user->getFacebook() == '') ? "" : $user->getFacebook(); ?>" />
            </div>
        </div>
        <div class="form-actions">
            <a href="#" class="btn" onclick="enviarMod('<?php echo $raiz; ?>')"><i class="icon-pencil"></i> Modificar</a>
            <?php if ($_SESSION["page"] == "user/profile.php") { ?>
                <a href="<?php echo $raiz; ?>/user/profile.php" class="btn"><i class="icon-minus"></i> Cancelar</a>
            <?php } else { ?>
                <a href="javascript:history.back()" class="btn"><i class="icon-minus"></i> Cancelar</a>
            <?php } ?>
        </div>
    </fieldset>
</form>
