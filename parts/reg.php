<?php
chdir(dirname(__FILE__));
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/constant.php';
global $raiz;
?>

<form class="form-horizontal" action="<?php echo $raiz; ?>/forms/registrar.php" method="post" name="insertar_form" enctype="multipart/form-data">
    <fieldset>
        <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == "admin") { ?>
            <legend>Datos del usuario</legend>
        <?php } else { ?>
            <legend>Únete a BookWorm</legend>
        <?php } ?>
        <div class="control-group">
            <label class="control-label" for="input01">Nombre de usuario *</label>
            <div class="controls">
                <?php if (isset($_GET["inv"])) { ?>
                    <input name="inv" value="<?php echo $_GET["inv"]; ?>" type="hidden" />
                <?php } else { ?>
                    <input name="inv" value="0" type="hidden" />
                <?php } ?>
                <input type="text" class="input-xlarge" id="input01" name="nombre" />
                <span id="help-inline-text01" class="help-inline" style="color: #B94A48;"></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input02">Email *</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="input02" name="email" />
                <span id="help-inline-text02" class="help-inline" style="color: #B94A48;"></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input03">Contraseña *</label>
            <div class="controls">
                <input type="password" class="input password" id="input03" name="pass" />
                <span id="help-inline-text03" class="help-inline" style="color: #B94A48;"></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input04">Confirmar *</label>
            <div class="controls">
                <input type="password" class="input password" id="input04" />
                <span id="help-inline-text04" class="help-inline" style="color: #B94A48; display: inline-block !important"></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="file">Imagen</label>
            <div class="controls">
                <input type="file" name="imagen" id="file" accept="image/*" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input05">Fecha de nacimiento</label>
            <div class="controls">
                <input type="text" class="input date_input" id="input05" name="fecha_nac" readonly="readonly" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input06">Facebook</label>
            <div class="controls">
                <input type="text" class="input" id="input06" name="facebook" />
            </div>
        </div>
        <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == "admin") { ?>
            <div class="control-group">
                <label class="control-label">Tipo de usuario *</label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="tipo" id="optionsRadios1" value="1" checked="" />
                        Administrador de BookWorm
                    </label>
                    <label class="radio">
                        <input type="radio" name="tipo" id="optionsRadios2" value="2" />
                        Usuario de BookWorm
                    </label>
                </div>
            </div>
            <input type="hidden" id="optionsCheckbox" value="conditions" checked="checked">
        <?php } else { ?>
            <div class="control-group">
                <label class="control-label" for="optionsCheckbox">*</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox" id="optionsCheckbox" value="conditions">
                        Acepto la <a data-toggle="modal" href="#privacyModal">Política de privacidad</a> y las <a data-toggle="modal" href="#conditionsModal">Condiciones de uso</a> de BookWorm.
                    </label>
                    <span id="help-inline-text05" class="help-inline" style="color: #B94A48;"></span>
                </div>
            </div>
        <?php } ?>
        <div class="form-actions">
            <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == "admin") { ?>
                <a href="#" class="btn" onclick="enviarInsertar('<?php echo $raiz; ?>')"><i class="icon-plus"></i> Añadir</a>
            <?php } else { ?>
                <a href="#" class="btn" onclick="enviarInsertar('<?php echo $raiz; ?>')"><i class="icon-arrow-up"></i> Enviar</a>
            <?php } ?>
        </div>
    </fieldset>
</form>