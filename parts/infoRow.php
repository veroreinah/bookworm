<?php
chdir(dirname(__FILE__));
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/GestionRedSocial.php';
require_once '../utilidades/constant.php';
global $raiz;

if (isset($_SESSION["usuario"])) {
    $userInv = new Usuario();
    $userInv = unserialize($_SESSION["usuario"]);
    $invitaciones = array();
    $invitaciones = GestionRedSocial::recuperarInvitaciones($userInv->getId());
}
?>

<div class="row">
    <div class="span12">
        <div class="alert alert-info">
            <form class="form-search" action="<?php echo $raiz; ?>/search_results.php" method="post" name="info_row_form" style="float: left;">
                <input type="text" class="input-medium search-query" name="titulo" placeholder="título..." id="search_info_row">
                <a href="#" class="btn" onclick="search_validate(2)" style="color:#333333;">
                    <i class="icon-search"></i> Buscar</a>
                <input type="hidden" name="filtro" value="titulo">
            </form>
            <?php if (isset($_SESSION["usuario"])) {
                $u = new Usuario();
                $u = unserialize($_SESSION["usuario"]); ?>
                <p>
                    <?php if ($_SESSION["tipoUsuario"] == "admin") { ?>
                        <strong>BOOKWORM - PANEL DE ADMINISTRACIÓN</strong> |
                    <?php } ?>
                    <?php if ($_SESSION["tipoUsuario"] == "usuario") { ?>
                        <strong><?php echo $u->getNombre(); ?></strong> |
                        <a href="<?php echo $raiz; ?>/user/invitations.php"><?php echo count($invitaciones); ?> invitaciones pendientes</a> |
                    <?php } ?>
                    <a href="<?php echo $raiz; ?>/forms/logout.php">Salir</a>
                </p>
            <?php } else { ?>
                <p>
                    <strong><?php echo date("d-m-Y"); ?></strong>
                </p>
            <?php } ?>
        </div>
    </div>
</div>

<?php if (isset($_SESSION["usuario"]) && $_SESSION["tipoUsuario"] == "usuario") { ?>
    <script>
        jQuery(document).ready(function() {
            <?php if (isset($_SESSION["invitacion"]) || isset($_SESSION["invitacionNo"])) { ?>
                $('#sendInvitationModal').modal("show");
            <?php } ?>
        });
    </script>
    <div id="sendInvitationModal" class="modal hide fade" >
        <div class="modal-header">
            <a class="close" data-dismiss="modal">×</a>
            <h3>Invitación</h3>
        </div>
        <div class="modal-body">
            <?php
            if (isset($_SESSION["invitacion"])) {
                echo $_SESSION["invitacion"];
                session_unregister("invitacion");
            }
            if (isset($_SESSION["invitacionNo"])) {
                echo $_SESSION["invitacionNo"];
                session_unregister("invitacionNo");
            }
            ?>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal"><i class="icon-ok"></i> Aceptar</a>
        </div> 
    </div>
<?php } ?>