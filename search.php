<?php
session_start();
require_once 'utilidades/filter.php';
require_once 'utilidades/GestionTematica.php';
require_once 'utilidades/GestionAutores.php';
require_once 'utilidades/GestionEditoriales.php';
require_once 'utilidades/clases/Tematica.php';
require_once 'utilidades/clases/Autor.php';
require_once 'utilidades/clases/Editorial.php';
require_once 'utilidades/constant.php';
global $raiz;

$tematicas = array();
$tematicas = GestionTematica::recuperarTematicas();
$autores = array();
$autores = GestionAutores::recuperarAutores();
$editoriales = array();
$editoriales = GestionEditoriales::recuperarEditoriales();
?>

<!DOCTYPE html>
<html>
    <head>
        <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'admin') { ?>
            <title>Búsqueda - Panel de administración de BookWorm</title>
        <?php } else { ?>
            <title>BookWorm - Búsqueda</title>
        <?php } ?>
        <?php require_once 'parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/login-validation.js"></script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "search.php" ?>
            <?php require_once 'parts/menu.php'; ?>
            <?php require_once 'parts/carousel.php'; ?>
            <?php require_once 'parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="main-content">
                    <div class="span9">
                        <div class="accordion" id="accordion2">
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                        Buscar libro (título)
                                    </a>
                                </div>
                                <div id="collapseOne" class="accordion-body collapse" style="height: 0px; ">
                                    <div class="accordion-inner">
                                        <form class="form-horizontal" action="<?php echo $raiz; ?>/search_results.php" method="post" name="buscar_titulo_form">
                                            <fieldset>
                                                <div class="control-group">
                                                    <label class="control-label" for="input01">Título</label>
                                                    <div class="controls">
                                                        <input type="text" class="input-xlarge" id="search_input01" name="titulo">
                                                        <span id="s-help-inline-text01" class="help-inline" style="color: #B94A48;"></span>
                                                        <input type="hidden" name="filtro" value="titulo">
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <a href="#" class="btn" onclick="search_validate(1)" style="color:#333333;">
                                                        <i class="icon-search"></i> Buscar</a>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                        Buscar libro (ISBN)
                                    </a>
                                </div>
                                <div id="collapseTwo" class="accordion-body collapse" style="height: 0px; ">
                                    <div class="accordion-inner">
                                        <form class="form-horizontal" action="<?php echo $raiz; ?>/search_results.php" method="post" name="buscar_isbn_form">
                                            <fieldset>
                                                <div class="control-group">
                                                    <label class="control-label" for="input02">ISBN</label>
                                                    <div class="controls">
                                                        <input type="text" class="input-xlarge" id="search_input02" name="isbn">
                                                        <span id="s-help-inline-text02" class="help-inline" style="color: #B94A48;"></span>
                                                        <span id="help-inline-text01_1" class="help-inline" style="color: #BEBEC5; display: block;"><em>Escribe el ISBN sin guiones.</em></span>
                                                        <input type="hidden" name="filtro" value="isbn">
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <a href="#" class="btn" onclick="search_validate(3)" style="color:#333333;">
                                                        <i class="icon-search"></i> Buscar</a>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                        Buscar libro (autor)
                                    </a>
                                </div>
                                <div id="collapseThree" class="accordion-body collapse" style="height: 0px; ">
                                    <div class="accordion-inner">
                                        <form class="form-horizontal" action="<?php echo $raiz; ?>/search_results.php" method="post" name="buscar_autor_form">
                                            <fieldset>
                                                <div class="control-group">
                                                    <label class="control-label" for="input05">Autor</label>
                                                    <div class="controls">
                                                        <select id="search_select05" name="autor">
                                                            <?php for ($i = 0; $i < count($autores); $i++) {
                                                                $a = new Autor();
                                                                $a = $autores[$i]; ?>
                                                                <option value="<?php echo $a->getId() ?>"><?php echo $a->getNombre(); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <input type="hidden" name="filtro" value="autor">
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn"><i class="icon-search"></i> Buscar</button>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                        Buscar libro (temática)
                                    </a>
                                </div>
                                <div id="collapseFour" class="accordion-body collapse" style="height: 0px; ">
                                    <div class="accordion-inner">
                                        <form class="form-horizontal" action="<?php echo $raiz; ?>/search_results.php" method="post" name="buscar_tematicas_form">
                                            <fieldset>
                                                <div class="control-group">
                                                    <label class="control-label" for="select04">Temática</label>
                                                    <div class="controls">
                                                        <select id="search_select04" name="tematica">
                                                            <?php for ($i = 0; $i < count($tematicas); $i++) {
                                                                $t = new Tematica();
                                                                $t = $tematicas[$i]; ?>
                                                                <option value="<?php echo $t->getId() ?>"><?php echo $t->getDescripcion(); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <span id="help-inline-text04" class="help-inline" style="color: #B94A48;"></span>
                                                        <input type="hidden" name="filtro" value="tematica">
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn"><i class="icon-search"></i> Buscar</button>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
                                        Búsqueda avanzada
                                    </a>
                                </div>
                                <div id="collapseFive" class="accordion-body collapse" style="height: 0px; ">
                                    <div class="accordion-inner">
                                        <p class="help-block" id="help-inline-text07" style="color: #B94A48;"></p>
                                        <form class="form-horizontal" action="<?php echo $raiz; ?>/search_results.php" method="post" name="buscar_avanzada_form">
                                            <fieldset>
                                                <div class="control-group">
                                                    <label class="control-label" for="input04">Título</label>
                                                    <div class="controls">
                                                        <input type="text" class="input-xlarge" id="search_input04" name="titulo">
                                                        <span id="s-help-inline-text07" class="help-inline" style="color: #B94A48;"></span>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="input05">Subtítulo</label>
                                                    <div class="controls">
                                                        <input type="text" class="input-xlarge" id="search_input05" name="subtitulo">
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="select02">Autor</label>
                                                    <div class="controls">
                                                        <select id="search_select02" name="autor">
                                                            <option value="0">Elige un autor...</option>
                                                            <?php for ($i = 0; $i < count($autores); $i++) {
                                                                $a = new Autor();
                                                                $a = $autores[$i]; ?>
                                                                <option value="<?php echo $a->getId() ?>"><?php echo $a->getNombre(); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="select01">Editorial</label>
                                                    <div class="controls">
                                                        <select id="search_select01" name="editorial">
                                                            <option value="0">Elige una editorial...</option>
                                                            <?php for ($i = 0; $i < count($editoriales); $i++) {
                                                                $e = new Editorial();
                                                                $e = $editoriales[$i]; ?>
                                                            <option value="<?php echo $e->getId() ?>"><?php echo $e->getNombre(); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="select03">Temáticas</label>
                                                    <div class="controls">
                                                        <select id="search_select03" name="tematica">
                                                            <option value="0">Elige una tematica...</option>
                                                            <?php for ($i = 0; $i < count($tematicas); $i++) {
                                                                $t = new Tematica();
                                                                $t = $tematicas[$i]; ?>
                                                                <option value="<?php echo $t->getId() ?>"><?php echo $t->getDescripcion(); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="input06">Año de publicación</label>
                                                    <div class="controls">
                                                        <input type="text" class="input" id="search_input06" name="publicacion" maxlength="4" />
                                                    </div>
                                                </div>
                                                <input type="hidden" name="filtro" value="avanzada">
                                                <div class="form-actions">
                                                    <a href="#" class="btn" onclick="search_validate(4)" style="color:#333333;">
                                                        <i class="icon-search"></i> Buscar</a>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($_SESSION["usuario"])) { ?>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
                                            Buscar usuario (nombre)
                                        </a>
                                    </div>
                                    <div id="collapseSix" class="accordion-body collapse" style="height: 0px; ">
                                        <div class="accordion-inner">
                                            <form class="form-horizontal" action="<?php echo $raiz; ?>/user/search_results.php" method="post" name="buscar_nombre_form">
                                                <fieldset>
                                                    <div class="control-group">
                                                        <label class="control-label" for="input07">Nombre de usuario</label>
                                                        <div class="controls">
                                                            <input type="text" class="input-xlarge" id="search_input07" name="nombre">
                                                            <span id="s-help-inline-text05" class="help-inline" style="color: #B94A48;"></span>
                                                            <input type="hidden" name="filtro" value="nombre">
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                    <a href="#" class="btn" onclick="search_validate(5)" style="color:#333333;">
                                                        <i class="icon-search"></i> Buscar</a>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven">
                                            Buscar usuario (email)
                                        </a>
                                    </div>
                                    <div id="collapseSeven" class="accordion-body collapse" style="height: 0px; ">
                                        <div class="accordion-inner">
                                            <form class="form-horizontal" action="<?php echo $raiz; ?>/user/search_results.php" method="post" name="buscar_email_form">
                                                <fieldset>
                                                    <div class="control-group">
                                                        <label class="control-label" for="input08">Email</label>
                                                        <div class="controls">
                                                            <input type="text" class="input-xlarge" id="search_input08" name="email">
                                                            <span id="s-help-inline-text06" class="help-inline" style="color: #B94A48;"></span>
                                                            <input type="hidden" name="filtro" value="email">
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <a href="#" class="btn" onclick="search_validate(6)" style="color:#333333;">
                                                            <i class="icon-search"></i> Buscar</a>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <!-- SIDEBAR -->
                <?php require_once 'parts/sidebar.php'; ?>
            </div>

            <?php require_once 'parts/footer.php'; ?>
        </div>
    </body>
</html>