<?php
session_start();
require_once '../utilidades/GestionDenuncias.php';
require_once '../utilidades/clases/Denuncia.php';
require_once '../utilidades/clases/Libro.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/constant.php';
global $raiz;

/*
 * Paging
 */
$begin = "";
$limit = "";
if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $begin = mysql_real_escape_string($_GET['iDisplayStart']);
    $limit = mysql_real_escape_string($_GET['iDisplayLength']);
}


/*
 * Ordering
 */
$field = "";
$order = "";
if (isset($_GET['iSortCol_0'])) {
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $field = intval($_GET['iSortCol_0']) == 0 ? "u.nombre_usuario" : "";
            if ($field == "") {
                $field = intval($_GET['iSortCol_0']) == 1 ? "l.isbn" : "";
            }
            if ($field == "") {
                $field = intval($_GET['iSortCol_0']) == 2 ? "l.titulo" : "";
            }
            if ($field == "") {
                $field = intval($_GET['iSortCol_0']) == 3 ? "d.fecha" : "";
            }
            if ($field == "") {
                $field = intval($_GET['iSortCol_0']) == 4 ? "vista" : "";
            }
            $order = mysql_real_escape_string($_GET['sSortDir_' . $i]);
        }
    }
}


/*
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
$where = "";
if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
    $where = mysql_real_escape_string($_GET['sSearch']);
}




/*
 * SQL queries
 * Get data to display
 */
$result = GestionDenuncias::recuperarDenunciasL($begin, $limit, $field, $order, $where);

/* Data set length after filtering */
$iFilteredTotal = GestionDenuncias::totalFiltered($where);

/* Total data s$aResultTotalet length */
$iTotal = GestionDenuncias::totalDenuncias();


/*
 * Output
 */
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < count($result); $i++) {
    $d = new Denuncia();
    $d = $result[$i];
    $u = new Usuario();
    $u = $d->getUsuario();
    $l = new Libro();
    $l = $d->getLibro();
    
    $date2 = explode('-', $d->getFecha());
    $date = $date2[2] . '/' . $date2[1] . '/' . $date2[0];

    $row = array();
    $row[] = "<a href=\"$raiz/user/user_details.php?id=" . $u->getId() . "\" target=\"_blank\" >" . $u->getNombre() . "</a>";
    $row[] = "<a href=\"$raiz/book_details.php?isbn=" . $l->getIsbn() . "\" target=\"_blank\" >" . $l->getIsbn() . "</a>";
    $row[] = $l->getTitulo();
    $row[] = "<span class=\"" . $d->getEnlace() . "\">$date</span>";
    if ($d->getVista()) {
        $row[] = "<input type=\"checkbox\" checked=\"checked\" disabled=\"disabled\" />";
    } else {
        $row[] = "<input type=\"checkbox\" class=\"seen\" id=\"" . $d->getEnlace() . "\" value=\"" . $d->getEnlace() . "\" />";
    }
    $row[] = "<a class=\"details\" id=\"details01\" href=\"#\"><i class=\"icon-zoom-in\"></i></a>";
    $row[] = "<a class=\"remove\" id=\"remove01\" href=\"#\"><i class=\"icon-remove\"></i></a>";
    
    $output['aaData'][] = $row;
}

echo json_encode($output);
?>