<?php
session_start();
require_once '../utilidades/GestionLibros.php';
require_once '../utilidades/GestionAutores.php';
require_once '../utilidades/clases/Libro.php';
require_once '../utilidades/clases/Autor.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/constant.php';
global $raiz;

/*
 * Paging
 */
$begin = "";
$limit = "";
if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $begin = mysql_real_escape_string($_GET['iDisplayStart']);
    $limit = mysql_real_escape_string($_GET['iDisplayLength']);
}


/*
 * Ordering
 */
$field = "";
$order = "";
if (isset($_GET['iSortCol_0'])) {
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'admin') {
                $field = intval($_GET['iSortCol_0']) == 0 ? "l.isbn" : "";
                if ($field == "") {
                    $field = intval($_GET['iSortCol_0']) == 1 ? "titulo" : "";
                }
                if ($field == "") {
                    $field = intval($_GET['iSortCol_0']) == 2 ? "a.nombre_autor" : "";
                }
                if ($field == "") {
                    $field = intval($_GET['iSortCol_0']) == 3 ? "fecha" : "";
                }
                if ($field == "") {
                    $field = intval($_GET['iSortCol_0']) == 4 ? "u.nombre_usuario" : "";
                }
            } else {
                $field = intval($_GET['iSortCol_0']) == 1 ? "l.isbn" : "";
                if ($field == "") {
                    $field = intval($_GET['iSortCol_0']) == 2 ? "titulo" : "";
                }
                if ($field == "") {
                    $field = intval($_GET['iSortCol_0']) == 3 ? "a.nombre_autor" : "";
                }
                if ($field == "") {
                    $field = intval($_GET['iSortCol_0']) == 4 ? "u.nombre_usuario" : "";
                }
            }
            
            $order = mysql_real_escape_string($_GET['sSortDir_' . $i]);
        }
    }
}


/*
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
$where = "";
if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
    $where = mysql_real_escape_string($_GET['sSearch']);
}




/*
 * SQL queries
 * Get data to display
 */
$result = GestionLibros::recuperarLibrosL($begin, $limit, $field, $order, $where);

/* Data set length after filtering */
$iFilteredTotal = GestionLibros::totalFiltered($where);

/* Total data s$aResultTotalet length */
$iTotal = GestionLibros::totalLibros();


/*
 * Output
 */
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < count($result); $i++) {
    $l = new Libro();
    $l = $result[$i];
    $a = array();
    $a = $l->getAutores();
    $u = new Usuario();
    $u = $l->getUsuario();
    
    $date2 = explode('-', $l->getFecha());
    $date = $date2[2] . '/' . $date2[1] . '/' . $date2[0];
    
    $autores = array();
    for($j=0; $j < count($a); $j++) {
        $autor = new Autor();
        $autor = $a[$j];
        
        $autores[] = $autor->getNombre();
    }
    
    $autores = implode(', ', $autores);

    $row = array();
    if (!isset($_SESSION["tipoUsuario"]) || (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'usuario')) {
        $row[] = "<img src=\"$raiz/img/portadas/" . $l->getPortada() . "\" style=\"height: 40px;\" />";
    }
    $row[] = $l->getIsbn();
    $row[] = $l->getTitulo();
    $row[] = $autores;
    if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'admin') {
        $row[] = $date;
    }
    if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'admin') {
        $row[] = "<a href=\"$raiz/user/user_details.php?id=" . $u->getId() . "\" >" . $u->getNombre() . "</a>";
    } elseif (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'usuario') {
        $row[] = "<a href=\"$raiz/user/user_details.php?id=" . $u->getId() . "\" >" . $u->getNombre() . "</a>";
    }
    $row[] = "<a class=\"details\" id=\"details01\" href=\"$raiz/book_details.php?isbn=" . $l->getIsbn() . "\"><i class=\"icon-zoom-in\"></i></a>";
    if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'admin') {
        $row[] = "<a class=\"remove\" id=\"remove01\" href=\"#\"><i class=\"icon-remove\"></i></a>";
    }
    $output['aaData'][] = $row;
}

echo json_encode($output);
?>