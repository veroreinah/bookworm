<?php
session_start();
require_once '../utilidades/GestionLibros.php';
require_once '../utilidades/GestionAutores.php';
require_once '../utilidades/clases/Libro.php';
require_once '../utilidades/clases/Autor.php';
require_once '../utilidades/constant.php';
global $raiz;

if (isset($_GET["id_usuario"])) {
    $id = $_GET["id_usuario"];
} else {
    $id = 0;
}

/*
 * Paging
 */
$begin = "";
$limit = "";
if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $begin = mysql_real_escape_string($_GET['iDisplayStart']);
    $limit = mysql_real_escape_string($_GET['iDisplayLength']);
}


/*
 * Ordering
 */
$field = "";
$order = "";
if (isset($_GET['iSortCol_0'])) {
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $field = intval($_GET['iSortCol_0']) == 1 ? "l.isbn" : "";
            if ($field == "") {
                $field = intval($_GET['iSortCol_0']) == 2 ? "titulo" : "";
            }
            if ($field == "") {
                $field = intval($_GET['iSortCol_0']) == 3 ? "a.nombre_autor" : "";
            }
            $order = mysql_real_escape_string($_GET['sSortDir_' . $i]);
        }
    }
}


/*
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
$where = "";
if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
    $where = mysql_real_escape_string($_GET['sSearch']);
}




/*
 * SQL queries
 * Get data to display
 */
$result = GestionLibros::recuperarLibrosPorUsuario($begin, $limit, $field, $order, $where, $id);

/* Data set length after filtering */
$iFilteredTotal = GestionLibros::totalFilteredPorUsuario($where, $id);

/* Total data s$aResultTotalet length */
$iTotal = GestionLibros::totalLibrosPorUsuario($id);


/*
 * Output
 */
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < count($result); $i++) {
    $l = new Libro();
    $l = $result[$i];
    $a = array();
    $a = $l->getAutores();
    
    $autores = array();
    for($j=0; $j < count($a); $j++) {
        $autor = new Autor();
        $autor = $a[$j];
        
        $autores[] = $autor->getNombre();
    }
    
    $autores = implode(', ', $autores);

    $row = array();
    $row[] = "<img src=\"$raiz/img/portadas/" . $l->getPortada() . "\" style=\"height: 40px;\" />";
    $row[] = $l->getIsbn();
    $row[] = $l->getTitulo();
    $row[] = $autores;
    $row[] = "<a class=\"details\" id=\"details01\" href=\"$raiz/book_details.php?isbn=" . $l->getIsbn() . "\"><i class=\"icon-zoom-in\"></i></a>";
    $output['aaData'][] = $row;
}

echo json_encode($output);
?>