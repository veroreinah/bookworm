<?php
session_start();
require_once '../utilidades/GestionRedSocial.php';
require_once '../utilidades/clases/Usuario.php';

/*
 * Paging
 */
$begin = "";
$limit = "";
if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $begin = mysql_real_escape_string($_GET['iDisplayStart']);
    $limit = mysql_real_escape_string($_GET['iDisplayLength']);
}


/*
 * Ordering
 */
$field = "";
$order = "";
if (isset($_GET['iSortCol_0'])) {
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $field = intval($_GET['iSortCol_' . $i]) == 0 ? "u.nombre_usuario" : "fecha_inv";
            $order = mysql_real_escape_string($_GET['sSortDir_' . $i]);
        }
    }
}


/*
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
$where = "";
if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
    $where = mysql_real_escape_string($_GET['sSearch']);
}




/*
 * SQL queries
 * Get data to display
 */
$result = GestionRedSocial::recuperarInvitacionesL($begin, $limit, $field, $order, $where);

/* Data set length after filtering */
$iFilteredTotal = GestionRedSocial::totalFilteredInv($where);

/* Total data s$aResultTotalet length */
$iTotal = GestionRedSocial::totalInvitaciones();


/*
 * Output
 */
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < count($result); $i++) {
    $u = new Usuario();
    $u = $result[$i][0];
    
    $date2 = explode('-', $result[$i][1]);
    $date = $date2[2] . '/' . $date2[1] . '/' . $date2[0];

    $row = array();
    $row[] = "<a href=\"$raiz/user/user_details.php?id=" . $u->getId() . "\" >" . $u->getNombre() . "</a>";
    $row[] = $date;
    $row[] = "<a href=\"#\" onclick=\"accept('" . $u->getNombre() . "', '" . $result[$i][2] . "')\"><i class=\"icon-ok\"></i></a>";
    $row[] = "<a href=\"#\" onclick=\"reject('" . $u->getNombre() . "', '" . $result[$i][2] . "')\"><i class=\"icon-remove\"></i></a>";
    $output['aaData'][] = $row;
}

echo json_encode($output);
?>