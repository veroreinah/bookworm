<?php
require_once '../utilidades/GestionTematica.php';
require_once '../utilidades/clases/Tematica.php';

/*
 * Paging
 */
$begin = "";
$limit = "";
if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $begin = mysql_real_escape_string($_GET['iDisplayStart']);
    $limit = mysql_real_escape_string($_GET['iDisplayLength']);
}


/*
 * Ordering
 */
$field = "";
$order = "";
if (isset($_GET['iSortCol_0'])) {
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $field = intval($_GET['iSortCol_' . $i]) == 0 ? "descripcion_tematica" : "";
            $order = mysql_real_escape_string($_GET['sSortDir_' . $i]);
        }
    }
}


/*
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
$where = "";
if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
    $where = mysql_real_escape_string($_GET['sSearch']);
}




/*
 * SQL queries
 * Get data to display
 */
$result = GestionTematica::recuperarTematicasL($begin, $limit, $field, $order, $where);

/* Data set length after filtering */
$iFilteredTotal = GestionTematica::totalFiltered($where);

/* Total data s$aResultTotalet length */
$iTotal = GestionTematica::totalTematicas();


/*
 * Output
 */
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < count($result); $i++) {
    $t = new Tematica();
    $t = $result[$i];

    $row = array();
    $row[] = "<span class=\"" . $t->getId() . "\">" . $t->getDescripcion() . "</span>";
    $row[] = "<a class=\"pencil\" id=\"pencil01\" href=\"#\"><i class=\"icon-pencil\"></i></a>";
    $row[] = "<a class=\"remove\" id=\"remove01\" href=\"#\"><i class=\"icon-remove\"></i></a>";
    $output['aaData'][] = $row;
}

echo json_encode($output);
?>