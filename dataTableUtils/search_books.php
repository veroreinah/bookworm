<?php
session_start();
require_once '../utilidades/clases/Libro.php';
require_once '../utilidades/clases/Autor.php';
require_once '../utilidades/GestionBusqueda.php';
require_once '../utilidades/constant.php';
global $raiz;

$data = $_SESSION["data_books"];
$filtro = $data["filtro"];

/*
 * Paging
 */
$begin = "";
$limit = "";
if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $begin = mysql_real_escape_string($_GET['iDisplayStart']);
    $limit = mysql_real_escape_string($_GET['iDisplayLength']);
}

/*
 * SQL queries
 * Get data to display
 */
switch ($filtro) {
    case 'titulo':
        $result = GestionBusqueda::buscarLibro($begin, $limit, $data["titulo"]);
        break;
    case 'isbn':
        $result = GestionBusqueda::buscarLibroPorIsbn($begin, $limit, $data["isbn"]);
        break;
    case 'autor':
        $result = GestionBusqueda::buscarLibrosPorAutor($begin, $limit, $data["autor"]);
        break;
    case 'tematica':
        $result = GestionBusqueda::buscarLibrosPorTematica($begin, $limit, $data["tematica"]);
        break;
    case 'avanzada':
        $result = GestionBusqueda::buscarLibrosAvanzada($begin, $limit, $data["titulo"], $data["subtitulo"], $data["publicacion"], $data["editorial"], $data["tematica"], $data["autor"]);
        break;
    default:
        break;
}

/* Data set length after filtering */
$iFilteredTotal = $result["total"];

/* Total data s$aResultTotalet length */
$iTotal = $result["total"];


/*
 * Output
 */
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < (count($result) - 1); $i++) {
    $l = new Libro();
    $l = $result[$i];
    $a = array();
    $a = $l->getAutores();
    
    $autores = array();
    for($j=0; $j < count($a); $j++) {
        $autor = new Autor();
        $autor = $a[$j];
        
        $autores[] = $autor->getNombre();
    }
    
    $autores = implode(', ', $autores);

    $row = array();
    $row[] = "<img src=\"$raiz/img/portadas/" . $l->getPortada() . "\" style=\"height: 40px;\" />";
    $row[] = $l->getIsbn();
    $row[] = $l->getTitulo();
    $row[] = $autores;
    $row[] = "<a class=\"details\" id=\"details01\" href=\"$raiz/data/details.php?elem=s_book&isbn=" . $l->getIsbn() . "\"><i class=\"icon-zoom-in\"></i></a>";
    $output['aaData'][] = $row;
}

echo json_encode($output);
?>
