<?php

session_start();
require_once '../utilidades/GestionUsuarios.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/constant.php';
global $raiz;

$id = 0;
if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == "admin") {
    $user = new Usuario();
    $user = unserialize($_SESSION["usuario"]);
    $id = $user->getId();
}

/*
 * Paging
 */
$begin = "";
$limit = "";
if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $begin = mysql_real_escape_string($_GET['iDisplayStart']);
    $limit = mysql_real_escape_string($_GET['iDisplayLength']);
}


/*
 * Ordering
 */
$field = "";
$order = "";
if (isset($_GET['iSortCol_0'])) {
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $field = intval($_GET['iSortCol_' . $i]) == 0 ? "nombre_usuario" : "email";
            $order = mysql_real_escape_string($_GET['sSortDir_' . $i]);
        }
    }
}


/*
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
$where = "";
if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
    $where = mysql_real_escape_string($_GET['sSearch']);
}




/*
 * SQL queries
 * Get data to display
 */
$result = GestionUsuarios::recuperarUsuariosL($begin, $limit, $field, $order, $where, $id);

/* Data set length after filtering */
$iFilteredTotal = GestionUsuarios::totalFiltered($where, $id);

/* Total data s$aResultTotalet length */
$iTotal = GestionUsuarios::totalUsuarios($id);


/*
 * Output
 */
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < count($result); $i++) {
    $u = new Usuario();
    $u = $result[$i];

    $row = array();
    $row[] = "<span class=\"" . $u->getId() . "\">" . $u->getNombre() . "</span>";
    $row[] = "<span class=\"" . $u->getId() . "\">" . $u->getEmail() . "</span>";
    $row[] = "<a class=\"details\" id=\"details01\" href=\"$raiz/user/user_details.php?id=" . $u->getId() . "\"><i class=\"icon-zoom-in\"></i></a>";
    $row[] = "<a class=\"remove\" id=\"remove01\" href=\"#\"><i class=\"icon-remove\"></i></a>";

    $output['aaData'][] = $row;
}

echo json_encode($output);
?>