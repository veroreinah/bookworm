<?php
session_start();
require_once '../utilidades/GestionRedSocial.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/constant.php';
global $raiz;

/*
 * Paging
 */
$begin = "";
$limit = "";
if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $begin = mysql_real_escape_string($_GET['iDisplayStart']);
    $limit = mysql_real_escape_string($_GET['iDisplayLength']);
}


/*
 * Ordering
 */
$field = "";
$order = "";
if (isset($_GET['iSortCol_0'])) {
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $field = intval($_GET['iSortCol_0']) == 0 ? "u.nombre_usuario" : "";
            $order = mysql_real_escape_string($_GET['sSortDir_' . $i]);
        }
    }
}


/*
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
$where = "";
if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
    $where = mysql_real_escape_string($_GET['sSearch']);
}




/*
 * SQL queries
 * Get data to display
 */
$result = GestionRedSocial::recuperarAmigosL($begin, $limit, $field, $order, $where);

/* Data set length after filtering */
$iFilteredTotal = GestionRedSocial::totalFiltered($where);

/* Total data s$aResultTotalet length */
$iTotal = GestionRedSocial::totalAmigos();


/*
 * Output
 */
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < count($result); $i++) {
    $u = new Usuario();
    $u = $result[$i];

    $row = array();
    $row[] = $u->getNombre();
    $row[] = "<a class=\"details\" id=\"details01\" href=\"#\" onclick=\"unfollow(" . $u->getId() . ", '" . $u->getNombre() . "')\"><i class=\"icon-remove\"></i></a>";
    $row[] = "<a class=\"details\" id=\"details01\" href=\"$raiz/user/user_details.php?id=" . $u->getId() . "\"><i class=\"icon-zoom-in\"></i></a>";
    $output['aaData'][] = $row;
}

echo json_encode($output);
?>