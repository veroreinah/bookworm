<?php
session_start();
require_once '../utilidades/GestionBusqueda.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/constant.php';
global $raiz;

$data = $_SESSION["data_users"];
$filtro = $data["filtro"];

/*
 * Paging
 */
$begin = "";
$limit = "";
if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $begin = mysql_real_escape_string($_GET['iDisplayStart']);
    $limit = mysql_real_escape_string($_GET['iDisplayLength']);
}

/*
 * SQL queries
 * Get data to display
 */

switch ($filtro) {
    case 'nombre':
        $result = GestionBusqueda::buscarUsuario($begin, $limit, $data["nombre"]);
        break;
    case 'email':
        $result = GestionBusqueda::buscarUsuarioPorEmail($begin, $limit, $data["email"]);
        break;
    default:
        break;
}

/* Data set length after filtering */
$iFilteredTotal = $result["total"];

/* Total data s$aResultTotalet length */
$iTotal = $result["total"];


/*
 * Output
 */
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < (count($result) - 1); $i++) {
    $u = new Usuario();
    $u = $result[$i];

    $row = array();
    $row[] = $u->getNombre();
    $row[] = "<a class=\"details\" id=\"details01\" href=\"$raiz/data/details.php?elem=s_user&id=" . $u->getId() . "\"><i class=\"icon-zoom-in\"></i></a>";

    $output['aaData'][] = $row;
}

echo json_encode($output);
?>
