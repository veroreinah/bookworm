<?php
require_once '../utilidades/GestionEditoriales.php';
require_once '../utilidades/clases/Editorial.php';

/*
 * Paging
 */
$begin = "";
$limit = "";
if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $begin = mysql_real_escape_string($_GET['iDisplayStart']);
    $limit = mysql_real_escape_string($_GET['iDisplayLength']);
}


/*
 * Ordering
 */
$field = "";
$order = "";
if (isset($_GET['iSortCol_0'])) {
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $field = intval($_GET['iSortCol_' . $i]) == 0 ? "nombre_editorial" : "";
            $order = mysql_real_escape_string($_GET['sSortDir_' . $i]);
        }
    }
}


/*
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
$where = "";
if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
    $where = mysql_real_escape_string($_GET['sSearch']);
}




/*
 * SQL queries
 * Get data to display
 */
$result = GestionEditoriales::recuperarEditorialesL($begin, $limit, $field, $order, $where);

/* Data set length after filtering */
$iFilteredTotal = GestionEditoriales::totalFiltered($where);

/* Total data s$aResultTotalet length */
$iTotal = GestionEditoriales::totalEditoriales();


/*
 * Output
 */
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < count($result); $i++) {
    $e = new Editorial();
    $e = $result[$i];

    $row = array();
    $row[] = "<span class=\"" . $e->getId() . "\">" . $e->getNombre() . "</span>";
    $row[] = "<a class=\"pencil\" id=\"pencil01\" href=\"#\"><i class=\"icon-pencil\"></i></a>";
    $row[] = "<a class=\"remove\" id=\"remove01\" href=\"#\"><i class=\"icon-remove\"></i></a>";
    $output['aaData'][] = $row;
}

echo json_encode($output);
?>