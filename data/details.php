<?php
session_start();
require_once '../utilidades/constant.php';
global $raiz;

$filtro = $_GET["elem"];

switch ($filtro) {
    case 's_user':
        $_SESSION["data_users_page"] = "1";
        header('location:' . $raiz . '/user/user_details.php?id=' . $_GET["id"]);
        break;
    case 's_book':
        $_SESSION["data_books_page"] = "1";
        header('location:' . $raiz . '/book_details.php?isbn=' . $_GET["isbn"]);
        break;
    case 'register':
        $_SESSION["data_books_page"] = "1";
        header('location:' . $raiz . '/register.php');
        break;
    default:
        break;
}
?>
