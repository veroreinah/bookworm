<?php
session_start();
require_once 'utilidades/filter.php';
require_once 'utilidades/constant.php';
global $raiz;
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm - Contacta con nosotros</title>
        <?php require_once 'parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/login-validation.js"></script>
        <script>
            function validar() {
                error = false;
                //Mensaje
                if (jQuery("#textarea").val() == '') {
                    jQuery("#textarea").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text03").html("Campo obligatorio");
                    error = true;
                } else {
                    jQuery("#textarea").attr("style", "border: 1px solid #CCC;");
                    jQuery("#help-inline-text03").html("");
                }
                
                //Email
                if (jQuery("#input02").val() == '') {
                    jQuery("#input02").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text02").html("Campo obligatorio");
                    error = true;
                } else {
                    jQuery("#input02").attr("style", "border: 1px solid #CCC;");
                    jQuery("#help-inline-text02").html("");
                    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(jQuery("#input02").val())){
                        jQuery("#input02").attr("style", "border: 1px solid #CCC;");
                        jQuery("#help-inline-text02").html("");
                    } else {
                        jQuery("#input02").attr("style", "border: 1px solid #D90416");
                        jQuery("#help-inline-text02").html("Email incorrecto");
                        error = true;
                    }
                }

                //Si todo "ok", enviar formulario
                if (!error) {
                    document.contact_form.submit();
                }
            }
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "contact.php" ?>
            <?php require_once 'parts/menu.php'; ?>
            <?php require_once 'parts/carousel.php'; ?>
            <?php require_once 'parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <div class="contact-content">
                       <?php if (isset($_SESSION["noEnviado"])) { ?>
                            <div class="alert alert-error" style="margin: 0px; margin-bottom: 15px;">
                                <a class="close" data-dismiss="alert">×</a>
                                <?php
                                echo "<strong>" . $_SESSION["noEnviado"] . "</strong>";
                                session_unregister("noEnviado");
                                ?>
                            </div>
                        <?php } ?>
                       <?php if (isset($_SESSION["enviado"])) { ?>
                            <div class="alert alert-success" style="margin: 0px; margin-bottom: 15px;">
                                <a class="close" data-dismiss="alert">×</a>
                                <?php
                                echo "<strong>" . $_SESSION["enviado"] . "</strong>";
                                session_unregister("enviado");
                                ?>
                            </div>
                        <?php } ?>
                        <form class="form-horizontal" action="<?php echo $raiz; ?>/forms/contact.php" method="post" name="contact_form">
                            <fieldset>
                                <legend>Contacta con nosotros</legend>
                                <div class="control-group">
                                    <label class="control-label" for="input01">Nombre</label>
                                    <div class="controls">
                                        <input type="text" class="input-xlarge" id="input01" name="nombre">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input02">Email *</label>
                                    <div class="controls">
                                        <input type="text" class="input-xlarge" id="input02" name="email">
                                        <span id="help-inline-text02" class="help-inline" style="color: #B94A48;"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="textarea">Mensaje *</label>
                                    <div class="controls">
                                        <textarea class="input-xlarge" id="textarea" rows="7" name="mensaje"></textarea>
                                        <span id="help-inline-text03" class="help-inline" style="color: #B94A48;"></span>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <a href="#" class="btn" onclick="validar()"><i class="icon-envelope"></i> Enviar</a>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>

                <!-- SIDEBAR -->
                <?php require_once 'parts/sidebar.php'; ?>
            </div>

            <?php require_once 'parts/footer.php'; ?>
        </div>
    </body>
</html>