<?php
session_start();
require_once '../utilidades/GestionAutores.php';
require_once '../utilidades/clases/Autor.php';
require_once '../utilidades/constant.php';
global $raiz;

$autor = new Autor();
$autor->setNombre($_POST["nombre"]);

GestionAutores::crearAutor($autor);

header('location:' . $raiz . '/admin/authors.php');
?>
