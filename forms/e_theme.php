<?php
session_start();
require_once '../utilidades/GestionTematica.php';
require_once '../utilidades/constant.php';
global $raiz;

$i = GestionTematica::eliminarTematica($_GET["id"]);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al eliminar la temática. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["eliminada"] = "La temática se ha eliminado.";
}
header('location:' . $raiz . '/admin/theme.php');
?>
