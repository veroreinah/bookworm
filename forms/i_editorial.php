<?php
session_start();
require_once '../utilidades/GestionEditoriales.php';
require_once '../utilidades/clases/Editorial.php';
require_once '../utilidades/constant.php';
global $raiz;

$editorial = new Editorial();
$editorial->setNombre($_POST["nombre"]);

GestionEditoriales::crearEditorial($editorial);

header('location:' . $raiz . '/admin/editorial.php');
?>
