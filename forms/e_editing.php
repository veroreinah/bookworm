<?php
session_start();
require_once '../utilidades/GestionTipoEdicion.php';
require_once '../utilidades/constant.php';
global $raiz;

$i = GestionTipoEdicion::eliminarTipoEdicion($_GET["id"]);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al eliminar el tipo de edición. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["eliminada"] = "El tipo de edición se ha eliminado.";
}
header('location:' . $raiz . '/admin/editing.php');
?>

