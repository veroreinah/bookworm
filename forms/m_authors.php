<?php
session_start();
require_once '../utilidades/GestionAutores.php';
require_once '../utilidades/clases/Autor.php';
require_once '../utilidades/constant.php';
global $raiz;

$a = new Autor();
$a->setId($_POST["id"]);
$a->setNombre($_POST["nombre"]);

$i = GestionAutores::modificarAutor($a);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al modificar el autor. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["modificada"] = "El autor se ha modificado.";
}
header('location:' . $raiz . '/admin/authors.php');
?>
