<?php
session_start();
require_once '../utilidades/GestionCitas.php';
require_once '../utilidades/GestionAutores.php';
require_once '../utilidades/clases/Cita.php';
require_once '../utilidades/clases/Autor.php';
require_once '../utilidades/constant.php';
global $raiz;

$c = new Cita();
$c->setId($_POST["id"]);
$c->setTexto($_POST["texto"]);

$a = new Autor();
if ($_POST["idAutor"] == 0) {
    $autor = new Autor();
    $autor->setNombre($_POST["autor"]);
    $a->setId(GestionAutores::crearAutor($autor));
} else {
    $a->setId($_POST["idAutor"]);
}

$c->setAutor($a);

$i = GestionCitas::modificarCita($c);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al modificar la cita. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["modificada"] = "La cita se ha modificado.";
}
header('location:' . $raiz . '/admin/quotes.php');
?>
