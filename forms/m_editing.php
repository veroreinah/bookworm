<?php
session_start();
require_once '../utilidades/GestionTipoEdicion.php';
require_once '../utilidades/clases/TipoEdicion.php';
require_once '../utilidades/constant.php';
global $raiz;

$tipoEd = new TipoEdicion();
$tipoEd->setId($_POST["id"]);
$tipoEd->setDescripcion($_POST["nombre"]);

$i = GestionTipoEdicion::modificarTipoEdicion($tipoEd);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al modificar el tipo de edición. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["modificada"] = "El tipo de edición se ha modificado.";
}
header('location:' . $raiz . '/admin/editing.php');
?>
