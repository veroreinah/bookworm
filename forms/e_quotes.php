<?php
session_start();
require_once '../utilidades/GestionCitas.php';
require_once '../utilidades/constant.php';
global $raiz;

$i = GestionCitas::eliminarCita($_GET["id"]);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al eliminar la cita. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["eliminada"] = "La cita se ha eliminado.";
}
header('location:' . $raiz . '/admin/quotes.php');
?>
