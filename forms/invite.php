<?php
session_start();
require_once '../utilidades/GestionRedSocial.php';
require_once '../utilidades/GestionUsuarios.php';
require_once '../utilidades/clases/Invitacion.php';
require_once '../utilidades/constant.php';
global $raiz;

if (GestionUsuarios::existeUsuario($_POST["email"])) {
    $_SESSION["userExists"] = "Parece que " . $_POST["email"] .  " ya es usuario de BookWorm. Haz clic en \"Seguir en BookWorm\" para estar al tanto de sus lecturas.";
    $_SESSION["userEmail"] = $_POST["email"];
    header('location:' . $raiz . '/user/user_details.php');
} else {
    $u = new Usuario();
    $u = unserialize($_SESSION["usuario"]);

    $invitacion = new Invitacion();
    $invitacion->setEmail($_POST["email"]);
    $invitacion->setTexto($_POST["mensaje"]);
    $invitacion->setUsuario($u);

    $enviado = GestionRedSocial::invitarPorEmail($invitacion);
    
    if ($enviado === 1) {
        $_SESSION["invitacion"] = "Tu invitación se ha enviado correctamente.";
    } else {
        $_SESSION["invitacionNo"] = "Vaya, parece que ha habido un error al intentar enviar tu invitación. Inténtalo de nuevo más tarde.";
    }
    
    header('location:' . $_SERVER['HTTP_REFERER']);
}
?>