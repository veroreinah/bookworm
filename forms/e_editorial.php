<?php
session_start();
require_once '../utilidades/GestionEditoriales.php';
require_once '../utilidades/constant.php';
global $raiz;

$i = GestionEditoriales::eliminarEditorial($_GET["id"]);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al eliminar la editorial. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["eliminada"] = "La editorial se ha eliminado.";
}
header('location:' . $raiz . '/admin/editorial.php');
?>

