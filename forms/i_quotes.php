<?php
session_start();

require_once '../utilidades/GestionCitas.php';
require_once '../utilidades/GestionAutores.php';
require_once '../utilidades/clases/Cita.php';
require_once '../utilidades/clases/Autor.php';
require_once '../utilidades/constant.php';
global $raiz;

$c = new Cita();
$c->setTexto($_POST["texto"]);

$a = new Autor();
if ($_POST["idAutor"] == 0) {
    $autor = new Autor();
    $autor->setNombre($_POST["autor"]);
    $a->setId(GestionAutores::crearAutor($autor));
} else {
    $a->setId($_POST["idAutor"]);
}

$c->setAutor($a);

GestionCitas::crearCita($c);

header('location:' . $raiz . '/admin/quotes.php');
?>
