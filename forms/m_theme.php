<?php
session_start();
require_once '../utilidades/GestionTematica.php';
require_once '../utilidades/clases/Tematica.php';
require_once '../utilidades/constant.php';
global $raiz;

$tematica = new Tematica();
$tematica->setId($_POST["id"]);
$tematica->setDescripcion($_POST["nombre"]);

$i = GestionTematica::modificarTematica($tematica);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al modificar la editorial. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["modificada"] = "La editorial se ha modificado.";
}
header('location:' . $raiz . '/admin/theme.php');
?>
