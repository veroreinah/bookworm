<?php
session_start();

require_once '../utilidades/GestionLibros.php';
require_once '../utilidades/GestionAutores.php';
require_once '../utilidades/GestionEditoriales.php';
require_once '../utilidades/GestionTipoEdicion.php';
require_once '../utilidades/clases/Libro.php';
require_once '../utilidades/clases/Autor.php';
require_once '../utilidades/clases/Editorial.php';
require_once '../utilidades/clases/TipoEdicion.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/constant.php';
global $raiz;

$u = new Usuario();
$u = unserialize($_SESSION["usuario"]);

$l = new Libro();
$l->setIsbn($_POST["isbn"]);
$l->setTitulo($_POST["titulo"]);
$l->setUsuario($u);
$l->setSubtitulo($_POST["subtitulo"]);
$l->setSinopsis($_POST["sinopsis"]);
$l->setPaginas($_POST["paginas"]);
$l->setAnyoPublicacion($_POST["publicacion"]);
$l->setEdicion($_POST["edicion"]);

if ($_SERVER['SERVER_NAME'] == 'localhost') {
    $pre = "C:/AppServ/www/";
} else {
    $pre = "/var/fog/apps/app43064/bookworm.phpfogapp.com/";
}
$uploads_dir = $pre . $raiz . '/img/portadas';
if (isset($_FILES["portada"]["tmp_name"]) && $_FILES["portada"]["tmp_name"] != "") {
    $tmp_name = $_FILES["portada"]["tmp_name"];
    $name2 = $_FILES["portada"]["name"];
    $ext = pathinfo($name2, PATHINFO_EXTENSION);
    $name = $_POST["isbn"] . ".$ext";
    move_uploaded_file($tmp_name, "$uploads_dir/$name");
    
    $l->setPortada($name);
} else {
    $l->setPortada("");
}

if (trim($_POST["nombre_editorial"]) != '') {
    $editorial = new Editorial();
    if ($_POST['editorial'] == 0) {
        $where = " where nombre_editorial = '" . $_POST["nombre_editorial"] . "' ";
        $id = GestionLibros::existe('id_editorial', 't_editoriales', $where);
        if (intval($id) == 0) {
            $ed = new Editorial();
            $ed->setNombre($_POST["nombre_editorial"]);
            $editorial->setId(GestionEditoriales::crearEditorial($ed));
        } else {
            $editorial->setId($id);
        }
    } else {
        $editorial->setId($_POST['editorial']);
    }
    $l->setEditorial($editorial);
} else {
    $l->setEditorial("");
}

if (trim($_POST["nombre_tipo_edicion"]) != '') {
    $t = new TipoEdicion();
    if ($_POST['tipo_edicion'] == 0) {
        $where = " where descripcion_edicion = '" . $_POST["nombre_tipo_edicion"] . "' ";
        $id = GestionLibros::existe('id_tipo_edicion', 't_tipo_edicion', $where);
        if (intval($id) == 0) {
            $tipoEd = new TipoEdicion();
            $tipoEd->setDescripcion($_POST["nombre_tipo_edicion"]);
            $t->setId(GestionTipoEdicion::crearTipoEdicion($tipoEd));
        } else {
            $t->setId($id);
        }
    } else {
        $t->setId($_POST['tipo_edicion']);
    }
    $l->setTipoEdicion($t);
} else {
    $l->setTipoEdicion("");
}


if (trim($_POST["autores"]) != '') {
    $autores = array();
    $nuevos = array();
    $autores = explode('-', $_POST["autores"]);
    $nuevos = explode(';', $_POST["nuevos_autores"]);
    
    $as = array();
    $i = 0;
    foreach ($autores as $aut) {
        $aut = trim($aut);
        if ($aut != null && $aut != '') {
            $a = new Autor();
            if (preg_match('#0\_[0-9]+x$#', $aut)) {
                $where = " where nombre_autor = '" . $nuevos[$i] . "' ";
                $id = GestionLibros::existe('id_autor', 't_autores', $where);
                if (intval($id) == 0) {
                    $autor = new Autor();
                    $autor->setNombre($nuevos[$i]);
                    $a->setId(GestionAutores::crearAutor($autor));
                } else {
                    $a->setId($id);
                }
                
                $i++;
            } else {
                $a->setId($aut);
            }
            $as[] = $a;
        }
    }
    $l->setAutores($as);
}

if (!empty($_POST["tematicas"])) {
    $tematicas = array();
    foreach ($_POST["tematicas"] as $t) {
        $tem = new Tematica();
        $tem->setId($t);
        
        $tematicas[] = $tem;
    }
    $l->setTematicas($tematicas);
}

$i = GestionLibros::modificarLibro($l);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al modificar el libro. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["modify"] = "Datos modificados correctamente.";
}
header('location:' . $raiz . '/book_details.php?isbn=' . $_POST["isbn"]);
?>
