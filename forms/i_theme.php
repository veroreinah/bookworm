<?php
session_start();
require_once '../utilidades/GestionTematica.php';
require_once '../utilidades/clases/Tematica.php';
require_once '../utilidades/constant.php';
global $raiz;

$tematica = new Tematica();
$tematica->setDescripcion($_POST["nombre"]);

GestionTematica::crearTematica($tematica);

header('location:' . $raiz . '/admin/theme.php');
?>
