<?php
session_start();
require_once '../utilidades/constant.php';
require_once '../lib/Swift-4.1.7/lib/swift_required.php';
global $email;

set_time_limit(-1);

$body = "<html><body>";
if (isset($_POST["nombre"]) && $_POST["nombre"] != "") {
    $body .= "Nombre: " . $_POST["nombre"] . "<br /><br />";
}

$body .= "Email: <a href='mailto:" . $_POST["email"] . "'>". $_POST["email"] ."</a><br /><br />";
$body .= "Mensaje: <em>" . $_POST["mensaje"] . "</em>";
$body .= "</body></html>";

// Create a message
$message = Swift_Message::newInstance()

  // Give the message a subject
  ->setSubject('Mensaje desde BookWorm')

  // Set the From address with an associative array
  ->setFrom(array($email))

  // Set the To addresses with an associative array
  ->setTo(array($email))

  // Give it a body
  ->setBody($body, 'text/html')
  ;
  
$password = getenv('EMAIL_PASSWORD');
$password = str_replace('\\', '', $password);

// Create the Transport
$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
  ->setUsername($email)
  ->setPassword($password)
  ;

// Create the Mailer using your created Transport
$mailer = Swift_Mailer::newInstance($transport);

// Send the message
$result = $mailer->send($message);

if ($result) {
    $_SESSION["enviado"] = "Tu mensaje se ha enviado correctamente. ¡Muchas gracias!";
} else {
    $_SESSION["noEnviado"] = "Parece que ha habido algún error al enviar el mensaje. Inténtalo de nuevo más tarde.";
}
header('location:../contact.php');
?>
