<?php
session_start();
require_once '../utilidades/GestionUsuarios.php';
require_once '../utilidades/constant.php';
global $raiz;

$i = GestionUsuarios::eliminarUsuario($_GET["id"]);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al eliminar el usuario. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["eliminada"] = "El usuario se ha eliminado.";
}
header('location:' . $raiz . '/admin/users.php');
?>
