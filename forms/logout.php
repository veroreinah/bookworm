<?php
session_start();
require_once '../utilidades/GestionAcceso.php';
require_once '../utilidades/constant.php';
global $raiz;

GestionAcceso::logout();

header('location:' . $raiz . '/index.php');
?>
