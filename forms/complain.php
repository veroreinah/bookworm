<?php
session_start();
require_once '../utilidades/GestionDenuncias.php';
require_once '../utilidades/clases/Denuncia.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/clases/Libro.php';
require_once '../utilidades/constant.php';
global $raiz;

$u = new Usuario();
$u = unserialize($_SESSION["usuario"]);
$libro = new Libro();
$libro->setIsbn($_POST["isbn"]);

$denuncia = new Denuncia();
$denuncia->setUsuario($u);
$denuncia->setLibro($libro);
$denuncia->setTexto($_POST["mensaje"]);
$denuncia->setFecha(date("Y-m-d"));

GestionDenuncias::crearDenuncia($denuncia);

$_SESSION["thanksComp"] = "Gracias por colaborar en la mejora de BookWorm.";
$_SESSION["isbnComp"] = $_POST["isbn"];
header('location:' . $raiz . '/user/complain.php');
?>

