<?php
session_start();
require_once '../utilidades/GestionTipoEdicion.php';
require_once '../utilidades/clases/TipoEdicion.php';
require_once '../utilidades/constant.php';
global $raiz;

$tipoEd = new TipoEdicion();
$tipoEd->setDescripcion($_POST["nombre"]);

GestionTipoEdicion::crearTipoEdicion($tipoEd);

header('location:' . $raiz . '/admin/editing.php');
?>
