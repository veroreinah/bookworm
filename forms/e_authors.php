<?php
session_start();
require_once '../utilidades/GestionAutores.php';
require_once '../utilidades/constant.php';
global $raiz;

$i = GestionAutores::eliminarAutor($_GET["id"]);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al eliminar el autor. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["eliminada"] = "El autor se ha eliminado.";
}
header('location:' . $raiz . '/admin/authors.php');
?>

