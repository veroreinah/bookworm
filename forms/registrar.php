<?php
session_start();
require_once '../utilidades/GestionAcceso.php';
require_once '../utilidades/GestionUsuarios.php';
require_once '../utilidades/GestionRedSocial.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/clases/TipoUsuario.php';
require_once '../utilidades/constant.php';
global $raiz;

$u = new Usuario();
$u->setNombre($_POST["nombre"]);
$u->setEmail($_POST["email"]);
$u->setPassword($_POST["pass"]);

if ($_SERVER['SERVER_NAME'] == 'localhost') {
    $pre = "C:/AppServ/www/";
} else {
    $pre = "/var/fog/apps/app43064/bookworm.phpfogapp.com/";
}
$uploads_dir = $pre . $raiz . '/img/users';
if (isset($_FILES["imagen"]["tmp_name"]) && $_FILES["imagen"]["tmp_name"] != "") {
    $tmp_name = $_FILES["imagen"]["tmp_name"];
    $name2 = $_FILES["imagen"]["name"];
    $ext = pathinfo($name2, PATHINFO_EXTENSION);
    $name = uniqid() . ".$ext";
    move_uploaded_file($tmp_name, "$uploads_dir/$name");
    
    $u->setImagen($name);
} else {
    $name = "default.JPG";
    $u->setImagen($name);
}
if ($_POST["fecha_nac"] == "") {
    $u->setFechaNac("null");
} else {
    $date2 = explode("/", $_POST["fecha_nac"]);
    $date = $date2[2] . '-' . $date2[1] . '-' . $date2[0];
    $u->setFechaNac($date);
}
if ($_POST["facebook"] == "") {
    $u->setFacebook("null");
} else {
    $u->setFacebook($_POST["facebook"]);
}

if (isset($_POST["tipo"])) {
    $t = new TipoUsuario();
    $t->setId($_POST["tipo"]);
    
    $u->setTipoUsuario($t);
}

if (isset($_SESSION["usuario"])) {
    GestionUsuarios::insertarUsuario($u);
    header('location:' . $raiz . '/admin/users.php');
} else {
    $id = GestionAcceso::registrarse($u);
    if ($_POST["inv"] != 0 || $_POST["inv"] != '0') {
        $usuario = new Usuario();
        $usuario->setId($id);
        $usuarioInvita = new Usuario();
        $usuarioInvita->setId(GestionRedSocial::recuperarUsuarioInvita($_POST["inv"]));

        GestionRedSocial::responderInvitacionEmail($_POST["inv"], $usuarioInvita, $usuario);
    }
    GestionAcceso::login($_POST["email"], $_POST["pass"]);
    $_SESSION["bienvenido"] = "Bienvenido a BookWorm.";
    header('location:' . $raiz . '/index.php');
}
?>
