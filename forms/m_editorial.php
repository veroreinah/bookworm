<?php
session_start();
require_once '../utilidades/GestionEditoriales.php';
require_once '../utilidades/clases/Editorial.php';
require_once '../utilidades/constant.php';
global $raiz;

$editorial = new Editorial();
$editorial->setId($_POST["id"]);
$editorial->setNombre($_POST["nombre"]);

$i = GestionEditoriales::modificarEditorial($editorial);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al modificar la editorial. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["modificada"] = "La editorial se ha modificado.";
}
header('location:' . $raiz . '/admin/editorial.php');
?>
