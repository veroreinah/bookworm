<?php
session_start();
require_once '../utilidades/GestionLibros.php';
require_once '../utilidades/constant.php';
global $raiz;

$i = GestionLibros::eliminarLibro($_GET["isbn"]);

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al eliminar el libro. Inténtelo de nuevo más tarde.";
} else {
    $_SESSION["eliminada"] = "El libro se ha eliminado.";
}
header('location:' . $raiz . '/admin/books.php');
?>
