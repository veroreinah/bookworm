<?php
session_start();
require_once '../utilidades/GestionLibros.php';
require_once '../utilidades/constant.php';
global $raiz;

$u = new Usuario();
$u = unserialize($_SESSION["usuario"]);

if (GestionLibros::existeLibro($_POST["isbn"])) {
    GestionLibros::agregarLibro($u->getId(), $_POST["isbn"]);
    header('location:' . $raiz . '/book_details.php?isbn=' . $_POST["isbn"]);
} else {
    $_SESSION["addBook"] = "Parece que todavía no existe ese libro en BookWorm. Ayúdanos a completar las estanterías añadiéndolo.";
    header('location:' . $raiz . '/user/new_book.php?isbn=' . $_POST["isbn"]);
}
?>
