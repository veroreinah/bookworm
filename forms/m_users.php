<?php
session_start();
require_once '../utilidades/GestionAcceso.php';
require_once '../utilidades/GestionUsuarios.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/clases/TipoUsuario.php';
require_once '../utilidades/constant.php';
global $raiz;

$u = new Usuario();
$u->setId($_POST["id"]);
$u->setNombre($_POST["nombre"]);
$u->setEmail($_POST["email"]);

if ($_POST["oldPass"] == "") {
    $u->setPassword("");
} else {
    $u->setPassword($_POST["pass"]);
}

if ($_SERVER['SERVER_NAME'] == 'localhost') {
    $pre = "C:/AppServ/www/";
} else {
    $pre = "/var/fog/apps/app43064/bookworm.phpfogapp.com/";
}
$uploads_dir = $pre . $raiz . '/img/users';
if (isset($_FILES["imagen"]["tmp_name"]) && $_FILES["imagen"]["tmp_name"] != "") {
    $tmp_name = $_FILES["imagen"]["tmp_name"];
    $name2 = $_FILES["imagen"]["name"];
    $ext = pathinfo($name2, PATHINFO_EXTENSION);
    $name = uniqid() . ".$ext";
    move_uploaded_file($tmp_name, "$uploads_dir/$name");
    
    $u->setImagen($name);
} else {
    $u->setImagen("");
}
if (trim($_POST["fecha_nac"]) == "") {
    
} else {
    $date2 = explode("/", $_POST["fecha_nac"]);
    $date = $date2[2] . '-' . $date2[1] . '-' . $date2[0];
    $u->setFechaNac($date);
}
if (trim($_POST["facebook"]) == "") {
    
} else {
    $u->setFacebook($_POST["facebook"]);
}

if ($_SESSION["perfil"] == "perfil") {
    $i = GestionUsuarios::modificarUuario($u);
    if (intval($i) == -1 || intval($i) == 0) {
        $_SESSION["error"] = "Se ha producido un error al sus datos. Inténtelo de nuevo más tarde.";
    } else {
        $_SESSION["modify"] = "Sus datos se han modificado correctamente.";
    }
    header('location:' . $raiz . '/user/profile.php');
} else {
    $i = GestionUsuarios::modificarUuario($u);
    if (intval($i) == -1 || intval($i) == 0) {
        $_SESSION["error"] = "Se ha producido un error al modificar el usuario. Inténtelo de nuevo más tarde.";
    } else {
        $_SESSION["modify"] = "Datos modificados correctamente.";
    }
    header('location:' . $raiz . "/user/user_details.php?id=" . $_POST["id"]);
}
?>
