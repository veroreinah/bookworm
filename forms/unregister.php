<?php
session_start();
require_once '../utilidades/GestionUsuarios.php';
require_once '../utilidades/GestionAcceso.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/constant.php';
global $raiz;

$u = new Usuario();
$u = unserialize($_SESSION["usuario"]);

$i = GestionUsuarios::eliminarUsuario($u->getId());

if (intval($i) == -1 || intval($i) == 0) {
    $_SESSION["error"] = "Se ha producido un error al darse de baja. Inténtelo de nuevo más tarde.";
    header('location:' . $raiz . '/user/profile.php');
} else {
    GestionAcceso::logout();
    header('location:' . $raiz . '/index.php');
}
?>
