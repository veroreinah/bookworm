<?php
session_start();
require_once 'utilidades/filter.php';
require_once 'utilidades/GestionLibros.php';
require_once 'utilidades/clases/Libro.php';
require_once 'utilidades/clases/Autor.php';
require_once 'utilidades/clases/Editorial.php';
require_once 'utilidades/clases/TipoEdicion.php';
require_once 'utilidades/clases/Tematica.php';
require_once 'utilidades/clases/Usuario.php';
require_once 'utilidades/clases/Comentario.php';
require_once 'utilidades/constant.php';
global $raiz;

$isbn = array();
if (isset($_SESSION["usuario"])) {
    $isbn = GestionLibros::recuperarIsbnPorUsuario();
    $comentado = GestionLibros::tieneComentario($_GET["isbn"]);
    $comentarios = array();
    $comentarios = GestionLibros::recuperarComentarios($_GET["isbn"]);
}

$libro = new Libro();
if (in_array($_GET["isbn"], $isbn)) {
    $u = new Usuario();
    $u = unserialize($_SESSION["usuario"]);
    $libro = GestionLibros::recuperarLibro($_GET["isbn"], $u->getId());
} else {
    $libro = GestionLibros::recuperarLibro($_GET["isbn"]);
}

$autores = array();
$autores = $libro->getAutores();

$editorial = new Editorial();
if ($libro->getEditorial() == null) {
    $editorial = 0;
} else {
    $editorial = $libro->getEditorial();
}

$tipoEdicion = new TipoEdicion();
if ($libro->getTipoEdicion() == null) {
    $tipoEdicion = 0;
} else {
    $tipoEdicion = $libro->getTipoEdicion();
}

$tematicas = array();
$tematicas = $libro->getTematicas();
?>

<!DOCTYPE html>
<html>
    <head>
        <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'admin') { ?>
            <title>Libros - Panel de administración de BookWorm</title>
        <?php } else { ?>
            <title>BookWorm - Libros</title>
        <?php } ?>
        <?php require_once 'parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/login-validation.js"></script>
        <script>
            function addShelf(raiz) {
                isbn = jQuery('#isbn').val();
                jQuery.ajax({
                    dataType: "json",
                    type: "post",
                    url: raiz + "/ajax/addShelf.php",
                    data: {
                        isbn: isbn
                    },
                    success: function(data) {
                        if (data) {
                            location.reload();
                        }
                    }
                });
            }
            
            function removeShelf(raiz) {
                isbn = jQuery('#isbn').val();
                jQuery.ajax({
                    dataType: "json",
                    type: "post",
                    url: raiz + "/ajax/removeShelf.php",
                    data: {
                        isbn: isbn
                    },
                    success: function(data) {
                        if (data) {
                            location.reload();
                        }
                    }
                });
            }
            
            function addComment() {
                $('#commentModal').modal({
                    backdrop: "static",
                    show: true
                });
            }
            
            function enviarComentario(raiz) {
                error = false;
                isbn = jQuery('#isbn').val();
                titulo = jQuery('#input01').val();
                comentario = jQuery('#textarea').val();
                
                if (comentario == '') {
                    jQuery("#textarea").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text02").html("Campo obligatorio");
                    error = true;
                } else {
                    jQuery("#textarea").attr("style", "border: 1px solid #CCC");
                    jQuery("#help-inline-text02").html("");
                }
                
                if (jQuery("#optionsCheckbox").attr("checked") == 'checked') {
                    spoiler = 1;
                } else {
                    spoiler = 0;
                }
                
                if (!error) {
                    jQuery.ajax({
                        dataType: "json",
                        type: "post",
                        url: raiz + "/ajax/comment.php",
                        data: {
                            isbn: isbn,
                            titulo: titulo,
                            comentario: comentario,
                            spoiler: spoiler
                        },
                        success: function(data) {
                            if (data) {
                                location.reload();
                            }
                        }
                    });
                }
            }
            
            jQuery(document).ready(function() {
                jQuery('.valor').rating({
                    callback: function(value, link){
                        isbn = jQuery('#isbn').val();
                        jQuery.ajax({
                            dataType: "json",
                            type: "post",
                            url: "ajax/rating.php",
                            data: {
                                isbn: isbn,
                                valoracion: value
                            },
                            success: function(data) {
                                if (data) {
                                    location.reload();
                                }
                            }
                        });
                    }
                });
                
                <?php if (isset($_SESSION["comment"]) && $_SESSION["comment"] == 'OK') { ?>
                    $('.tabbable a[href="#2"]').tab("show");
                <?php session_unregister('comment');
                } ?>
            });
        </script>
    </head>
    <body>
        <div class="container">
            <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'admin') { ?>
                <?php $_SESSION["page"] = "admin/books.php" ?>
            <?php } elseif (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'usuario') { ?>
                <?php $_SESSION["page"] = "user/books.php" ?>
            <?php } else { ?>
                <?php $_SESSION["page"] = "books.php" ?>
            <?php } ?>
            <?php require_once 'parts/menu.php'; ?>
            <?php require_once 'parts/carousel.php'; ?>
            <?php require_once 'parts/infoRow.php'; ?>
            <?php $_SESSION["page"] = "book_details.php" ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab"><?php echo $libro->getTitulo(); ?></a></li>
                            <li><a href="#2" data-toggle="tab">Comentarios</a></li>
                        </ul>
                        <div class="tab-content">
                            <!-- PESTAÑA 1 -->
                                <div class="tab-pane active" id="1">
                                    <div align="right" style="margin: 15px;">
                                        <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'admin') { ?>
                                            <a href="<?php echo $raiz; ?>/user/book_modify.php?isbn=<?php echo $libro->getIsbn(); ?>" class="btn">
                                                <i class="icon-list-alt"></i> Mejorar datos</a>
                                        <?php } elseif(isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'usuario') { ?>
                                            <?php if (in_array($libro->getIsbn(), $isbn)) { ?>
                                                <a href="#" class="btn" onclick="removeShelf('<?php echo $raiz; ?>')">
                                                    <i class="icon-remove"></i> Eliminar de mi estantería</a>
                                            <?php } else { ?>
                                                <a href="#" class="btn" onclick="addShelf('<?php echo $raiz; ?>')">
                                                    <i class="icon-plus"></i> Agregar a mi estantería</a>
                                            <?php } ?>
                                            <a href="<?php echo $raiz; ?>/user/book_modify.php?isbn=<?php echo $libro->getIsbn(); ?>" class="btn">
                                                <i class="icon-list-alt"></i> Mejorar datos</a>
                                            <a href="<?php echo $raiz; ?>/user/complain.php?isbn=<?php echo $libro->getIsbn(); ?>" class="btn">
                                                <i class="icon-warning-sign"></i> Denunciar</a>
                                        <?php } else { ?>
                                        <div class="alert alert-success" style="clear: both; margin-left: 0px; margin-right: 0px; margin-bottom: 15px; text-align: justify;">
                                            <strong><a href="<?php echo $raiz ?>/register.php">Únete a Bookworm</a></strong> y podrás crear tu propia 
                                            estantería con éste y más libros, mejorar sus datos y ¡mucho más!
                                        </div>
                                        <?php } ?>
                                        <a href="<?php echo $_SESSION["data_books_page"] == "1" ? "$raiz/search_results.php" : "javascript:history.back()"?>" class="btn">
                                            <i class="icon-arrow-left"></i> Volver</a>
                                    </div>
                                    <hr>
                                <?php if (isset($_SESSION["modify"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo $_SESSION["modify"];
                                        session_unregister("modify");
                                        ?>
                                    </div>
                                <?php } ?>
                                <?php if (isset($_SESSION["modify"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo $_SESSION["modify"];
                                        session_unregister("modify");
                                        ?>
                                    </div>
                                <?php } ?>
                                <?php if (isset($_SESSION["error"])) {
                                    ?>
                                    <div class="alert alert-error">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php
                                        echo "<strong>" . $_SESSION["error"] . "</strong>";
                                        session_unregister("error");
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div style="margin: 15px;">
                                    <input id="isbn" value="<?php echo $libro->getIsbn(); ?>" type="hidden" />
                                    <legend><?php echo $libro->getTitulo(); ?></legend>
                                    <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'usuario') { ?>
                                        <div>
                                            <?php if (in_array($libro->getIsbn(), $isbn)) { ?>
                                                <?php for ($k = 1; $k < 11; $k++) { ?>
                                                    <input name="star" value="<?php echo $k; ?>" type="radio" class="valor <?php if($k == 1) { ?>required<?php } ?>" <?php echo $libro->getValoracionUsuario() == $k ? "checked='checked'" : "" ?>/>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <em>Agrégalo a tu estantería para poder valorarlo.</em>
                                            <?php } ?>
                                            <br /><br />
                                        </div>
                                    <?php } ?>
                                        
                                    <div style="float: left; border: 1px solid #D90416; padding: 5px; margin-bottom: 15px; clear: both;">
                                        <img src="<?php echo $raiz; ?>/img/portadas/<?php echo $libro->getPortada(); ?>" alt="" width="300px" />
                                    </div>
                                    <div style="float: left;">
                                        <dl>
                                            <dt>Valoración media</dt>
                                            <dd>
                                                <?php if ($libro->getValoracionMedia() != null) { 
                                                    for ($m = 1; $m < 11; $m++) { ?>
                                                        <input name="star2" value="<?php echo $m; ?>" type="radio" class="star" disabled="disabled" <?php echo intval($libro->getValoracionMedia()) == $m ? "checked='checked'" : "" ?> />
                                                    <?php }
                                                } else { ?>
                                                        <em>Todavía no ha sido valorado.</em>
                                                <?php }?>
                                            </dd>
                                            <dt style="clear: both;">ISBN</dt>
                                            <dd><?php echo $libro->getIsbn(); ?></dd>
                                            <?php if (count($autores) == 1) { ?>
                                                <dt>Autor</dt>
                                                <?php 
                                                    $a = new Autor();
                                                    $a = $autores[0];
                                                ?>
                                                <dd><?php echo $a->getNombre(); ?></dd>
                                            <?php } else { ?>
                                                <dt>Autores</dt>
                                                <?php for ($j = 0; $j < count($autores); $j++) { 
                                                    $a = new Autor();
                                                    $a = $autores[$j]; ?>
                                                    <dd><?php echo $a->getNombre(); ?></dd>
                                                <?php } ?>
                                            <?php } ?>
                                            <dt>Subtítulo</dt>
                                            <dd><?php echo $libro->getSubtitulo() != '' ? $libro->getSubtitulo() : "<em>No se ha indicado un subtítulo.</em>" ?></dd>
                                            <dt>Páginas</dt>
                                            <dd><?php echo $libro->getPaginas() != '0' ? $libro->getPaginas() . " páginas" : "<em>No se ha indicado el número de páginas.</em>" ?></dd>
                                            <dt>Año de publicación</dt>
                                            <dd><?php echo $libro->getAnyoPublicacion() != '0000' ? $libro->getAnyoPublicacion() : "<em>No se ha indicado el año de publicación.</em>" ?></dd>
                                            <dt>Editorial</dt>
                                            <dd><?php echo $editorial != 0 ? $editorial->getNombre() : "<em>No se ha indicado la editorial.</em>" ?></dd>
                                            <dt>Tipo de edición</dt>
                                            <dd><?php echo $tipoEdicion != 0 ? $tipoEdicion->getDescripcion() : "<em>No se ha indicado el tipo de edición.</em>" ?></dd>
                                            <dt>Edición</dt>
                                            <dd><?php echo $libro->getEdicion() != '' ? $libro->getEdicion() : "<em>No se ha indicado la edición.</em>" ?></dd>
                                            <dt>Temáticas</dt>
                                            <?php if (count($tematicas) == 0) { ?>
                                                <em>No se ha indicado ninguna temática.</em>
                                            <?php } else {
                                                for ($k = 0; $k < count($tematicas); $k++) { 
                                                    $t = new Tematica();
                                                    $t = $tematicas[$k]; ?>
                                                    <dd><?php echo $t->getDescripcion(); ?></dd>
                                                <?php }
                                            } ?>
                                            
                                        </dl>
                                    </div>
                                    <div class="alert sinopsis">
                                        <strong>Sinópsis</strong><br /><br />
                                        <?php echo $libro->getSinopsis() != '' ? stripslashes($libro->getSinopsis()) : "<em>No se ha añadido sinópsis a este libro.</em>" ?> 
                                    </div>
                                </div>
                            </div>
                            <!-- PESTAÑA 2 -->
                            <div class="tab-pane" id="2">
                                <div align="right" style="margin: 15px;">
                                    <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'admin') { ?>
                                        
                                    <?php } elseif(isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'usuario') { ?>
                                        <?php if (in_array($libro->getIsbn(), $isbn)) { ?>
                                            <?php if ($comentado == 0) { ?>
                                            <a href="#" class="btn" onclick="addComment('<?php echo $raiz; ?>')">
                                                <i class="icon-pencil"></i> Comentar</a>
                                            <?php } ?>
                                            <a href="#" class="btn" onclick="removeShelf('<?php echo $raiz; ?>')">
                                                <i class="icon-remove"></i> Eliminar de mi estantería</a>
                                        <?php } else { ?>
                                            <a href="#" class="btn" onclick="addShelf('<?php echo $raiz; ?>')">
                                                <i class="icon-plus"></i> Agregar a mi estantería</a>
                                        <?php } ?>
                                            <a href="<?php echo $raiz; ?>/user/book_modify.php?isbn=<?php echo $libro->getIsbn(); ?>" class="btn">
                                                <i class="icon-list-alt"></i> Mejorar datos</a>
                                            <a href="<?php echo $raiz; ?>/user/complain.php?isbn=<?php echo $libro->getIsbn(); ?>" class="btn">
                                                <i class="icon-warning-sign"></i> Denunciar</a>
                                    <?php } else { ?>
                                    <div class="alert alert-success" style="clear: both; margin-left: 0px; margin-right: 0px; margin-bottom: 15px; text-align: justify;">
                                        <strong><a href="<?php echo $raiz ?>/register.php">Regístrate</a> o accede a BookWorm</strong> para poder 
                                        ver los comentarios y valoraciones de los usuarios.
                                    </div>
                                    <?php } ?>
                                    <a href="<?php echo $_SESSION["data_books_page"] == "1" ? "$raiz/search_results.php" : "javascript:history.back()"?>" class="btn">
                                        <i class="icon-arrow-left"></i> Volver</a>
                                </div>
                                <hr>
                                <?php if (isset($_SESSION["tipoUsuario"])) { ?>
                                    <?php if (count($comentarios) == 0) { ?>
                                        <?php if (isset($_SESSION["noHayC"])) { ?>
                                            <div class="alert alert-error">
                                                <?php
                                                echo "<strong>" . $_SESSION["noHayC"] . "</strong>";
                                                session_unregister("noHayC");
                                                ?>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <? for ($i = 0; $i < count($comentarios); $i++) {
                                            $c = new Comentario();
                                            $c = $comentarios[$i];
                                            $u = new Usuario();
                                            $u = $c->getUsuario();
                                            $date2 = explode('-', $c->getFecha());
                                            $date = $date2[2] . '/' . $date2[1] . '/' . $date2[0]; ?>
                                            <div class="alert sinopsis" style="margin: 10px;">
                                                Comentario de <strong><a href="<?php echo $raiz; ?>/user/user_details.php?id=<?php echo $u->getId(); ?>"><?php echo $u->getNombre(); ?></a></strong><br />
                                                <em>Publicado: <?php echo $date; ?></em><br />
                                                <?php if ($c->getSpoiler()) { ?>
                                                    <div style="color: #D90416;"><strong>¡ESTE COMENTARIO CONTIENE SPOILERS!</strong></div>
                                                <?php } ?>
                                                <div class="comment">
                                                    <strong><?php echo $c->getTitulo(); ?></strong><br /><br />
                                                    <p><?php echo $c->getComentario(); ?></p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                                
                            </div>
                        </div>
                    </div>
                    
                    <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'usuario') { ?>
                        <?php if (in_array($libro->getIsbn(), $isbn)) { ?>
                            <!-- MODAL - AÑADIR COMENTARIO -->
                            <div id="commentModal" class="modal hide fade" >
                                <div class="modal-header">
                                    <a class="close" data-dismiss="modal">×</a>
                                    <h3>Comentario - <?php echo $libro->getTitulo(); ?></h3>
                                </div>
                                <form class="mod" method="post" name="comentario_form">
                                    <fieldset>
                                        <div class="modal-body">
                                            <input id="idM" name="id" value="" type="hidden" />
                                            <div class="control-group">
                                                <label class="control-label" for="input01">Título</label>
                                                <div class="controls">
                                                    <input type="text" class="input-xlarge" id="input01" name="titulo" value="" maxlength="30" />
                                                </div>
                                                <label class="control-label" for="input01">Comentario *</label>
                                                <div class="controls">
                                                    <textarea class="input-xlarge" id="textarea" rows="4" name="comentario"></textarea>
                                                    <span id="help-inline-text02" class="help-inline" style="color: #B94A48;"></span>
                                                </div>
                                                <label class="control-label" for="optionsCheckbox">&#32;</label>
                                                <div class="controls">
                                                    <label class="checkbox">
                                                        <input type="checkbox" id="optionsCheckbox" value="spoiler">
                                                        Contiene spoiler
                                                    </label>
                                                </div>

                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                                <div class="modal-footer">
                                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
                                    <a href="#" class="btn" onclick="enviarComentario('<?php echo $raiz; ?>')"><i class="icon-pencil"></i> Comentar</a>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    
                </div>
                <!-- SIDEBAR -->
                <?php require_once 'parts/sidebar.php'; ?>
            </div>

            <?php require_once 'parts/footer.php'; ?>
        </div>
    </body>
</html>
