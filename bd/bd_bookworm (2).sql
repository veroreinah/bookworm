-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Servidor: localhost
-- Tiempo de generación: 21-06-2012 a las 17:34:10
-- Versión del servidor: 5.0.51
-- Versión de PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Base de datos: `bd_bookworm`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_autores`
-- 

CREATE TABLE `t_autores` (
  `id_autor` int(11) NOT NULL auto_increment,
  `nombre_autor` varchar(70) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`id_autor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=45 ;

-- 
-- Volcar la base de datos para la tabla `t_autores`
-- 

INSERT INTO `t_autores` VALUES (1, 'Alfonso V El MagnÃ¡nimo');
INSERT INTO `t_autores` VALUES (2, 'Jorge Luis Borges');
INSERT INTO `t_autores` VALUES (3, 'Miguel de Cervantes Saavedra');
INSERT INTO `t_autores` VALUES (4, 'Sir Francis Bacon');
INSERT INTO `t_autores` VALUES (5, 'Joseph Addison');
INSERT INTO `t_autores` VALUES (6, 'Albert Einstein');
INSERT INTO `t_autores` VALUES (7, 'Gabriel GarcÃ­a MÃ¡rquez');
INSERT INTO `t_autores` VALUES (10, 'Stephen King');
INSERT INTO `t_autores` VALUES (11, 'Jandy Nelson');
INSERT INTO `t_autores` VALUES (13, 'Robert Louis Stevenson');
INSERT INTO `t_autores` VALUES (14, 'Emily Dickinson');
INSERT INTO `t_autores` VALUES (15, 'Carlo Dossi');
INSERT INTO `t_autores` VALUES (16, 'Thomas De Kempis');
INSERT INTO `t_autores` VALUES (17, 'Jane Austen');
INSERT INTO `t_autores` VALUES (21, 'Ken Follet');
INSERT INTO `t_autores` VALUES (23, 'C.S. Lewis');
INSERT INTO `t_autores` VALUES (24, 'Stella Gibbons');
INSERT INTO `t_autores` VALUES (25, 'Gail Carriger');
INSERT INTO `t_autores` VALUES (26, 'Ally Condie');
INSERT INTO `t_autores` VALUES (27, 'Lisa McMann');
INSERT INTO `t_autores` VALUES (28, 'Cornelia Caroline Funke');
INSERT INTO `t_autores` VALUES (29, 'Alan Moore');
INSERT INTO `t_autores` VALUES (30, 'David Lloyd');
INSERT INTO `t_autores` VALUES (31, 'Charlaine Harris');
INSERT INTO `t_autores` VALUES (32, 'Federico Moccia');
INSERT INTO `t_autores` VALUES (33, 'James Dashner');
INSERT INTO `t_autores` VALUES (34, 'Daniel Glattauer');
INSERT INTO `t_autores` VALUES (35, 'Jan-Philipp Sendker');
INSERT INTO `t_autores` VALUES (36, 'Sophie Kinsella');
INSERT INTO `t_autores` VALUES (37, 'Henning Mankell');
INSERT INTO `t_autores` VALUES (38, 'Camilla Lackberg');
INSERT INTO `t_autores` VALUES (39, 'Orson Scott Card');
INSERT INTO `t_autores` VALUES (40, 'Federico Moccia');
INSERT INTO `t_autores` VALUES (41, 'Suzanne Collins');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_citas`
-- 

CREATE TABLE `t_citas` (
  `id_cita` int(11) NOT NULL auto_increment,
  `texto` varchar(500) collate utf8_spanish_ci NOT NULL,
  `id_autor` int(11) default NULL,
  PRIMARY KEY  (`id_cita`),
  KEY `fk_autor_cita` (`id_autor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=15 ;

-- 
-- Volcar la base de datos para la tabla `t_citas`
-- 

INSERT INTO `t_citas` VALUES (1, 'No debemos de llenar la cabeza de los jÃ³venes con fÃ³rmulas complicadas que pueden encontrar en cualquier libro, hay que enseÃ±arles a razonar.', 6);
INSERT INTO `t_citas` VALUES (2, 'Los libros son, entre mis consejeros, los que mÃ¡s me agradan, porque ni el temor ni la esperanza les impiden decirme lo que debo hacer.', 1);
INSERT INTO `t_citas` VALUES (3, 'Siempre imaginÃ© que el ParaÃ­so serÃ­a algÃºn tipo de biblioteca.', 2);
INSERT INTO `t_citas` VALUES (4, 'El que lee mucho y anda mucho, ve mucho y sabe mucho.', 3);
INSERT INTO `t_citas` VALUES (5, 'Algunos libros son probados, otros devorados, poquÃ­simos masticados y digeridos.', 4);
INSERT INTO `t_citas` VALUES (6, 'La lectura es a la mente lo que el ejercicio al cuerpo.', 5);
INSERT INTO `t_citas` VALUES (8, 'Nunca releo mis libros, porque me da miedo.', 7);
INSERT INTO `t_citas` VALUES (9, 'El escritor escribe su libro para explicarse a sÃ­ mismo lo que no se puede explicar.', 7);
INSERT INTO `t_citas` VALUES (10, 'Para viajar lejos, no hay mejor nave que un libro.', 14);
INSERT INTO `t_citas` VALUES (11, 'Nunca escribo mi nombre en los libros que compro hasta despuÃ©s de haberlos leÃ­do, porque sÃ³lo entonces puedo llamarlos mÃ­os.', 15);
INSERT INTO `t_citas` VALUES (12, 'He buscado el sosiego en todas partes, y sÃ³lo lo he encontrado sentado en un rincÃ³n apartado, con un libro en las manos.', 16);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_comentarios`
-- 

CREATE TABLE `t_comentarios` (
  `id_usuario` int(11) NOT NULL,
  `isbn` varchar(20) collate utf8_spanish_ci NOT NULL,
  `fecha` date default NULL,
  `titulo` varchar(30) collate utf8_spanish_ci default NULL,
  `comentario` text collate utf8_spanish_ci,
  `spoiler` tinyint(1) default NULL,
  `valoracion` int(11) default NULL,
  PRIMARY KEY  (`id_usuario`,`isbn`),
  KEY `fk_usuario_com` (`id_usuario`),
  KEY `fk_libro_com` (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- 
-- Volcar la base de datos para la tabla `t_comentarios`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_denuncias`
-- 

CREATE TABLE `t_denuncias` (
  `id_usuario` int(11) NOT NULL,
  `isbn` varchar(20) collate utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `texto` text collate utf8_spanish_ci NOT NULL,
  `vista` tinyint(1) default NULL,
  `enlace` int(200) NOT NULL auto_increment,
  PRIMARY KEY  (`enlace`),
  KEY `fk_usuario_denuncia` (`id_usuario`),
  KEY `fk_libro_denuncia` (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `t_denuncias`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_editoriales`
-- 

CREATE TABLE `t_editoriales` (
  `id_editorial` int(11) NOT NULL auto_increment,
  `nombre_editorial` varchar(50) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`id_editorial`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=23 ;

-- 
-- Volcar la base de datos para la tabla `t_editoriales`
-- 

INSERT INTO `t_editoriales` VALUES (1, 'B DE BOLSILLO');
INSERT INTO `t_editoriales` VALUES (3, 'Molino');
INSERT INTO `t_editoriales` VALUES (4, 'Alfaguara');
INSERT INTO `t_editoriales` VALUES (6, 'Everest');
INSERT INTO `t_editoriales` VALUES (8, 'Plaza & JanÃ©s');
INSERT INTO `t_editoriales` VALUES (9, 'Destino');
INSERT INTO `t_editoriales` VALUES (10, 'Grijalbo');
INSERT INTO `t_editoriales` VALUES (11, 'Editorial Planeta');
INSERT INTO `t_editoriales` VALUES (12, 'Impedimenta');
INSERT INTO `t_editoriales` VALUES (13, 'VersÃ¡til');
INSERT INTO `t_editoriales` VALUES (14, 'Montena');
INSERT INTO `t_editoriales` VALUES (15, 'DeBolsillo');
INSERT INTO `t_editoriales` VALUES (16, 'Siruela');
INSERT INTO `t_editoriales` VALUES (17, 'La FactorÃ­a de Ideas');
INSERT INTO `t_editoriales` VALUES (18, 'Nocturna');
INSERT INTO `t_editoriales` VALUES (19, 'Maeva');
INSERT INTO `t_editoriales` VALUES (20, 'Obelisco');
INSERT INTO `t_editoriales` VALUES (21, 'Editorial Planeta, S.A.');
INSERT INTO `t_editoriales` VALUES (22, 'Booket');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_invitacion`
-- 

CREATE TABLE `t_invitacion` (
  `id_invitacion` int(11) NOT NULL auto_increment,
  `fecha_envio` date NOT NULL,
  `email_inv` varchar(80) collate utf8_spanish_ci NOT NULL,
  `texto` varchar(255) collate utf8_spanish_ci default NULL,
  `aceptada` tinyint(1) default NULL,
  `id_usuario` int(11) NOT NULL,
  `enlace` varchar(200) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`id_invitacion`),
  UNIQUE KEY `enlace` (`enlace`),
  KEY `fk_usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `t_invitacion`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_libros`
-- 

CREATE TABLE `t_libros` (
  `isbn` varchar(20) collate utf8_spanish_ci NOT NULL,
  `titulo` varchar(100) collate utf8_spanish_ci NOT NULL,
  `subtitulo` varchar(200) collate utf8_spanish_ci default NULL,
  `paginas` int(10) default NULL,
  `portada` varchar(200) collate utf8_spanish_ci default NULL,
  `sinopsis` text collate utf8_spanish_ci,
  `anyo_publicacion` year(4) default NULL,
  `edicion` varchar(20) collate utf8_spanish_ci default NULL,
  `id_editorial` int(11) default NULL,
  `id_tipo_edicion` int(11) default NULL,
  `id_usuario` int(11) default NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY  (`isbn`),
  KEY `fk_editorial` (`id_editorial`),
  KEY `fk_tipo_edicion` (`id_tipo_edicion`),
  KEY `fk_usuario_crea` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- 
-- Volcar la base de datos para la tabla `t_libros`
-- 

INSERT INTO `t_libros` VALUES ('9788401335983', 'Cell', '', 201, 'default.JPG', 'El dÃ­a 1 de octubre a las 15:03 se envÃ­an mensajes a travÃ©s de los telÃ©fonos mÃ³viles que convierten a todos los que los reciben en zombies asesinos... Se desata una locura criminal y destructiva de la que pocos escaparÃ¡n, y los que lo logren tendrÃ¡n que sobrevivir en un mundo totalmente transformado. Terror puro, el retrato de un mundo escalofriante que nunca volverÃ¡ a ser el mismo.', 0000, '', 8, 5, 11, '2012-06-12');
INSERT INTO `t_libros` VALUES ('9788408061489', 'Las crÃ³nicas de Narnia', 'Obra Completa', 524, 'default.JPG', 'Durante los Ãºltimos cincuenta aÃ±os, Las crÃ³nicas de Narnia han entusiasmado a millones de lectores en todo el mundo. El universo mÃ¡gico de Narnia, descrito por la inmortal prosa de C.S. Lewis, ha dejad huella imborrable, mezcla de curiosidad, admiraciÃ³n y afÃ¡n de aventura, en una generaciÃ³n tras otra de lectores. Ahora, por primera vez, se editan conjuntamente las siete crÃ³nicas de Narnia en un solo volumen, con una introducciÃ³n de Douglas Gresham y las ilustraciones originales de Pauline Baynes.', 2005, '', 11, 1, 11, '2012-06-11');
INSERT INTO `t_libros` VALUES ('9788408082385', 'A tres metros sobre el cielo', '', 0, 'default.JPG', '<p></p><div>Babi es una estudiante modelo y la hija perfecta. Step, en cambio, es violento y descarado. Provienen de dos mundos completamente distintos. A pesar de todo entre los dos nacerÃ¡ un amor fuera de todas las convenciones. Un amor controvertido por el que deberÃ¡n luchar mÃ¡s de lo que esperaban. Babi y Step se erigen como un Romeo y Julieta contemporÃ¡neos en Roma, un escenario que parece creado para el amor.</div><div>A tres metros sobre el cielo es la primera obra de Federico Moccia. Publicado por primera vez en 1992 en una ediciÃ³n mÃ­nima pagada por el propio autor y que se agotÃ³ inmediatamente, fue fotocopiado una y otra vez, y circulÃ³ de mano en mano hasta que se reeditÃ³ en 2004 y se convirtiÃ³ en un espectacular Ã©xito de ventas. Se han vendido mÃ¡s de un millÃ³n de ejemplares en Italia.</div><p></p>', 0000, '', 11, NULL, 11, '2012-06-12');
INSERT INTO `t_libros` VALUES ('9788408087670', 'Perdona si te llamo amor', '', 688, 'default.JPG', '', 2009, '5', 22, NULL, 11, '2012-06-14');
INSERT INTO `t_libros` VALUES ('9788408093978', 'Tengo ganas de ti', '', 496, 'default.JPG', '', 2010, '1', 21, NULL, 11, '2012-06-14');
INSERT INTO `t_libros` VALUES ('9788420406107', 'Contra el viento del norte', '', 260, 'default.JPG', '<div>En la vida diaria Â¿hay lugar mÃ¡s seguro para los deseos secretos que el mundo virtual? Leo Leike recibe mensajes por error de una desconocida llamada Emmi. Como es educado, le contesta y como Ã©l la atrae, ella escribe de nuevoÂ… AsÃ­, poco a poco, se entabla un diÃ¡logo en el que no hay marcha atrÃ¡s. Parece solo una cuestiÃ³n de tiempo que se conozcan en persona, pero la idea los altera tan profundamente que prefieren posponer el encuentro. AdemÃ¡s, Emmi estÃ¡ felizmente casada y Leo aÃºn estÃ¡ saliendo de una relaciÃ³n fallida. Y, en cualquier caso Â¿sobrevivirÃ­an las emociones enviadas, recibidas y guardadas un encuentro Â«realÂ»? Y en ese caso, Â¿quÃ© hacer?&nbsp;</div><div><br></div><div>Un libro que, una vez abierto, no puedes volver a cerrar. Una trama audaz y brillante, llena de inteligencia, humor y ternura.&nbsp;</div><div>Unos protagonistas inolvidables que empatizan inmediatamente con el lector.</div>', 2010, '4', 4, NULL, 11, '2012-06-12');
INSERT INTO `t_libros` VALUES ('9788420406398', 'Cada siete olas', '', 0, 'default.JPG', '<div>Nunca habÃ­amos estado tan cerca y al mismo tiempo tan lejos</div><div>1) Â¿Ya conoces a Emmi Rothner y a Leo Leike? Entonces es que has leÃ­do Contra el viento del norte, la inusual historia de amor en que dos personas que jamÃ¡s se han visto se enamoran perdidamente por e-mail.</div><div>2) Â¿Opinas que los enamorados se merecÃ­an verse personalmente, aunque fuera sÃ³lo una vez? Comienza a leer Cada siete olas.</div><div>3) Â¿Te dispones a abrir este libro sin conocer Contra el viento del norte? AquÃ­ tienes el equipaje necesario: Leo Leike vuelve de Boston tras poco menos de un aÃ±o. En casa lo esperan noticias de Emmi Rothner. Ambos se dan cuenta de que sus sentimientos no han cambiado y piensan que quizÃ¡ deberÃ­an verse una vez en persona. Pero Leo ha empezado una relaciÃ³n y Emmi sigue casadaâ€¦</div><div>Daniel Glattauer vuelve a cautivar a los lectores y a la crÃ­tica internacional con su peculiar mirada sobre las relaciones amorosas en nuestro tiempo. Emmi y Leo nos enseÃ±an que, despuÃ©s de que seis olas rompan en la orilla llega la sÃ©ptima, y Ã©sa trae siempre muchas sorpresas.</div>', 0000, '', 4, NULL, 11, '2012-06-13');
INSERT INTO `t_libros` VALUES ('9788444145259', 'El cielo estÃ¡ en cualquier lugar', '', 342, 'default.JPG', '<p><span style="font-family: ''Lucida Grande'', Verdana, ''Lucida Sans Unicode'', sans-serif; font-size: 12px; line-height: 19px; text-align: left; background-color: rgb(255, 255, 255); ">Lennie walker, de 17 aÃ±os, lectora empedernida y fanÃ¡tica de la mÃºsica, es segundo clarinete de la banda de su instituto. Vive feliz y sin contratiempos a la sombre de Bailey, su impetuosa hermana mayor. Pero cuando Bailey muere repentinamente, Lennie se convierte en protagonista de su propia vida... Y , aunque nunca habÃ­a salido con ningÃºn chico, de pronto se encuentra intentando compatibilizar a dos.</span></p><div><span style="font-family: ''Lucida Grande'', Verdana, ''Lucida Sans Unicode'', sans-serif; font-size: 12px; line-height: 19px; text-align: left; background-color: rgb(255, 255, 255); ">Toby era el novio de Bailey; Lennie se identifica con el dolor que siente. Joe es el chico nuevo del pueblo, acaba de llegar de ParÃ­s y su sonrisa casi mÃ¡gica solo puede compararse con su talento musical. Para Lennie son el sol y la luna: uno la saca de su pesar mientras el otro le ofrece consuelo.</span></div><div><span style="font-family: ''Lucida Grande'', Verdana, ''Lucida Sans Unicode'', sans-serif; font-size: 12px; line-height: 19px; text-align: left; background-color: rgb(255, 255, 255); ">Aunque, al igual que los propios cuerpos celestes, si llegan a chocar estallarÃ¡ el mundo entero.</span></div><p></p>', 2010, '1Âª ediciÃ³n', 6, 4, 11, '2012-06-11');
INSERT INTO `t_libros` VALUES ('9788444146256', 'SueÃ±a', 'Cazadora de SueÃ±os #1', 232, 'default.JPG', 'Para Janie, de 17 aÃ±os, ser absorbida dentro de los sueÃ±os ajenos empieza a ser aburrido. Janie ha tenido suficiente fantasÃ­a para toda una vida. No puede contarle a nadie lo que hace: no la creerÃ­an, o aÃºn peor, creerÃ­an que es un bicho raro. AsÃ­ que vive al lÃ­mite, maldita por una habilidad que no desea y que ademÃ¡s no puede controlar. Y entonces cae dentro de una horripilante pesadilla, una que la asusta hasta la mÃ©dula. Por primera vez, Janie es mÃ¡s que un testigo en la retorcida psique de otro: es una participante...', 2011, '', 6, 2, 11, '2012-06-12');
INSERT INTO `t_libros` VALUES ('9788467494181', 'V de Vendetta', 'EdiciÃ³n Absolute', 400, 'default.JPG', '<div>â€œRecordad el 5 de noviembreâ€</div><div>La historia de V de Vendetta estÃ¡ ambientada en Gran BretaÃ±a durante un futuro cercano y tras una guerra nuclear parcial. En este futuro, con gran parte del mundo destruido, un partido fascista ostenta el poder en el Reino Unido. Un misterioso anarquista revolucionario apodado "V", oculto tras una mÃ¡scara de Guy Fawkes, inicia una elaborada y violenta campaÃ±a con el fin de derrocar al gobierno e incitar a la poblaciÃ³n a caminar hacia una sociedad anarquista.</div>', 0000, '', NULL, NULL, 11, '2012-06-12');
INSERT INTO `t_libros` VALUES ('9788477209003', 'Encantamiento', '', 0, 'default.JPG', 'En el momento en que IvÃ¡n se encontrÃ³ en un claro de un denso bosque de los CÃ¡rpatos, su vida cambiÃ³ para siempre. Sobre un pedestal rodeado de hojas caÃ­das, la bella princesa Katerina yacÃ­a tan quieta que pareciera muerta. Pero bajo el follaje, se movÃ­a una maligna presencia que hizo que, a sus diez aÃ±itos, IvÃ¡n saliese corriendo a casa de su primo Marek.', 0000, '', 20, NULL, 11, '2012-06-13');
INSERT INTO `t_libros` VALUES ('9788478447909', 'CorazÃ³n de tinta', '', 596, 'default.JPG', 'QuiÃ©nes son de verdad Dedo Polvoriento, Capricornio o Lengua de Brujo lo sabrÃ¡ la joven Meggie por las respuestas que encuentre en un viejo pueblo de las montaÃ±as de Liguria... y tambiÃ©n en un libro. Cuando Mo, el padre de Meggie, saluda a un extraÃ±o visitante que aparece en su casa, la niÃ±a siente que aquella persona emana un peligro, quizÃ¡ una gran amenaza contra su padre... y entonces huyen al sur, a la casa de tÃ­a Elinor, propietaria de una de las mÃ¡s fascinantes bibliotecas que uno pueda imaginar. Meggie descubrirÃ¡ que los forasteros que misteriosamente aparecen y desaparecen, como aquel visitante nocturno, llaman a su padre Lengua de Brujo, ya que tiene el don de dar vida a los personajes de los libros cuando lee en voz alta. Esta nueva novela de Cornelia Funke es magia, es mÃ¡gica y es fantÃ¡stica: es un viaje al mundo de los libros, una gran novela de aventuras y una declaraciÃ³n de amor a los grandes textos universales que cautivan a los lectores. Y tambiÃ©n, una lucha feroz entre ficciÃ³n y realidad, y entre bien y mal.', 2004, '', 16, NULL, 11, '2012-06-12');
INSERT INTO `t_libros` VALUES ('9788483830956', 'El chino', '', 0, 'default.JPG', 'Una helada maÃ±ana de enero de 2006, un fotÃ³grafo hace un descubrimiento aterrador: en el pequeÃ±o pueblo de Hesjovallen aparecen brutalmente asesinadas diecinueve personas. Ante semejante atrocidad, la policÃ­a sospecha que puede tratarse de la obra de un perturbado. Sin embargo, la jueza Birgitta Roslin, en Helsingborg, al leer el suceso se entera de que entre las vÃ­ctimas se encontraban los padres adoptivos de su madre, August y Britta AndrÃ©n; cuando busca mÃ¡s informaciÃ³n, descubre que tambiÃ©n en Nevada, Estados Unidos, ha muerto asesinada una familia apellidada AndrÃ©n. Una sospecha comienza abrirse paso en su mente. En el lugar del crimen se ha encontrado una cinta de seda roja, y Birgitta cae en la cuenta, fortuitamente, de que es una de las cintas que cuelgan de los farolillos de un restaurante chino de Hudiksvall. En sus investigaciones, Birgitta viaja hasta PekÃ­n, donde se verÃ¡ involucrada de pronto en la cruenta lucha de poderes dentro del Partido Comunista Chino. Como potencia mundial en pleno desarrollo, China se enfrenta al grave problema de la falta de materia prima y al de millones de campesinos pobres sin acceso al creciente bienestar; en tal encrucijada, una posible soluciÃ³n consiste en enviar fuera del paÃ­s a esos campesinos para que colonicen grandes zonas de Ãfrica... Entretanto, Birgitta es sometida a una implacable persecuciÃ³n que la pone en grave peligro. AsÃ­ arranca El chino, una novela trepidante de Henning Mankell que sitÃºa a los lectores no sÃ³lo en la Suecia y la China actuales, sino tambiÃ©n en escenarios que, en el siglo XIX, fueron testigos de grandes dramas.', 2008, '', NULL, 2, 11, '2012-06-13');
INSERT INTO `t_libros` VALUES ('9788484416937', 'Juntos', '', 352, 'default.JPG', '<div>Ha llegado el momento mÃ¡s importante en la vida de Cassia: las autoridades van a comunicarle quiÃ©n es su Â«pareja perfectaÂ», la persona con quien deberÃ¡ compartir el resto de su vida. Nos encontramos en un futuro remoto, en una ciudad donde la tecnologÃ­a es omnipresente y donde toda elecciÃ³n personal ha quedado depositada en manos de las autoridades para garantizar la plena armonÃ­a social.&nbsp;</div><div>En este mundo supuestamente utÃ³pico vive Cassia, la protagonista de esta novela. A sus 17 aÃ±os reciÃ©n cumplidos, acaba de asistir a su Ceremonia de Compromiso, el evento mÃ¡s importante en la vida de cualquier adolescente, porque allÃ­ se da a conocer quiÃ©n es su Â«pareja perfectaÂ».&nbsp;</div><div>A Cassia las autoridades le han comunicado que, tras desarrollar un minucioso estudio de personalidad computerizado, la persona con quien deberÃ¡ compartir el resto de su vida es Xander, su mejor amigo. EstÃ¡n predestinados.&nbsp;</div><div>Pero un extraÃ±o error informÃ¡tico harÃ¡ que Cassia se enamore de la persona equivocada.</div>', 2011, '1', 14, NULL, 11, '2012-06-11');
INSERT INTO `t_libros` VALUES ('9788484505075', 'Misery', '', 373, 'default.JPG', '<div style="text-align: justify;"><font color="#333333" face="verdana, sans-serif"><span style="font-size: 12px; line-height: 18px;">Misery es un relato obsesivo que sÃ³lo podÃ­a escribir Stephen King. Un escritor sufre un grave accidente y recobra el conocimiento en una apartada casa en la que vive una misteriosa mujer, corpulenta y de extraÃ±o carÃ¡cter. Se trata de una antigua enfermera, involucrada en varias muertes misteriosas ocurridas en diversos hospitales. Esta mujer es capaz de los mayores horrores, y el escritor, con las piernas rotas y entre terribles dolores, tiene que luchar por su vida.</span><br></font></div>', 2001, '', 15, NULL, 11, '2012-06-12');
INSERT INTO `t_libros` VALUES ('9788484506591', 'El arte de escuchar los latidos del corazÃ³n', '', 314, 'default.JPG', 'La neoyorquina Julia Win solo tiene una pista de su padre desaparecido: una carta de amor escrita por Ã©l 40 aÃ±os atrÃ¡s y que la conduce hasta un remoto pueblo de Birmania. AllÃ­ conoce a un anciano que dice saber la historia de su padre: ciego desde pequeÃ±o, se enamorÃ³ de una joven minusvÃ¡lida de quien tuvo que separarse cuando fue enviado a Estados Unidos para ser operado de los ojos y estudiar. La familia que creÃ³ allÃ¡ no fue suficiente para apagar la llama de ese primer y verdadero amor. En Birmania, Julia siente a su padre mÃ¡s cerca que nunca, pero Â¿serÃ¡ capaz de encontrarlo?', 2009, '1', 15, NULL, 11, '2012-06-12');
INSERT INTO `t_libros` VALUES ('9788492929245', 'Sin alma', '1Âº El protectorado de la sombrilla', 383, 'default.JPG', '<p style="padding: 0px 0px 1em; margin: 0px; ">Una novela de vampiros, licÃ¡ntropos y sombrillas en pleno Londres Victoriano.</p><p style="padding: 0px 0px 1em; margin: 0px; ">Alexia Tarabotti debe desenvolverse bajo muchas y variadas tribulaciones sociales. En primer lugar, no tiene alma. En segundo, es una soltera cuyo padre es italiano y, ademÃ¡s, estÃ¡ muerto. Y en tercer lugar, ha sido groseramente atacada por un vampiro sin ningÃºn respeto por la etiqueta social.</p><p style="padding: 0px 0px 1em; margin: 0px; ">Â¿QuÃ© puede esperarse de todo eso? Aparentemente, nada bueno, pues Alexia mata accidentalmente al vampiro y el atroz Lord Maccon (ruidoso, indecoroso, atractivo y hombre lobo) es enviado por la Reina para investigar el caso. Con inesperados vampiros apareciendo y esperados vampiros desapareciendo, todo el mundo parece estar convencido de la culpabilidad de Alexia. Â¿SerÃ¡ capaz de averiguar lo que estÃ¡ sucediendo realmente en la alta sociedad londinense? Â¿Su habilidad para anular los poderes sobrenaturales debido a su carencia de alma demostrarÃ¡ ser Ãºtil o simplemente embarazosa? Y, finalmente, Â¿quiÃ©n es el autÃ©ntico enemigo, y tendrÃ¡n tarta de melaza?</p>', 2010, '', 13, NULL, 11, '2012-06-11');
INSERT INTO `t_libros` VALUES ('9788493760137', 'La hija de Robert Poste', '', 357, 'default.JPG', 'Ganadora del Prix Femina-Vie Hereuse en 1933, y mÃ­tico long-seller, La hija de Robert Poste estÃ¡ considerada la novela cÃ³mica mÃ¡s perfecta de la literatura inglesa del XX. Brutalmente divertida, dotada de un ingenio irreverente, narra la historia de Flora Poste, una joven que, tras haber recibido una educaciÃ³n Â«cara, deportiva y largaÂ», se queda huÃ©rfana y acaba siendo acogida por sus parientes, los rÃºsticos y asilvestrados Starkadder, en la bucÃ³lica granja de Cold Comfort Farm, en plena Inglaterra profunda. Una vez allÃ­, Flora tendrÃ¡ ocasiÃ³n de intimar con toda una galerÃ­a de extraÃ±os y taciturnos personajes: Amos, llamado por Dios; Seth, dominado por el despertar de su prominente sexualidad; Meriam, la chica que se queda preÃ±ada cada aÃ±o Â«cuando florece la parravirgenÂ»; o la tÃ­a Ada Doom, la solitaria matriarca, ya entrada en aÃ±os, que en una ocasiÃ³n Â«vio algo sucio en la leÃ±eraÂ». Flora, entonces, decide poner orden en la vida de Cold Comfort Farm, y allÃ­ empezarÃ¡ su desgracia.', 2010, '1', 12, NULL, 11, '2012-06-11');
INSERT INTO `t_libros` VALUES ('9788493801311', 'El corredor del laberinto', '', 528, 'default.JPG', '<div>MEMORIZA. CORRE. SOBREVIVE.</div><div>Â«Bienvenido al bosque. VerÃ¡s que, una vez a la semana, siempre el mismo dÃ­a y a la misma hora, nos llegan vÃ­veres. Una vez al mes, siempre el mismo dÃ­a y a la misma hora, aparece un nuevo chico, como tÃº. Siempre un chico. Como ves, este lugar estÃ¡ cercado por muros de piedraâ€¦ Has de saber que estos muros se abren por la maÃ±ana y se cierran por la noche, siempre a la hora exacta. Al otro lado se encuentra el laberinto. De noche, las puertas se cierranâ€¦ y, si quieres sobrevivir, no debes estar allÃ­ para entoncesÂ».</div><div>Todo sigue un ordenâ€¦ y, sin embargo, al dÃ­a siguiente suena una alarma. Significa que ha llegado alguien mÃ¡s. Para asombro de todos, es una chica. Su llegada vendrÃ¡ acompaÃ±ada de un mensaje que cambiarÃ¡ las reglas del juego.</div>', 2010, '', 18, NULL, 11, '2012-06-12');
INSERT INTO `t_libros` VALUES ('9788496748507', 'Los gritos del pasado', '', 383, 'default.JPG', '<div>En plena temporada de verano en la pequeÃ±a poblaciÃ³n costera de FjÃ¤llbacka, un niÃ±o descubre el cadÃ¡ver de una turista alemana cruelmente torturada. Muy cerca, la policÃ­a encuentra los esqueletos de dos mujeres desaparecidas hace veinte aÃ±os.</div><div>La joven pareja formada por la escritora Erica y el detective Patrik disfrutan de unas merecidas vacaciones. Erica estÃ¡ embarazada de ocho meses y el calor sofocante del verano vuelve especialmente difÃ­cil este Ãºltimo mes de gestaciÃ³n. La Ãºltima cosa que necesitan ambos es un nuevo caso de asesinatos, pero el malhumorado comisario Mellberg incluye rÃ¡pidamente a Patrik en los acontecimientos. Sorprendentemente todos terminarÃ¡n descubriendo que todas las vÃ­ctimas tenÃ­an alguna relaciÃ³n con el predicador Ephraim Hult y su particular familiaâ€¦</div>', 2008, '', 19, NULL, 11, '2012-06-13');
INSERT INTO `t_libros` VALUES ('9788498004823', 'Muerto hasta el anochecer', 'Serie Sookie Stackhouse Vol. 1', 0, 'default.JPG', '<div>Con la primera novela de la serie de Sookie, Muerto hasta el anochecer, Charlaine Harris demuestra hasta quÃ© punto su talento puede hacer que una casi imposible mezcla de vampiros, misterio, intriga y humor se convierta en una obraÂ deliciosamente imprescindible. Una autora avalada por mÃ¡s de dos dÃ©cadas escribiendo y entreteniendo a miles de lectores de todo el mundo.Â </div><div>Sookie Stackhouse es una camarera con un inusitado poder para leer la mente. Su don es el origen de sus problemas. Siempre acaba sabiendo mÃ¡s de lo que le gustarÃ­a de la gente que le rodea, de todos menos de Bill Compton, porque su mente, la de un vampiro que trata de reinsertarse en la sociedad, es absolutamente impenetrable. Cuando susÂ vidas se cruzan descubrirÃ¡ que para ella ya no hay vuelta atrÃ¡s. La apariciÃ³n de un asesino en serie es la prueba definitiva para su confianzaâ€¦ porque ni siquiera ella sabe si Bill es su protector, o si se convertirÃ¡ en su fatal asesino.</div>', 0000, '', 17, NULL, 11, '2012-06-12');
INSERT INTO `t_libros` VALUES ('9788498381702', 'Loca por las compras', 'Las aventuras de Becky Bloomwood', 0, 'default.JPG', '<div>DÃ­as soporÃ­feros, noches frustrantes y nada en el horizonte necesitas algo que te levante el Ã¡nimo Â¡ya! coges el bolso, compruebas la tarjeta de crÃ©dito y... Â¡a comprar!&nbsp;</div><div><br></div><div>Si uno acabara de conocer a Rebecca dirÃ­a que es una chica como muchas otras, activa, alegre y con ganas de marcha. Pero si intentara compartir con ella un placentero e inocente sÃ¡bado por la tarde, paseando por, digamos, King''s Road, constatarÃ­a de inmediato que Becky, como la llaman sus amigos, estÃ¡ total, absoluta, perdida e irremisiblemente... Â¡loca por las compras! Pese a sus denodados esfuerzos por controlar esos devastadores impulsos consumistas, Becky ha sucumbido tantas veces a la tentaciÃ³n que las deudas empiezan a volverse una seria amenaza para sus inmaculados antecedentes penales.&nbsp;</div><div><br></div><div>Necesitada de una soluciÃ³n urgente y en el punto Ã¡lgido de su desesperaciÃ³n, Becky ha ideado un plan que, o bien la saca de apuros para siempre, o de lo contrario tendrÃ¡ que hacerse a la idea de una tranquila vida en una remota y solitaria isla en los Mares del Sur.</div>', 0000, '', NULL, NULL, 11, '2012-06-13');
INSERT INTO `t_libros` VALUES ('9788498675399', 'Los Juegos del Hambre', 'Los Juegos del Hambre I', 400, 'default.JPG', '<p></p><div>Es la hora.&nbsp;</div><div>Ya no hay vuelta atrÃ¡s.&nbsp;</div><div>Los juegos van a comenzar.&nbsp;</div><div>Los tributos deben salir a la Arena y... luchar por sobrevivir.&nbsp;</div><div>Ganar significa fama y riqueza, perder significa la muerte segura...&nbsp;</div><div>Â¡Que empiecen los SeptuagÃ©simo Cuartos Juegos del Hambre!</div><div><br></div><div>Un pasado de guerras ha dejado los 12 distritos que dividen Panem bajo el poder tirÃ¡nico del â€œCapitolioâ€. Sin libertad y en la pobreza, nadie puede salir de los lÃ­mites de su distrito. SÃ³lo una chica de 16 aÃ±os, Katniss Everdeen, osa desafiar las normas para conseguir comida. Sus prinicipios se pondrÃ¡n a prueba con â€œLos juegos del hambreâ€, espectÃ¡culo televisado que el Capitolio organiza para humillar a la poblaciÃ³n. Cada aÃ±o, 2 representantes de cada distrito serÃ¡n obligados a subsistir en un medio hostil y luchar a muerte entre ellos hasta que quede un solo superviviente. Cuando su hermana pequeÃ±a es elegida para participar, Katniss no duda en ocupar su lugar, decidida a demostrar con su actitud firme y decidida, que aÃºn en las situaciones mÃ¡s desesperadas hay lugar para el amor y el respeto.</div><p></p>', 2009, '', 3, NULL, 11, '2012-06-15');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_libros_autores`
-- 

CREATE TABLE `t_libros_autores` (
  `isbn` varchar(20) collate utf8_spanish_ci NOT NULL,
  `id_autor` int(11) NOT NULL,
  PRIMARY KEY  (`isbn`,`id_autor`),
  KEY `fk_libro_aut` (`isbn`),
  KEY `fk_autor` (`id_autor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- 
-- Volcar la base de datos para la tabla `t_libros_autores`
-- 

INSERT INTO `t_libros_autores` VALUES ('9788401335983', 10);
INSERT INTO `t_libros_autores` VALUES ('9788484505075', 10);
INSERT INTO `t_libros_autores` VALUES ('9788444145259', 11);
INSERT INTO `t_libros_autores` VALUES ('9788408061489', 23);
INSERT INTO `t_libros_autores` VALUES ('9788493760137', 24);
INSERT INTO `t_libros_autores` VALUES ('9788492929245', 25);
INSERT INTO `t_libros_autores` VALUES ('9788484416937', 26);
INSERT INTO `t_libros_autores` VALUES ('9788444146256', 27);
INSERT INTO `t_libros_autores` VALUES ('9788478447909', 28);
INSERT INTO `t_libros_autores` VALUES ('9788467494181', 29);
INSERT INTO `t_libros_autores` VALUES ('9788467494181', 30);
INSERT INTO `t_libros_autores` VALUES ('9788498004823', 31);
INSERT INTO `t_libros_autores` VALUES ('9788408082385', 32);
INSERT INTO `t_libros_autores` VALUES ('9788408087670', 32);
INSERT INTO `t_libros_autores` VALUES ('9788493801311', 33);
INSERT INTO `t_libros_autores` VALUES ('9788420406107', 34);
INSERT INTO `t_libros_autores` VALUES ('9788420406398', 34);
INSERT INTO `t_libros_autores` VALUES ('9788484506591', 35);
INSERT INTO `t_libros_autores` VALUES ('9788498381702', 36);
INSERT INTO `t_libros_autores` VALUES ('9788483830956', 37);
INSERT INTO `t_libros_autores` VALUES ('9788496748507', 38);
INSERT INTO `t_libros_autores` VALUES ('9788477209003', 39);
INSERT INTO `t_libros_autores` VALUES ('9788408093978', 40);
INSERT INTO `t_libros_autores` VALUES ('9788498675399', 41);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_libros_tematica`
-- 

CREATE TABLE `t_libros_tematica` (
  `isbn` varchar(20) collate utf8_spanish_ci NOT NULL,
  `id_tematica` int(11) NOT NULL,
  PRIMARY KEY  (`isbn`,`id_tematica`),
  KEY `fk_libro_tem` (`isbn`),
  KEY `fk_tematica` (`id_tematica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- 
-- Volcar la base de datos para la tabla `t_libros_tematica`
-- 

INSERT INTO `t_libros_tematica` VALUES ('9788444145259', 1);
INSERT INTO `t_libros_tematica` VALUES ('9788492929245', 1);
INSERT INTO `t_libros_tematica` VALUES ('9788477209003', 2);
INSERT INTO `t_libros_tematica` VALUES ('9788478447909', 2);
INSERT INTO `t_libros_tematica` VALUES ('9788401335983', 3);
INSERT INTO `t_libros_tematica` VALUES ('9788444145259', 10);
INSERT INTO `t_libros_tematica` VALUES ('9788444145259', 15);
INSERT INTO `t_libros_tematica` VALUES ('9788444146256', 15);
INSERT INTO `t_libros_tematica` VALUES ('9788478447909', 15);
INSERT INTO `t_libros_tematica` VALUES ('9788498675399', 15);
INSERT INTO `t_libros_tematica` VALUES ('9788408082385', 16);
INSERT INTO `t_libros_tematica` VALUES ('9788420406107', 16);
INSERT INTO `t_libros_tematica` VALUES ('9788420406398', 16);
INSERT INTO `t_libros_tematica` VALUES ('9788444145259', 16);
INSERT INTO `t_libros_tematica` VALUES ('9788444146256', 16);
INSERT INTO `t_libros_tematica` VALUES ('9788477209003', 16);
INSERT INTO `t_libros_tematica` VALUES ('9788401335983', 18);
INSERT INTO `t_libros_tematica` VALUES ('9788484505075', 18);
INSERT INTO `t_libros_tematica` VALUES ('9788484505075', 19);
INSERT INTO `t_libros_tematica` VALUES ('9788483830956', 20);
INSERT INTO `t_libros_tematica` VALUES ('9788493760137', 28);
INSERT INTO `t_libros_tematica` VALUES ('9788498381702', 28);
INSERT INTO `t_libros_tematica` VALUES ('9788493801311', 30);
INSERT INTO `t_libros_tematica` VALUES ('9788498675399', 30);
INSERT INTO `t_libros_tematica` VALUES ('9788477209003', 31);
INSERT INTO `t_libros_tematica` VALUES ('9788478447909', 31);
INSERT INTO `t_libros_tematica` VALUES ('9788498675399', 31);
INSERT INTO `t_libros_tematica` VALUES ('9788401335983', 32);
INSERT INTO `t_libros_tematica` VALUES ('9788492929245', 33);
INSERT INTO `t_libros_tematica` VALUES ('9788498004823', 33);
INSERT INTO `t_libros_tematica` VALUES ('9788492929245', 34);
INSERT INTO `t_libros_tematica` VALUES ('9788498004823', 34);
INSERT INTO `t_libros_tematica` VALUES ('9788408082385', 35);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_tematica`
-- 

CREATE TABLE `t_tematica` (
  `id_tematica` int(11) NOT NULL auto_increment,
  `descripcion_tematica` varchar(30) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`id_tematica`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=36 ;

-- 
-- Volcar la base de datos para la tabla `t_tematica`
-- 

INSERT INTO `t_tematica` VALUES (1, 'Romance');
INSERT INTO `t_tematica` VALUES (2, 'FantasÃ­a');
INSERT INTO `t_tematica` VALUES (3, 'Ciencia ficciÃ³n');
INSERT INTO `t_tematica` VALUES (4, 'Humor');
INSERT INTO `t_tematica` VALUES (5, 'Infantil');
INSERT INTO `t_tematica` VALUES (6, 'Intriga');
INSERT INTO `t_tematica` VALUES (7, 'Arquitectura');
INSERT INTO `t_tematica` VALUES (8, 'Arte');
INSERT INTO `t_tematica` VALUES (9, 'Derecho');
INSERT INTO `t_tematica` VALUES (10, 'FilosofÃ­a');
INSERT INTO `t_tematica` VALUES (11, 'ReligiÃ³n');
INSERT INTO `t_tematica` VALUES (13, 'Comic');
INSERT INTO `t_tematica` VALUES (14, 'PoesÃ­a');
INSERT INTO `t_tematica` VALUES (15, 'Amistad');
INSERT INTO `t_tematica` VALUES (16, 'Amor');
INSERT INTO `t_tematica` VALUES (17, 'BiografÃ­a');
INSERT INTO `t_tematica` VALUES (18, 'Terror');
INSERT INTO `t_tematica` VALUES (19, 'Suspense');
INSERT INTO `t_tematica` VALUES (20, 'PolicÃ­aca');
INSERT INTO `t_tematica` VALUES (21, 'Navidad');
INSERT INTO `t_tematica` VALUES (22, 'Naturaleza');
INSERT INTO `t_tematica` VALUES (23, 'Misterio');
INSERT INTO `t_tematica` VALUES (24, 'Medieval');
INSERT INTO `t_tematica` VALUES (25, 'Magia');
INSERT INTO `t_tematica` VALUES (26, 'Leyenda');
INSERT INTO `t_tematica` VALUES (27, 'Drama');
INSERT INTO `t_tematica` VALUES (28, 'Comedia');
INSERT INTO `t_tematica` VALUES (29, 'Espionaje');
INSERT INTO `t_tematica` VALUES (30, 'DistopÃ­a');
INSERT INTO `t_tematica` VALUES (31, 'Familia');
INSERT INTO `t_tematica` VALUES (32, 'Zombis');
INSERT INTO `t_tematica` VALUES (33, 'Vampiros');
INSERT INTO `t_tematica` VALUES (34, 'Sobrenatural');
INSERT INTO `t_tematica` VALUES (35, 'Adolescente');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_tipo_edicion`
-- 

CREATE TABLE `t_tipo_edicion` (
  `id_tipo_edicion` int(11) NOT NULL auto_increment,
  `descripcion_edicion` varchar(30) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`id_tipo_edicion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=6 ;

-- 
-- Volcar la base de datos para la tabla `t_tipo_edicion`
-- 

INSERT INTO `t_tipo_edicion` VALUES (1, 'Tapa dura');
INSERT INTO `t_tipo_edicion` VALUES (2, 'RÃºstica');
INSERT INTO `t_tipo_edicion` VALUES (3, 'CartonÃ©');
INSERT INTO `t_tipo_edicion` VALUES (4, 'Tapa blanda');
INSERT INTO `t_tipo_edicion` VALUES (5, 'Bolsillo');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_tipo_usuario`
-- 

CREATE TABLE `t_tipo_usuario` (
  `id_tipo_usuario` int(11) NOT NULL auto_increment,
  `descripcion_tipo_usuario` varchar(50) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`id_tipo_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=3 ;

-- 
-- Volcar la base de datos para la tabla `t_tipo_usuario`
-- 

INSERT INTO `t_tipo_usuario` VALUES (1, 'admin');
INSERT INTO `t_tipo_usuario` VALUES (2, 'usuario');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_usuarios`
-- 

CREATE TABLE `t_usuarios` (
  `id_usuario` int(11) NOT NULL auto_increment,
  `nombre_usuario` varchar(50) collate utf8_spanish_ci NOT NULL,
  `email` varchar(80) collate utf8_spanish_ci NOT NULL,
  `passw` varchar(200) collate utf8_spanish_ci NOT NULL,
  `imagen` varchar(200) collate utf8_spanish_ci default NULL,
  `fecha_nacimiento` date default NULL,
  `facebook` varchar(100) collate utf8_spanish_ci default NULL,
  `id_tipo_usuario` int(11) NOT NULL,
  PRIMARY KEY  (`id_usuario`),
  KEY `fk_tipo_usuario` (`id_tipo_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=13 ;

-- 
-- Volcar la base de datos para la tabla `t_usuarios`
-- 

INSERT INTO `t_usuarios` VALUES (11, 'Derek Smeath', 'collector@bookworm.es', '7bae163f40156f1b24ee1f953f64314c', 'default.JPG', '0000-00-00', 'null', 2);
INSERT INTO `t_usuarios` VALUES (12, 'Admin', 'admin@bookworm.es', '4297f44b13955235245b2497399d7a93', 'default.JPG', NULL, NULL, 1);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_usuario_libros`
-- 

CREATE TABLE `t_usuario_libros` (
  `id_usuario` int(11) NOT NULL,
  `isbn` varchar(20) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`id_usuario`,`isbn`),
  KEY `fk_usuario_lib` (`id_usuario`),
  KEY `fk_libro_usua` (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- 
-- Volcar la base de datos para la tabla `t_usuario_libros`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `t_usuario_usuario`
-- 

CREATE TABLE `t_usuario_usuario` (
  `id_usuario_inv` int(11) NOT NULL,
  `id_usuario_rec` int(11) NOT NULL,
  `fecha_inv` date NOT NULL,
  `aceptada` tinyint(1) default NULL,
  `enlace` varchar(200) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`id_usuario_inv`,`id_usuario_rec`),
  UNIQUE KEY `enlace` (`enlace`),
  KEY `fk_usuario_inv` (`id_usuario_inv`),
  KEY `fk_usuario_rec` (`id_usuario_rec`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- 
-- Volcar la base de datos para la tabla `t_usuario_usuario`
-- 


-- 
-- Filtros para las tablas descargadas (dump)
-- 

-- 
-- Filtros para la tabla `t_citas`
-- 
ALTER TABLE `t_citas`
  ADD CONSTRAINT `t_citas_ibfk_1` FOREIGN KEY (`id_autor`) REFERENCES `t_autores` (`id_autor`);

-- 
-- Filtros para la tabla `t_comentarios`
-- 
ALTER TABLE `t_comentarios`
  ADD CONSTRAINT `t_comentarios_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `t_usuarios` (`id_usuario`),
  ADD CONSTRAINT `t_comentarios_ibfk_2` FOREIGN KEY (`isbn`) REFERENCES `t_libros` (`isbn`);

-- 
-- Filtros para la tabla `t_denuncias`
-- 
ALTER TABLE `t_denuncias`
  ADD CONSTRAINT `t_denuncias_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `t_usuarios` (`id_usuario`),
  ADD CONSTRAINT `t_denuncias_ibfk_2` FOREIGN KEY (`isbn`) REFERENCES `t_libros` (`isbn`);

-- 
-- Filtros para la tabla `t_invitacion`
-- 
ALTER TABLE `t_invitacion`
  ADD CONSTRAINT `t_invitacion_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `t_usuarios` (`id_usuario`);

-- 
-- Filtros para la tabla `t_libros`
-- 
ALTER TABLE `t_libros`
  ADD CONSTRAINT `t_libros_ibfk_1` FOREIGN KEY (`id_editorial`) REFERENCES `t_editoriales` (`id_editorial`),
  ADD CONSTRAINT `t_libros_ibfk_2` FOREIGN KEY (`id_tipo_edicion`) REFERENCES `t_tipo_edicion` (`id_tipo_edicion`),
  ADD CONSTRAINT `t_libros_ibfk_3` FOREIGN KEY (`id_usuario`) REFERENCES `t_usuarios` (`id_usuario`);

-- 
-- Filtros para la tabla `t_libros_autores`
-- 
ALTER TABLE `t_libros_autores`
  ADD CONSTRAINT `t_libros_autores_ibfk_2` FOREIGN KEY (`isbn`) REFERENCES `t_libros` (`isbn`),
  ADD CONSTRAINT `t_libros_autores_ibfk_3` FOREIGN KEY (`id_autor`) REFERENCES `t_autores` (`id_autor`);

-- 
-- Filtros para la tabla `t_libros_tematica`
-- 
ALTER TABLE `t_libros_tematica`
  ADD CONSTRAINT `t_libros_tematica_ibfk_2` FOREIGN KEY (`isbn`) REFERENCES `t_libros` (`isbn`),
  ADD CONSTRAINT `t_libros_tematica_ibfk_3` FOREIGN KEY (`id_tematica`) REFERENCES `t_tematica` (`id_tematica`);

-- 
-- Filtros para la tabla `t_usuarios`
-- 
ALTER TABLE `t_usuarios`
  ADD CONSTRAINT `t_usuarios_ibfk_1` FOREIGN KEY (`id_tipo_usuario`) REFERENCES `t_tipo_usuario` (`id_tipo_usuario`);

-- 
-- Filtros para la tabla `t_usuario_libros`
-- 
ALTER TABLE `t_usuario_libros`
  ADD CONSTRAINT `t_usuario_libros_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `t_usuarios` (`id_usuario`),
  ADD CONSTRAINT `t_usuario_libros_ibfk_2` FOREIGN KEY (`isbn`) REFERENCES `t_libros` (`isbn`);

-- 
-- Filtros para la tabla `t_usuario_usuario`
-- 
ALTER TABLE `t_usuario_usuario`
  ADD CONSTRAINT `t_usuario_usuario_ibfk_1` FOREIGN KEY (`id_usuario_inv`) REFERENCES `t_usuarios` (`id_usuario`),
  ADD CONSTRAINT `t_usuario_usuario_ibfk_2` FOREIGN KEY (`id_usuario_rec`) REFERENCES `t_usuarios` (`id_usuario`);
