<?php
session_start();
require_once 'utilidades/GestionCitas.php';
require_once 'utilidades/constant.php';
global $raiz;

$imagenes = GestionCitas::generarImagenes(1);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm</title>
        <?php require_once 'parts/head.php'; ?>
        <script>
            function enviarOlvidar() {
                error = false;
                //Email
                if (jQuery("#email").val() == '') {
                    jQuery("#email").attr("style", "border: 1px solid #D90416");
                    error = true;
                } else {
                    jQuery("#email").attr("style", "border: 1px solid #CCC;");
                    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(jQuery("#email").val())){
                        jQuery("#email").attr("style", "border: 1px solid #CCC;");
                    } else {
                        jQuery("#email").attr("style", "border: 1px solid #D90416");
                        error = true;
                    }
                }

                //Si todo "ok", enviar formulario
                if (!error) {
                    jQuery.post("ajax/ajaxValidation.php", 
                        {campo: 'email', tabla: 't_usuarios', valor: jQuery("#email").val()}, 
                        function(data){
                            if (data.exists) {
                                jQuery.post("ajax/forgot.php", 
                                {email: jQuery("#email").val()}, 
                                function(success){
                                    if(success) {
                                        $('#okModal').modal({
                                            backdrop: "static",
                                            show: true
                                        });
                                    } else {
                                        alert('Se ha producido un error al intentar enviar tu solicitud. Por favor, inténtalo de nuevo más tarde');
                                    }
                                }, 
                                "json");
                            } else {
                                jQuery("#input01M").attr("style", "border: 1px solid #D90416");
                                $('#errorModal').modal({
                                    backdrop: "static",
                                    show: true
                                });
                            }
                        }, 
                        "json");
                }
            }
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "index.php" ?>
            <?php require_once 'parts/menu.php'; ?>

            <div class="row" style="padding-top: 60px;">
                <!-- MAIN CONTENT -->
                <div class="span12">
                    <div class="contact-content">
                        <div class="page-header">
                            <h1>¿Has olvidado tu contraseña? <small>Escribe tu correo electrónico<br />
                                    y te enviaremos una nueva para que puedas seguir navegando por <a href="<?php echo $raiz; ?>/index.php" style="color: #D90416;">BookWorm</a></small></h1>
                        </div>
                        <form class="form-horizontal">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="email">Email *</label>
                                    <div class="controls">
                                        <input type="text" class="input-xlarge" id="email" name="email" />
                                        <a href="#" class="btn" onclick="enviarOlvidar()"><i class="icon-arrow-up"></i> Enviar</a>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                        <div align="center">
                            <img src="<?php echo $raiz; ?>/img/citas/<?php echo $imagenes[0]; ?>" alt="" width="850" />
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- ERROR MODAL -->
            <div id="errorModal" class="modal hide fade" >
                <div class="modal-header">
                    <a class="close" data-dismiss="modal">×</a>
                    <h3>Error</h3>
                </div>
                <div class="modal-body">
                    El email introducido no pertenece a ningún usuario de BookWorm.<br /><br />
                    ¿Te has <a href="<?php echo $raiz; ?>/register.php">registrado</a>?
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-ok"></i> Aceptar</a>
                </div> 
            </div>
            
            <!-- OK MODAL -->
            <div id="okModal" class="modal hide fade" >
                <div class="modal-header">
                    <a class="close" data-dismiss="modal">×</a>
                    <h3>Enviado</h3>
                </div>
                <div class="modal-body">
                    Te hemos enviado tu nueva contraseña.<br /><br />
                    Si no ves el email en tu bandeja de entrada, revisa la carpeta de spam.<br /><br />
                    ¡Hasta pronto!
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-ok"></i> Aceptar</a>
                </div> 
            </div>

            <?php require_once 'parts/footer.php'; ?>
        </div>
    </body>
</html>
