<?php
session_start();
require_once 'utilidades/GestionTematica.php';
require_once 'utilidades/GestionAutores.php';
require_once 'utilidades/clases/Autor.php';
require_once 'utilidades/clases/Tematica.php';
require_once 'utilidades/filter.php';
require_once 'utilidades/constant.php';
global $raiz;

if (!isset($_POST) || $_POST == null || count($_POST) == 0) {
    $_POST = $_SESSION["data_books"];
}

$filtro = $_POST["filtro"];
switch ($filtro) {
    case 'titulo':
        $campo = "Resultados: " . $_POST["titulo"] . " (título)";
        break;
    case 'isbn':
        $campo = "Resultados: " . $_POST["isbn"] . " (ISBN)";
        break;
    case 'autor':
        $autor = new Autor();
        $autor = GestionAutores::recuperarAutor($_POST["autor"]);
        $campo = "Resultados: " . $autor->getNombre() . " (autor)";
        break;
    case 'tematica':
        $tematica = new Tematica();
        $tematica = GestionTematica::recuperarTematica($_POST["tematica"]);
        $campo = "Resultados: " . $tematica->getDescripcion() . " (temática)";
        break;
    case 'avanzada':
        $campo = "Resultados: búsqueda avanzada";
        break;
    default:
        break;
}

$_SESSION["data_books"] = $_POST;
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm - Resultados de búsqueda</title>
        <?php require_once 'parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/login-validation.js"></script>
        <script>
            jQuery(document).ready(function() {
                jQuery('#books_table').dataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": false,
                    "bInfo": false,
                    "oLanguage": {
                        "sUrl": "dataTableUtils/spanish"
                    },
                    "sPaginationType": "full_numbers",
                    "sAjaxSource": "dataTableUtils/search_books.php"
                } );
            });
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "search.php" ?>
            <?php require_once 'parts/menu.php'; ?>
            <?php require_once 'parts/carousel.php'; ?>
            <?php require_once 'parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab"><?php echo $campo; ?></a></li>
                        </ul>
                        <div class="tab-content">
                            <!-- PESTAÑA 1 -->
                            <div class="tab-pane active" id="1">
                                <div align="right" style="margin: 15px;">
                                    <a href="<?php echo $raiz; ?>/search.php" class="btn">
                                        <i class="icon-arrow-left"></i> Volver</a>
                                </div>
                                <hr>
                                <table id="books_table" class="table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>ISBN</th>
                                            <th>Título</th>
                                            <th>Autor/es</th>
                                            <th class="icons-column">Detalles</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SIDEBAR -->
                <?php require_once 'parts/sidebar.php'; ?>
            </div>

            <?php require_once 'parts/footer.php'; ?>
        </div>
    </body>
</html>