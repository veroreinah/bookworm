<?php
require_once 'conexion.php';
require_once 'clases/Cita.php';
require_once 'clases/Autor.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GestionCitas
 *
 * @author Vero
 */
class GestionCitas {
    
    public static function crearCita($cita) {
        global $conexion;
        $c = new Cita();
        $c = $cita;
        $autor = new Autor();
        $autor = $c->getAutor();

        try {
            $query = "insert into t_citas
                (texto, id_autor)
                    values ('" . $c->getTexto() . "',
                            '" . $autor->getId() . "')";
            $result = mysql_query($query, $conexion);
            $_SESSION["insertada"] = "La cita se ha insertado con éxito.";
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido crear la nueva cita. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function modificarCita($cita) {
        global $conexion;
        $c = new Cita();
        $c = $cita;
        $autor = new Autor();
        $autor = $c->getAutor();

        try {
            $query = "update t_citas
                set texto = '" . $c->getTexto() . "',
                    id_autor = '" . $autor->getId() . "'
                    where id_cita = '" . $c->getId() . "'";
            $result = mysql_query($query, $conexion);
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido modificar la cita. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function eliminarCita($id) {
        global $conexion;

        try {
            $query = "delete from t_citas
                where id_cita = '" . $id . "'";
            $result = mysql_query($query, $conexion);
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido eliminar la cita. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalCitas() {
        global $conexion;

        try {
            $query = "select count(*) as total
                from t_citas";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            $total = $row["total"];
            
            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las citas. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalFiltered($where) {
        global $conexion;
        
        if ($where != "") {
            $w = " where texto like '%$where%' or nombre_autor like '%$where%' ";
        }

        try {
            $query = "select count(id_cita) as total 
                from t_citas inner join t_autores
                on t_citas.id_autor = t_autores.id_autor $w";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            $total = $row["total"];
            
            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las citas. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarCitasL($begin, $limit, $field="", $order="", $where="") {
        global $conexion;
        $citas = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }
        
        if ($field != "" && $order != "") {
            $orderBy = " order by $field $order ";
        }
        
        if ($where != "") {
            $w = " where texto like '%$where%' or nombre_autor like '%$where%' ";
        }

        try {
            $query = "select id_cita, texto, t_citas.id_autor, nombre_autor
                from t_citas inner join t_autores
                on t_citas.id_autor = t_autores.id_autor";
            if (isset($w)) {
                $query = $query . $w;
            }
            if (isset($orderBy)) {
                $query = $query . $orderBy;
            }
            if (isset($l)) {
                $query = $query . $l;
            }
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $cita = new Cita();
                $cita->setId($row["id_cita"]);
                $cita->setTexto($row["texto"]);
                
                $autor = new Autor();
                $autor->setId($row["id_autor"]);
                $autor->setNombre($row["nombre_autor"]);
                
                $cita->setAutor($autor);
                
                $citas[] = $cita;
            }
            
            return $citas;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las citas. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarCitas() {
        global $conexion;
        $citas = array();

        try {
            $query = "select id_cita, texto, t_citas.id_autor, nombre_autor
                from t_citas inner join t_autores
                on t_citas.id_autor = t_autores.id_autor
                order by id_cita";
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $cita = new Cita();
                $cita->setId($row["id_cita"]);
                $cita->setTexto($row["texto"]);
                
                $autor = new Autor();
                $autor->setId($row["id_autor"]);
                $autor->setNombre($row["nombre_autor"]);
                
                $cita->setAutor($autor);
                
                $citas[] = $cita;
            }
            
            if (count($citas) > 0) {
                return $citas;
            } else {
                $_SESSION["noHay"] = "No existe ninguna cita en BookWorm.";
                return 0;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las citas. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function generarAleatorios($cuantos) {
        $total = GestionCitas::totalCitas();
        $aleatorios = array();
        if ($cuantos >= $total) {
            for ($i = 0; $i < $cuantos; $i++) {
                $aleatorios[] = $i+1;
            }
        } else {
            for ($i = 0; $i < $cuantos; $i++) {
                do {
                    $existe = false;
                    $numero = mt_rand(1, $total);
                    foreach ($aleatorios as $n) {
                        if ($n == $numero) {
                            $existe = true;
                        }
                    }
                } while ($existe);
                $aleatorios[] = $numero;
            }
        }
        return $aleatorios;
    }
    
    public static function generarImagenes($cuantos) {
        $total = 12;
        $imagenes = array();
        if ($cuantos >= $total) {
            for ($i = 0; $i < $cuantos; $i++) {
                $imagenes[] = $i+1;
            }
        } else {
            for ($i = 0; $i < $cuantos; $i++) {
                do {
                    $existe = false;
                    $numero = mt_rand(1, $total);
                    foreach ($imagenes as $n) {
                        if ($n == $numero) {
                            $existe = true;
                        }
                    }
                } while ($existe);
                $imagenes[] = $numero;
            }
        }
        return $imagenes;
    }
}

?>
