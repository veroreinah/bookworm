<?php
require_once 'conexion.php';
require_once 'clases/Editorial.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GestionCitas
 *
 * @author Vero
 */
class GestionEditoriales {
    
    public static function crearEditorial($editorial) {
        global $conexion;
        $e = new Editorial();
        $e = $editorial;

        try {
            $query = "insert into t_editoriales
                (nombre_editorial)
                    values ('" . $e->getNombre() . "')";
            $result = mysql_query($query, $conexion);
            $_SESSION["insertada"] = "La editorial se ha insertado con éxito.";
            
            return mysql_insert_id($conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido crear la nueva editorial. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function modificarEditorial($editorial) {
        global $conexion;
        $e = new Editorial();
        $e = $editorial;

        try {
            $query = "update t_editoriales
                set nombre_editorial = '" . $e->getNombre() . "'
                    where id_editorial = '" . $e->getId() . "'";
            $result = mysql_query($query, $conexion);
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido modificar la editorial. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function eliminarEditorial($id) {
        global $conexion;

        try {
            $query = "delete from t_editoriales
                where id_editorial = '" . $id . "'";
            $result = mysql_query($query, $conexion);
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido eliminar la editorial. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalEditoriales() {
        global $conexion;

        try {
            $query = "select count(*) as total
                from t_editoriales";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            $total = $row["total"];
            
            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las editoriales. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalFiltered($where) {
        global $conexion;
        
        if ($where != "") {
            $w = " where nombre_editorial like '%$where%' ";
        }

        try {
            $query = "select count(*) as total 
                from t_editoriales 
                $w";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            $total = $row["total"];
            
            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las editoriales. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarEditorialesL($begin, $limit, $field, $order, $where) {
        global $conexion;
        $editoriales = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }
        
        if ($field != "" && $order != "") {
            $orderBy = " order by $field $order ";
        }
        
        if ($where != "") {
            $w = " where nombre_editorial like '%$where%' ";
        }

        try {
            $query = "select *
                from t_editoriales";
            if (isset($w)) {
                $query = $query . $w;
            }
            if (isset($orderBy)) {
                $query = $query . $orderBy;
            }
            if (isset($l)) {
                $query = $query . $l;
            }
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $editorial = new Editorial();
                $editorial->setId($row["id_editorial"]);
                $editorial->setNombre($row["nombre_editorial"]);
                
                $editoriales[] = $editorial;
            }
            
            return $editoriales;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las editoriales. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarEditoriales() {
        global $conexion;
        $editoriales = array();

        try {
            $query = "select *
                from t_editoriales
                order by nombre_editorial";
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $editorial = new Editorial();
                $editorial->setId($row["id_editorial"]);
                $editorial->setNombre($row["nombre_editorial"]);
                
                $editoriales[] = $editorial;
            }
            
            if (count($editoriales) > 0) {
                return $editoriales;
            } else {
                $_SESSION["noHay"] = "No existe ninguna editorial en BookWorm.";
                return 0;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las editoriales. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarEditorial($id) {
        global $conexion;

        try {
            $query = "select *
                from t_editoriales
                where id_editorial = $id";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            if(!$row) {
                return 0;
            } else {
                $editorial = new Editorial();
                $editorial->setId($row["id_editorial"]);
                $editorial->setNombre($row["nombre_editorial"]);
                
                return $editorial;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar la editorial. Inténtelo de nuevo más tarde.";
        }
    }
}

?>
