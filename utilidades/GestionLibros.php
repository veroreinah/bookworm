<?php

require_once 'conexion.php';
require_once 'GestionUsuarios.php';
require_once 'clases/Usuario.php';
require_once 'clases/Libro.php';
require_once 'GestionEditoriales.php';
require_once 'clases/Editorial.php';
require_once 'GestionTipoEdicion.php';
require_once 'clases/TipoEdicion.php';
require_once 'clases/Tematica.php';
require_once 'clases/Comentario.php';
require_once 'clases/Autor.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GestionLibros
 *
 * @author Vero
 */
class GestionLibros {

    public static function crearLibro($libro) {
        global $conexion;
        $l = new Libro();
        $l = $libro;
        
        $insert = "(isbn, titulo, subtitulo, paginas, portada, sinopsis, anyo_publicacion, edicion, ";
        
        $e = new Editorial();
        $e = $l->getEditorial();
        if ($e != null) {
            $editorial = "'" . $e->getId() . "',";
            $insert .= " id_editorial, ";
        } else {
            $editorial = "";
        }
        $t = new TipoEdicion();
        $t = $l->getTipoEdicion();
        if ($t != null) {
            $tipo = "'" . $t->getId() . "',";
            $insert .= "  id_tipo_edicion, ";
        } else {
            $tipo = "";
        }
        $u = new Usuario();
        $u = $l->getUsuario();

        $tematicas = array();
        $tematicas = $l->getTematicas();
        $autores = array();
        $autores = $l->getAutores();
    
        $insert .= " id_usuario, fecha)";
        try {
            $query = "insert into t_libros
                $insert
                    values ('" . $l->getIsbn() . "',
                            '" . $l->getTitulo() . "',
                            '" . $l->getSubtitulo() . "',
                            '" . $l->getPaginas() . "',
                            '" . $l->getPortada() . "',
                            \"" . mysql_escape_string($l->getSinopsis()) . "\",
                            '" . $l->getAnyoPublicacion() . "',
                            '" . $l->getEdicion() . "',
                            $editorial
                            $tipo
                            '" . $u->getId() . "',
                            '" . $l->getFecha() . "')";
            
            $result = mysql_query($query, $conexion);
            if ($tematicas != null && count($tematicas) > 0) {
                foreach ($tematicas as $tem) {
                    $tematica = new Tematica();
                    $tematica = $tem;
                    $query2 = "insert into t_libros_tematica (isbn, id_tematica) 
                        values ('" . $l->getIsbn() . "', '" . $tematica->getId() . "')";
                    $result2 = mysql_query($query2, $conexion);
                }
            }
            foreach ($autores as $aut) {
                $autor = new Autor();
                $autor = $aut;
                $query3 = "insert into t_libros_autores (isbn, id_autor) 
                    values ('" . $l->getIsbn() . "', '" . $autor->getId() . "')";
                $result3 = mysql_query($query3, $conexion);
            }
            
            $_SESSION["insertada"] = "El libro se ha insertado con éxito.";
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido crear el libro. Inténtelo de nuevo más tarde.";
        }
    }

    public static function agregarLibro($idUsuario, $isbn) {
        global $conexion;

        try {
            $query = "insert into t_usuario_libros
                (id_usuario, isbn)
                    values ('" . $idUsuario . "',
                            '" . $isbn . "')";
            $result = mysql_query($query, $conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido agregar el libro a su colección. Inténtelo de nuevo más tarde.";
        }
    }

    public static function modificarLibro($libro) {
        //TODO terminar!!
        global $conexion;
        $l = new Libro();
        $l = $libro;
        $e = new Editorial();
        $e = $l->getEditorial();
        $t = new TipoEdicion();
        $t = $l->getTipoEdicion();
        $u = new Usuario();
        $u = $l->getUsuario();

        $tematicas = array();
        $tematicas = $l->getTematicas();
        $autores = array();
        $autores = $l->getAutores();
        
        $portada = "";
        if ($l->getPortada() == "") {
            
        } else {
            $portada = " portada = '" . $l->getPortada() . "',";
        }
        $subtitulo = "";
        if (trim($l->getSubtitulo()) == "") {
            
        } else {
            $subtitulo = " subtitulo = '" . $l->getSubtitulo() . "',";
        }
        $paginas = "";
        if (trim($l->getPaginas()) == "") {
            
        } else {
            $paginas = " paginas = '" . $l->getPaginas() . "',";
        }
        $sinopsis = "";
        if (trim($l->getSinopsis()) == "" || $l->getSinopsis() == '<p><br></p>') {
            
        } else {
            $sinopsis = " sinopsis = '" . mysql_escape_string($l->getSinopsis()) . "',";
        }
        $anyo = "";
        if (trim($l->getAnyoPublicacion()) == "") {
            
        } else {
            $anyo = " anyo_publicacion = '" . $l->getAnyoPublicacion() . "',";
        }
        $edicion = "";
        if (trim($l->getEdicion()) == "") {
            
        } else {
            $edicion = " edicion = '" . $l->getEdicion() . "',";
        }
        $editorial = "";
        if ($l->getEditorial() == null) {
            
        } else {
            $editorial = " id_editorial = '" . $e->getId() . "',";
        }
        $tipoEditorial = "";
        if ($l->getTipoEdicion() == null) {
            
        } else {
            $tipoEditorial = " id_tipo_edicion = '" . $t->getId() . "',";
        }
        
        try {
            $query = "update t_libros
                set titulo = '" . $l->getTitulo() . "',
                    $subtitulo
                    $paginas
                    $portada
                    $sinopsis
                    $anyo
                    $edicion
                    $editorial
                    $tipoEditorial
                    id_usuario = '" . $u->getId() . "'
                where isbn like '" . $l->getIsbn() . "'";
            $result = mysql_query($query, $conexion);
            if (!empty($tematicas)) {
                $query2 = "delete from t_libros_tematica where isbn like '" . $l->getIsbn() . "'";
                $result2 = mysql_query($query2, $conexion);
                foreach ($tematicas as $tem) {
                    $tematica = new Tematica();
                    $tematica = $tem;
                    $query3 = "insert into t_libros_tematica (isbn, id_tematica) 
                        values ('" . $l->getIsbn() . "', '" . $tematica->getId() . "')";
                    $result3 = mysql_query($query3, $conexion);
                }
            }
            $query4 = "delete from t_libros_autores where isbn like '" . $l->getIsbn() . "'";
            $result4 = mysql_query($query4, $conexion);
            foreach ($autores as $aut) {
                $autor = new Autor();
                $autor = $aut;
                $query5 = "insert into t_libros_autores (isbn, id_autor) 
                    values ('" . $l->getIsbn() . "', '" . $autor->getId() . "')";
                $result5 = mysql_query($query5, $conexion);
            }
            
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido modificar el libro. Inténtelo de nuevo más tarde.";
        }
    }

    public static function quitarLibro($idUsuario, $isbn) {
        global $conexion;

        try {
            $query = "delete from t_usuario_libros
                where id_usuario = '" . $idUsuario . "'
                and isbn = '" . $isbn . "'";
            $result = mysql_query($query, $conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido eliminar el libro de su colección. Inténtelo de nuevo más tarde.";
        }
    }

    public static function eliminarLibro($isbn) {
        global $conexion;

        try {
            $query0 = "update t_libros set id_editorial = null, id_tipo_edicion = null, id_usuario = null where isbn = '" . $isbn . "'";
            $query1 = "delete from t_comentarios where isbn = '" . $isbn . "'";
            $query2 = "delete from t_denuncias where isbn = '" . $isbn . "'";
            $query3 = "delete from t_libros_autores where isbn = '" . $isbn . "'";
            $query4 = "delete from t_libros_tematica where isbn = '" . $isbn . "'";
            $query5 = "delete from t_usuario_libros where isbn = '" . $isbn . "'";
            $query6 = "delete from t_libros where isbn = '" . $isbn . "'";
            $result = mysql_query($query0, $conexion);
            $result = mysql_query($query1, $conexion);
            $result = mysql_query($query2, $conexion);
            $result = mysql_query($query3, $conexion);
            $result = mysql_query($query4, $conexion);
            $result = mysql_query($query5, $conexion);
            $result = mysql_query($query6, $conexion);
            
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido eliminar el libro. Inténtelo de nuevo más tarde.";
        }
    }

    public static function recuperarLibro($isbn, $id_usuario = 0) {
        global $conexion;

        $libro = new Libro();

        try {
            $query = "select * from t_libros where isbn like '$isbn'";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            if (!$row) {
                $_SESSION["noHay"] = "No existe ningún libro con ese ISBN en BookWorm.";
                return 0;
            } else {
                $libro->setIsbn($isbn);
                $libro->setTitulo($row['titulo']);
                $libro->setSubtitulo($row['subtitulo']);
                $libro->setPaginas($row['paginas']);
                $libro->setPortada($row['portada']);
                $libro->setSinopsis($row['sinopsis']);
                $libro->setAnyoPublicacion($row['anyo_publicacion']);
                $libro->setEdicion($row['edicion']);
                $libro->setFecha($row['fecha']);
                
                if ($row['id_editorial'] != null) {
                    $editorial = new Editorial();
                    $editorial = GestionEditoriales::recuperarEditorial($row['id_editorial']);
                    $libro->setEditorial($editorial);
                }
                
                if ($row['id_tipo_edicion'] != null) {
                    $tipo_edicion = new TipoEdicion();
                    $tipo_edicion = GestionTipoEdicion::recuperarTipoEdicion($row['id_tipo_edicion']);
                    $libro->setTipoEdicion($tipo_edicion);
                }
                
                $usuario = new Usuario();
                $usuario = GestionUsuarios::recuperarUsuario($row['id_usuario']);
                $libro->setUsuario($usuario);
                
                if ($id_usuario != 0) {
                    $query2 = "select * from t_comentarios where isbn like '$isbn' and id_usuario = $id_usuario";
                    $result2 = mysql_query($query2, $conexion);
                    $row2 = mysql_fetch_array($result2);
                    
                    if ($row2) {
                        $libro->setValoracionUsuario($row2['valoracion']);
                    }
                }
                
                $query3 = "select avg(valoracion) as media from t_comentarios where isbn like '$isbn'";
                $result3 = mysql_query($query3, $conexion);
                $row3 = mysql_fetch_array($result3);

                if ($row3) {
                    $libro->setValoracionMedia($row3['media']);
                }
                
                $tematicas = array();
                $query4 = "select t.* 
                    from t_tematica as t inner join  t_libros_tematica as lt 
                    on t.id_tematica = lt.id_tematica
                    where isbn like '$isbn'";
                $result4 = mysql_query($query4, $conexion);
                
                while($row4 = mysql_fetch_array($result4)) {
                    $tematica = new Tematica();
                    $tematica->setId($row4["id_tematica"]);
                    $tematica->setDescripcion($row4["descripcion_tematica"]);

                    $tematicas[] = $tematica;
                }
                
                $libro->setTematicas($tematicas);
                
                $autores = array();
                $query5 = "select a.* 
                    from t_autores as a inner join  t_libros_autores as la 
                    on a.id_autor = la.id_autor
                    where isbn like '$isbn'";
                $result5 = mysql_query($query5, $conexion);
                
                while($row5 = mysql_fetch_array($result5)) {
                    $autor = new Autor();
                    $autor->setId($row5["id_autor"]);
                    $autor->setNombre($row5["nombre_autor"]);

                    $autores[] = $autor;
                }
                
                $libro->setAutores($autores);
                
                return $libro;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido recuperar el libro. Inténtelo de nuevo más tarde.";
        }
    }

    public static function recuperarLibros() {
        global $conexion;
        
        try {
            $query = "select * from t_libros";
            $result = mysql_query($query, $conexion);

            $libros = array();

            while($row = mysql_fetch_array($result)) {
                $libro = new Libro();
                $libro = GestionLibros::recuperarLibro($row['isbn']);
                
                $libros[] = $libro;
            }
            
            if ($libros != null && count($libros) > 0) {
                
            } else {
                $_SESSION["noHayL"] = "No existe ningún libro en BookWorm.";
            }
            return $libros;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los libros. Inténtelo de nuevo más tarde.";
        }
    }

    public static function recuperarLibrosL($begin, $limit, $field="", $order="", $where="") {
        global $conexion;
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }
        
        if ($field != "" && $order != "") {
            $orderBy = " order by $field $order ";
        }
        
        if ($where != "") {
            $w = " where l.isbn like '%$where%' 
                or l.titulo like '%$where%' 
                or a.nombre_autor like '%$where%' 
                or u.nombre_usuario like '%$where%' ";
        }
        
        try {
            $query = "select distinct l.isbn 
                from t_libros as l inner join t_usuarios as u 
                on l.id_usuario = u.id_usuario 
                inner join t_libros_autores as la 
                on l.isbn = la.isbn 
                inner join t_autores as a 
                on la.id_autor = a.id_autor ";
            
            if (isset($w)) {
                $query = $query . $w;
            }
            if (isset($orderBy)) {
                $query = $query . $orderBy;
            }
            if (isset($l)) {
                $query = $query . $l;
            }
            
            $result = mysql_query($query, $conexion);

            $libros = array();

            while($row = mysql_fetch_array($result)) {
                $libro = new Libro();
                $libro = GestionLibros::recuperarLibro($row['isbn']);
                
                $libros[] = $libro;
            }
            
            return $libros;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los libros. Inténtelo de nuevo más tarde.";
        }
    }

    public static function recuperarTop($cuantos) {
        global $conexion;
        
        try {
            $query = "select l.*, avg(valoracion) as media 
                from t_libros as l inner join t_comentarios as c
                on l.isbn = c.isbn 
                where valoracion is not null 
                group by l.isbn
                order by media desc
                limit 0, $cuantos";
            $result = mysql_query($query, $conexion);

            $libros = array();

            while($row = mysql_fetch_array($result)) {
                $libro = new Libro();
                $libro = GestionLibros::recuperarLibro($row['isbn']);
                
                $libros[] = $libro;
            }
            
            return $libros;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los libros. Inténtelo de nuevo más tarde.";
        }
    }

    public static function recuperarUltimos($cuantos) {
        global $conexion;
        
        try {
            $query = "select * 
                from t_libros 
                order by fecha desc
                limit 0, $cuantos";
            $result = mysql_query($query, $conexion);

            $libros = array();

            while($row = mysql_fetch_array($result)) {
                $libro = new Libro();
                $libro = GestionLibros::recuperarLibro($row['isbn']);
                
                $libros[] = $libro;
            }
            
            return $libros;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los libros. Inténtelo de nuevo más tarde.";
        }
    }

    public static function recuperarLibrosPorUsuario($begin, $limit, $field="", $order="", $where="", $id = 0) {
        global $conexion;
        if (intval($id) === 0) {
            $u = new Usuario();
            $u = unserialize($_SESSION["usuario"]);
            $id = $u->getId();
        }
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }
        
        if ($field != "" && $order != "") {
            $orderBy = " order by $field $order ";
        }
        
        if ($where != "") {
            $w = "  and (l.isbn like '%$where%' 
                or l.titulo like '%$where%' 
                or a.nombre_autor like '%$where%') ";
        }
        
        try {
            $query = "select distinct l.isbn 
                from t_libros as l inner join t_usuario_libros as ul 
                on l.isbn = ul.isbn 
                inner join t_libros_autores as la 
                on l.isbn = la.isbn 
                inner join t_autores as a 
                on la.id_autor = a.id_autor 
                where ul.id_usuario = $id";
            
            if (isset($w)) {
                $query = $query . $w;
            }
            if (isset($orderBy)) {
                $query = $query . $orderBy;
            }
            if (isset($l)) {
                $query = $query . $l;
            }
            
            $result = mysql_query($query, $conexion);

            $libros = array();

            while($row = mysql_fetch_array($result)) {
                $libro = new Libro();
                $libro = GestionLibros::recuperarLibro($row['isbn'], $id);
                
                $libros[] = $libro;
            }
            
            return $libros;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los libros. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarIsbnPorUsuario($id = 0) {
        global $conexion;
        if (intval($id) === 0) {
            $u = new Usuario();
            $u = unserialize($_SESSION["usuario"]);
            $id = $u->getId();
        }
        
        try {
            $query = "select distinct l.isbn 
                from t_libros as l inner join t_usuario_libros as ul 
                on l.isbn = ul.isbn 
                inner join t_libros_autores as la 
                on l.isbn = la.isbn 
                inner join t_autores as a 
                on la.id_autor = a.id_autor 
                where ul.id_usuario = $id";
            
            $result = mysql_query($query, $conexion);

            $isbn = array();

            while($row = mysql_fetch_array($result)) {
                $isbn[] = $row['isbn'];
            }
            
            if (count($isbn) == 0) {
                $_SESSION["noHayE"] = "Todavía no has añadido ningún libro a tu estantería.";
            }
            
            return $isbn;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los libros. Inténtelo de nuevo más tarde.";
        }
    }

    public static function agregarValoracion($idUsuario, $isbn, $valoracion) {
        global $conexion;
        
        try {
            $query = "select * 
                from t_comentarios 
                where id_usuario = $idUsuario and isbn like '$isbn'";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            if(!$row) {
                $query2 = "insert into t_comentarios 
                    (id_usuario, isbn, valoracion)
                    values
                        ($idUsuario, '$isbn', $valoracion)";
            } else {
                $query2 = "update t_comentarios 
                    set valoracion = $valoracion 
                    where id_usuario = $idUsuario and isbn like '$isbn'";
            }
            $result2 = mysql_query($query2, $conexion);
            $_SESSION["ok"] = "La valoración se ha añadido correctamente.";
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido valorar el libro. Inténtelo de nuevo más tarde.";
        }
    }

    public static function agregarComentario($comentario) {
        global $conexion;
        $c = new Comentario();
        $c = $comentario;
        $u = new Usuario();
        $u = $c->getUsuario();
        $l = new Libro();
        $l = $c->getLibro();
        
        try {
            $query = "select * 
                from t_comentarios 
                where id_usuario = " . $u->getId() . " and isbn like '" . $l->getIsbn() . "'";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            if(!$row) {
                $query2 = "insert into t_comentarios 
                    (id_usuario, isbn, fecha, titulo, comentario, spoiler)
                    values
                        ('" . $u->getId() . "', 
                        '" . $l->getIsbn() . "', 
                        '" . $c->getFecha() . "', 
                        '" . $c->getTitulo() . "', 
                        '" . mysql_escape_string($c->getComentario()) . "', 
                        '" . $c->getSpoiler() . "')";
            } else {
                $query2 = "update t_comentarios 
                    set fecha = '" . $c->getFecha() . "',
                        titulo = '" . $c->getTitulo() . "',
                        comentario = '" . mysql_escape_string($c->getComentario()) . "',
                        spoiler = '" . $c->getSpoiler() . "'
                    where id_usuario = '" . $u->getId() . "' and isbn like '" . $l->getIsbn() . "'";
            }
            $result2 = mysql_query($query2, $conexion);
            $_SESSION["ok"] = "El comentario se ha añadido correctamente.";
            
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido añadir el comentario el libro. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarComentarios($isbn) {
        global $conexion;
        $comentarios = array();
        
        try {
            $query = "select * from t_comentarios 
                where isbn like '$isbn' and (comentario is not null or comentario != '') 
                order by fecha desc";
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $c = new Comentario();
                $c->setComentario($row["comentario"]);
                $c->setFecha($row["fecha"]);
                $c->setSpoiler($row["spoiler"]);
                $c->setTitulo($row["titulo"]);
                
                $u = new Usuario();
                $u = GestionUsuarios::recuperarUsuario($row["id_usuario"]);
                $l = new Libro();
                $l = GestionLibros::recuperarLibro($row["isbn"]);
                
                $c->setUsuario($u);
                $c->setLibro($l);
                
                $comentarios[] = $c;
            }
            
            if (count($comentarios) == 0) {
                $_SESSION["noHayC"] = "Todavía no se han añadido comentarios para este libro.";
            }
            
            return $comentarios;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido añadir el comentario el libro. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function tieneComentario($isbn) {
        global $conexion;
        $u = new Usuario();
        $u = unserialize($_SESSION["usuario"]);
        try {
            $query = "SELECT * 
                FROM t_comentarios  
                where id_usuario = '" . $u->getId() . "' and isbn like '$isbn' 
                    and (comentario is not null or comentario != '')";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            if (!$row) {
                return 0;
            } else {
                return 1;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido recuperar el comentario. Inténtelo de nuevo más tarde.";
        }
    }

    public static function totalLibros() {
        global $conexion;

        try {
            $query = "select count(*) as total
                from t_libros";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            $total = $row["total"];

            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los libros. Inténtelo de nuevo más tarde.";
        }
    }

    public static function totalLibrosPorUsuario($id = 0) {
        global $conexion;
        if (intval($id) === 0) {
            $u = new Usuario();
            $u = unserialize($_SESSION["usuario"]);
            $id = $u->getId();
        }
        
        try {
            $query = "select count(*) as total 
                from t_usuario_libros where id_usuario = $id";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            $total = $row["total"];

            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los libros. Inténtelo de nuevo más tarde.";
        }
    }

    public static function totalFiltered($where) {
        global $conexion;

        if ($where != "") {
            $w = " where l.isbn like '%$where%' 
                or l.titulo like '%$where%' 
                or a.nombre_autor like '%$where%' 
                or u.nombre_usuario like '%$where%' ";
        }

        try {
            $query = "select count(distinct l.isbn) as total 
                from t_libros as l inner join t_usuarios as u 
                on l.id_usuario = u.id_usuario 
                inner join t_libros_autores as la 
                on l.isbn = la.isbn 
                inner join t_autores as a 
                on la.id_autor = a.id_autor 
                $w";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            $total = $row["total"];

            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los libros. Inténtelo de nuevo más tarde.";
        }
    }

    public static function totalFilteredPorUsuario($where, $id = 0) {
        global $conexion;
        if (intval($id) === 0) {
            $u = new Usuario();
            $u = unserialize($_SESSION["usuario"]);
            $id = $u->getId();
        }

        if ($where != "") {
            $w = " and (l.isbn like '%$where%' 
                or l.titulo like '%$where%' 
                or a.nombre_autor like '%$where%') ";
        }

        try {
            $query = "select count(distinct l.isbn) as total 
                from t_libros as l inner join t_usuario_libros as ul 
                on l.isbn = ul.isbn 
                inner join t_libros_autores as la 
                on l.isbn = la.isbn 
                inner join t_autores as a 
                on la.id_autor = a.id_autor 
                where ul.id_usuario = $id $w";
            
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            $total = $row["total"];

            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los libros. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function existeLibro($isbn) {
        global $conexion;
        try {
            $query = "SELECT * 
                FROM t_libros  
                where isbn like '$isbn'";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            if (!$row) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido recuperar el libro. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function existe($campo, $tabla, $where) {
        global $conexion;
        try {
            $query = "SELECT $campo FROM $tabla $where";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            if (!$row) {
                return 0;
            } else {
                return $row["$campo"];
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido recuperar el libro. Inténtelo de nuevo más tarde.";
        }
    }

}

?>
