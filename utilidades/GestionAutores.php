<?php
require_once 'conexion.php';
require_once 'clases/Autor.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GestionCitas
 *
 * @author Vero
 */
class GestionAutores {
    
    public static function crearAutor($autor) {
        global $conexion;
        $a = new Autor();
        $a = $autor;

        try {
            $query = "insert into t_autores
                (nombre_autor)
                    values ('" . $a->getNombre() . "')";
            $result = mysql_query($query, $conexion);
            $_SESSION["insertada"] = "El autor se ha insertado con éxito.";
            
            return mysql_insert_id($conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido crear el nuevo autor. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function modificarAutor($autor) {
        global $conexion;
        $a = new Autor();
        $a = $autor;

        try {
            $query = "update t_autores
                set nombre_autor = '" . $a->getNombre() . "'
                    where id_autor = '" . $a->getId() . "'";
            $result = mysql_query($query, $conexion);
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido modificar el autor. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function eliminarAutor($id) {
        global $conexion;

        try {
            $query = "delete from t_autores
                where id_autor = '" . $id . "'";
            $result = mysql_query($query, $conexion);
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido eliminar el autor. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalAutores() {
        global $conexion;

        try {
            $query = "select count(*) as total
                from t_autores";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            $total = $row["total"];
            
            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los autores. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalFiltered($where="") {
        global $conexion;
        
        if ($where != "") {
            $w = " where nombre_autor like '%$where%' ";
        }

        try {
            $query = "select count(*) as total 
                from t_autores 
                $w";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            $total = $row["total"];
            
            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los autores. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarAutoresL($begin, $limit, $field="", $order="", $where="") {
        global $conexion;
        $autores = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }
        
        if ($field != "" && $order != "") {
            $orderBy = " order by $field $order ";
        }
        
        if ($where != "") {
            $w = " where nombre_autor like '%$where%' ";
        }

        try {
            $query = "select *
                from t_autores";
            if (isset($w)) {
                $query = $query . $w;
            }
            if (isset($orderBy)) {
                $query = $query . $orderBy;
            }
            if (isset($l)) {
                $query = $query . $l;
            }
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $autor = new Autor();
                $autor->setId($row["id_autor"]);
                $autor->setNombre($row["nombre_autor"]);
                
                $autores[] = $autor;
            }
            
            return $autores;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los autores. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarAutores() {
        global $conexion;
        $autores = array();

        try {
            $query = "select *
                from t_autores
                order by nombre_autor";
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $autor = new Autor();
                $autor->setId($row["id_autor"]);
                $autor->setNombre($row["nombre_autor"]);
                
                $autores[] = $autor;
            }
            
            if (count($autores) > 0) {
                return $autores;
            } else {
                $_SESSION["noHay"] = "No existe ningún autor en BookWorm.";
                return 0;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los autores. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarAutor($id) {
        global $conexion;

        try {
            $query = "select *
                from t_autores
                where id_autor = $id";
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $autor = new Autor();
                $autor->setId($row["id_autor"]);
                $autor->setNombre($row["nombre_autor"]);
                
                return $autor;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recupera el autor. Inténtelo de nuevo más tarde.";
        }
    }
}

?>
