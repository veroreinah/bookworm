<?php

require_once 'conexion.php';
require_once 'clases/Usuario.php';
require_once 'clases/TipoUsuario.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gestionUsuarios
 *
 * @author Vero
 */
class GestionAcceso {

    public static function registrarse($usuario) {
        global $conexion;
        $u = new Usuario();
        $u = $usuario;

        try {
            $query = "insert into t_usuarios
                (nombre_usuario, email, passw, imagen, fecha_nacimiento, facebook, id_tipo_usuario)
                    values ('" . $u->getNombre() . "',
                            '" . $u->getEmail() . "',
                            '" . md5($u->getPassword()) . "',
                            '" . $u->getImagen() . "',
                            '" . $u->getFechaNac() . "',
                            '" . $u->getFacebook() . "',
                            '2')";
            $result = mysql_query($query, $conexion);
            return mysql_insert_id($conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido registrar en este momento. Inténtelo de nuevo más tarde.";
        }
    }

    public static function login($email, $password) {
        global $conexion;
        try {
            $query = "SELECT id_usuario, nombre_usuario, email, passw, imagen, fecha_nacimiento, facebook, t_usuarios.id_tipo_usuario, descripcion_tipo_usuario 
                FROM t_usuarios inner join t_tipo_usuario 
                on t_usuarios.id_tipo_usuario = t_tipo_usuario.id_tipo_usuario 
                where email like '" . $email . "'";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            if (!$row || md5($password) != $row["passw"]) {
                $_SESSION["errorLogin"] = "Email y/o contraseña incorrectos";
                $_SESSION["emailLogin"] = $email;
                return false;
            } else {
                $usuario = new Usuario();
                $usuario->setId($row["id_usuario"]);
                $usuario->setNombre($row["nombre_usuario"]);
                $usuario->setEmail($row["email"]);
                //$usuario->setPassword($row["passw"]);
                $usuario->setImagen($row["imagen"]);
                $usuario->setFechaNac($row["fecha_nacimiento"]);
                $usuario->setFacebook($row["facebook"]);

                $tipo = new TipoUsuario();
                $tipo->setId($row["id_tipo_usuario"]);
                $tipo->setDescripcion($row["descripcion_tipo_usuario"]);

                $usuario->setTipoUsuario($tipo);

                //Para almacenar objetos en la sesión se utiliza el método "serialize()"
                $_SESSION["usuario"] = serialize($usuario);
                $_SESSION["tipoUsuario"] = $tipo->getDescripcion();
                
                return true;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "Se ha producido un error al acceder a BookWorm. Inténtelo de nuevo más tarde.";
        }
    }

    public static function logout() {
        session_unregister("usuario");
        session_unregister("tipoUsuario");
        session_unregister("page");
        session_unregister("active");
        session_destroy();
    }

}

?>
