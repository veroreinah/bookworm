<?php

require_once 'conexion.php';
require_once 'clases/Denuncia.php';
require_once 'clases/Usuario.php';
require_once 'clases/Libro.php';
require_once 'GestionUsuarios.php';
require_once 'GestionLibros.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GestionDenuncias
 *
 * @author Vero
 */
class GestionDenuncias {

    public static function crearDenuncia($denuncia) {
        global $conexion;
        $d = new Denuncia();
        $d = $denuncia;
        $usuario = new Usuario();
        $usuario = $d->getUsuario();
        $libro = new Libro();
        $libro = $d->getLibro();

        try {
            $query = "insert into t_denuncias
                (id_usuario, isbn, fecha, texto, vista)
                    values ('" . $usuario->getId() . "',
                            '" . $libro->getIsbn() . "',
                            '" . $d->getFecha() . "',
                            '" . mysql_escape_string($d->getTexto()) . "',
                            'false')";
            $result = mysql_query($query, $conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido enviar la denuncia. Inténtelo de nuevo más tarde.";
        }
    }

    public static function recuperarDenuncias() {
        global $conexion;
        $denuncias = array();

        try {
            $query = "select * from t_denuncias order by fecha desc";
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $denuncia = new Denuncia();
                $denuncia->setTexto($row["texto"]);
                $denuncia->setFecha($row["fecha"]);
                $denuncia->setVista($row["vista"]);
                $denuncia->setEnlace($row["enlace"]);
                
                $usuario = new Usuario();
                $usuario = GestionUsuarios::recuperarUsuario($row["id_usuario"]);
                $denuncia->setUsuario($usuario);
                
                $libro = new Libro();
                $libro = GestionLibros::recuperarLibro($row["isbn"]);
                $denuncia->setLibro($libro);
                
                $denuncias[] = $denuncia;
            }
            
            if ($denuncias != null && count($denuncias) > 0) {
                
            } else {
                $_SESSION["noHay"] = "Actualmente no existe ninguna denuncia en BookWorm.";
            }
            return $denuncias;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las denuncias. Inténtelo de nuevo más tarde.";
        }
    }

    public static function recuperarDenuncia($enlace) {
        global $conexion;

        try {
            $query = "select * from t_denuncias
                where enlace = '" . $enlace . "'";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            if(!$row) {
                $_SESSION["noHay"] = "No existe ninguna denuncia con ese identificador en BookWorm.";
                return 0;
            } else {
                $denuncia = new Denuncia();
                $denuncia->setTexto($row["texto"]);
                $denuncia->setFecha($row["fecha"]);
                $denuncia->setVista($row["vista"]);
                $denuncia->setEnlace($row["enlace"]);
                
                $us = new Usuario();
                $us = GestionUsuarios::recuperarUsuario($row["id_usuario"]);
                $denuncia->setUsuario($us);
                
                $li = new Libro();
                $li = GestionLibros::recuperarLibro($row["isbn"]);
                $denuncia->setLibro($li);
                
                return $denuncia;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido recuperar la denuncia. Inténtelo de nuevo más tarde.";
        }
    }

    public static function marcarComoVista($enlace) {
        global $conexion;

        try {
            $query = "update t_denuncias
                set vista = true
                where enlace = '" . $enlace . "'";
            $result = mysql_query($query, $conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido marcar la denuncia como vista. Inténtelo de nuevo más tarde.";
        }
    }

    public static function eliminarDenuncia($enlace) {
        global $conexion;

        try {
            $query = "delete from t_denuncias
                where enlace = '" . $enlace . "'";
            $result = mysql_query($query, $conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido eliminar la denuncia. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarDenunciasL($begin, $limit, $field="", $order="", $where="") {
        global $conexion;
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }
        
        if ($field != "" && $order != "") {
            $orderBy = " order by $field $order ";
        }
        
        if ($where != "") {
            $w = " where l.isbn like '%$where%' 
                or l.titulo like '%$where%' 
                or u.nombre_usuario like '%$where%' ";
        }
        
        try {
            $query = "select d.* 
                from t_denuncias as d inner join t_usuarios as u 
                on d.id_usuario = u.id_usuario 
                inner join t_libros as l 
                on d.isbn = l.isbn";
            
            if (isset($w)) {
                $query = $query . $w;
            }
            if (isset($orderBy)) {
                $query = $query . $orderBy;
            }
            if (isset($l)) {
                $query = $query . $l;
            }
            
            $result = mysql_query($query, $conexion);

            $denuncias = array();

            while($row = mysql_fetch_array($result)) {
                $denuncia = new Denuncia();
                $denuncia->setTexto($row["texto"]);
                $denuncia->setFecha($row["fecha"]);
                $denuncia->setVista($row["vista"]);
                $denuncia->setEnlace($row["enlace"]);
                
                $usuario = new Usuario();
                $usuario = GestionUsuarios::recuperarUsuario($row["id_usuario"]);
                $denuncia->setUsuario($usuario);
                
                $libro = new Libro();
                $libro = GestionLibros::recuperarLibro($row["isbn"]);
                $denuncia->setLibro($libro);
                
                $denuncias[] = $denuncia;
            }
            
            return $denuncias;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las denuncias. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalDenuncias() {
        global $conexion;

        try {
            $query = "select count(*) as total
                from t_denuncias";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            $total = $row["total"];

            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las denuncias. Inténtelo de nuevo más tarde.";
        }
    }

    public static function totalFiltered($where) {
        global $conexion;

        if ($where != "") {
            $w = " where l.isbn like '%$where%' 
                or l.titulo like '%$where%' 
                or u.nombre_usuario like '%$where%' ";
        }

        try {
            $query = "select count(d.isbn) as total 
                from t_denuncias as d inner join t_usuarios as u 
                on d.id_usuario = u.id_usuario 
                inner join t_libros as l 
                on d.isbn = l.isbn $w";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            $total = $row["total"];

            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las denuncias. Inténtelo de nuevo más tarde.";
        }
    }

}

?>
