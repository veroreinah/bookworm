<?php

require_once 'conexion.php';
require_once 'clases/Usuario.php';
require_once 'clases/Invitacion.php';
require_once 'GestionUsuarios.php';
require_once 'constant.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gestionRedSocial
 *
 * @author Vero
 */
class GestionRedSocial {

    public static function invitar($usuarioInvita, $usuarioInvitado) {
        global $conexion;
        $uInvita = new Usuario();
        $uInvita = $usuarioInvita;
        $uInvitado = new Usuario();
        $uInvitado = $usuarioInvitado;
        
        $enlace = uniqid();

        try {
            $query = "insert into t_usuario_usuario
                (id_usuario_inv, id_usuario_rec, fecha_inv, aceptada, enlace)
                    values ('" . $uInvita->getId() . "',
                            '" . $uInvitado->getId() . "',
                            '" . date("Y-m-d") . "',
                            null,
                            '" . $enlace . "')";
            $result = mysql_query($query, $conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido enviar la invitación en este momento. Inténtelo de nuevo más tarde.";
        }
    }

    public static function invitarPorEmail($invitacion) {
        global $conexion;
        $i = new Invitacion();
        $i = $invitacion;
        $u = new Usuario();
        $u = $i->getUsuario();
        
        $enlace = uniqid();

        try {
            $query = "insert into t_invitacion
                (fecha_envio, email_inv, texto, aceptada, id_usuario, enlace)
                    values ('" . date("Y-m-d") . "',
                            '" . $i->getEmail() . "',
                            '" . $i->getTexto() . "',
                            null,
                            '" . $u->getId() . "',
                            '" . $enlace . "')";
            $result = mysql_query($query, $conexion);
            
            $i->setEnlace($enlace);
            $enviado = GestionRedSocial::enviarMail($i);
            
            return $enviado;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido enviar la invitación en este momento. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function enviarMail($invitacion) {
        if ($_SERVER['SERVER_NAME'] == 'localhost') {
            $base = "http://localhost:8080/bookworm";
        } else {
            $base = "http://bookworm.phpfogapp.com";
        }
        
        $i = new Invitacion();
        $i = $invitacion;
        $u = new Usuario();
        $u = $i->getUsuario();
        
        require_once '../utilidades/constant.php';
        require_once '../lib/Swift-4.1.7/lib/swift_required.php';
        global $email;
        set_time_limit(-1);

        $body = "<html><body>";
        $body .= $u->getNombre() . " (" . $u->getEmail() . ") te ha intivado a unirte a <a href='$base/index.php'>BookWorm</a>, la nueva red social de lectores.<br /><br />";
        if ($i->getTexto() != "") {
            $body .= "Su mensaje: <em>" . $i->getTexto() . "</em><br /><br />";
        }
        $body .= "Si deseas unirte a <a href='$base/index.php'>BookWorm</a> y estar al día de las lecturas de " . $u->getNombre() . ", haz clic en el siguiente enlace:<br />";
        $body .= "<a href='$base/register.php?inv=" . $i->getEnlace() . "'>Unirse...</a><br /><br /><br />¡Hasta pronto!";
        $body .= "</body></html>";

        // Create a message
        $message = Swift_Message::newInstance()

        // Give the message a subject
        ->setSubject($u->getNombre() . " te invita a BookWorm.")

        // Set the From address with an associative array
        ->setFrom(array($email))

        // Set the To addresses with an associative array
        ->setTo(array($i->getEmail()))

        // Give it a body
        ->setBody($body, 'text/html')
        ;

        $password = getenv('EMAIL_PASSWORD');
        $password = str_replace('\\', '', $password);
        
        // Create the Transport
        $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
        ->setUsername($email)
        ->setPassword($password)
        ;

        // Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($transport);

        // Send the message
        $result = $mailer->send($message);

        if ($result) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function responderInvitacion($enlace, $respuesta) {
        global $conexion;

        try {
            if ($respuesta) {
                $query = "update t_usuario_usuario
                    set aceptada = true
                        where enlace = '" . $enlace . "'";
                $result = mysql_query($query, $conexion);
            } else {
                $query = "delete from t_usuario_usuario
                    where enlace = '" . $enlace . "'";
                $result = mysql_query($query, $conexion);
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido gestionar su respuesta a la invitación. Inténtelo de nuevo más tarde.";
        }
    }

    public static function responderInvitacionEmail($enlace, $usuarioInvita, $usuario) {
        global $conexion;
        $uInvita = new Usuario();
        $uInvita = $usuarioInvita;
        $u = new Usuario();
        $u = $usuario;

        try {
            $query = "update t_invitacion
                set aceptada = true
                where enlace = '" . $enlace . "'";
            $result = mysql_query($query, $conexion);
            
            $query2 = "insert into t_usuario_usuario
                (id_usuario_inv, id_usuario_rec, fecha_inv, aceptada, enlace)
                    values ('" . $uInvita->getId() . "',
                            '" . $u->getId() . "',
                            '" . date("Y-m-d") . "',
                            true,
                            '$enlace')";
            $result2 = mysql_query($query2, $conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido gestionar su respuesta a la invitación. Inténtelo de nuevo más tarde.";
        }
    }

    public static function terminarAmistad($usuarioInvita, $usuarioInvitado) {
        global $conexion;
        $uInvita = new Usuario();
        $uInvita = $usuarioInvita;
        $uInvitado = new Usuario();
        $uInvitado = $usuarioInvitado;

        try {
            $query = "delete from t_usuario_usuario
                where (id_usuario_inv = '" . $uInvita->getId() . "'
                    and id_usuario_rec = '" . $uInvitado->getId() . "')
                    or (id_usuario_rec = '" . $uInvita->getId() . "'
                    and id_usuario_inv = '" . $uInvitado->getId() . "')";
            $result = mysql_query($query, $conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido llevar a cabo su solicitud en este momento. Inténtelo de nuevo más tarde.";
        }
    }

    public static function recuperarInvitaciones($id) {
        global $conexion;
        $invitaciones = array();

        try {
            $query = "select * from t_usuario_usuario
                where id_usuario_rec = '" . $id . "'
                and (aceptada is null or aceptada = false)";
            $result = mysql_query($query, $conexion);
            
            while ($row = mysql_fetch_array($result)) {
                $datos = array();
                $datos[] = GestionUsuarios::recuperarUsuario($row["id_usuario_inv"]);
                $datos[] = $row["fecha_inv"];
                $datos[] = $row["enlace"];
                
                $invitaciones[] = $datos;
            }
            
            return $invitaciones;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las invitaciones pendientes. Inténtelo de nuevo más tarde.";
        }
    }

    public static function recuperarInvitacionesL($begin, $limit, $field="", $order="", $where="") {
        global $conexion;
        $user = new Usuario();
        $user = unserialize($_SESSION["usuario"]);
        $id = $user->getId();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }
        
        if ($field != "" && $order != "") {
            $orderBy = " order by $field $order ";
        }
        
        if ($where != "") {
            $w = "  and u.nombre_usuario like '%$where%' ";
        }
        
        $invitaciones = array();
        try {
            $query = "select * from t_usuario_usuario 
                inner join t_usuarios as u 
                on t_usuario_usuario.id_usuario_inv = u.id_usuario 
                where id_usuario_rec = '" . $id . "'
                and (aceptada is null or aceptada = false)";
            
            if (isset($w)) {
                $query = $query . $w;
            }
            if (isset($orderBy)) {
                $query = $query . $orderBy;
            }
            if (isset($l)) {
                $query = $query . $l;
            }
            
            $result = mysql_query($query, $conexion);
            
            while ($row = mysql_fetch_array($result)) {
                $datos = array();
                $datos[] = GestionUsuarios::recuperarUsuario($row["id_usuario_inv"]);
                $datos[] = $row["fecha_inv"];
                $datos[] = $row["enlace"];
                
                $invitaciones[] = $datos;
            }
            
            return $invitaciones;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las invitaciones pendientes. Inténtelo de nuevo más tarde.";
        }
    }

    public static function recuperarAmigos($id) {
        global $conexion;
        $amigos = array();

        try {
            $query = "select * from t_usuario_usuario
                where (id_usuario_rec = '" . $id . "'
                or id_usuario_inv = '" . $id . "')
                and aceptada = true";
            $result = mysql_query($query, $conexion);
            
            while ($row = mysql_fetch_array($result)) {
                $u = new Usuario();
                if ($row["id_usuario_rec"] == $id) {
                    $u = ($row["id_usuario_inv"]);
                }
                if ($row["id_usuario_inv"] == $id) {
                    $u = ($row["id_usuario_rec"]);
                }
                
                $amigos[] = $u;
            }
            
            if (count($amigos) == 0) {
                $_SESSION["noHayA"] = "Todavía no sigues a ningún usuario de BookWorm. Puedes invitar a tus amigos desde el panel de la derecha o seguir navegando y seguir a los usuarios con los que tengas más afinidad.";
            }
            
            return $amigos;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar sus contactos. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarUsuarioInvita($enlace) {
        global $conexion;
        try {
            $query = "select * from t_invitacion
                where enlace = '$enlace'";
            $result = mysql_query($query, $conexion);
            
            while ($row = mysql_fetch_array($result)) {
                return $row['id_usuario'];
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido recuperar el usuario. Inténtelo de nuevo más tarde.";
        }
    }

    public static function recuperarAmigosL($begin, $limit, $field="", $order="", $where="") {
        global $conexion;
        $u = new Usuario();
        $u = unserialize($_SESSION["usuario"]);
        $id = $u->getId();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }
        
        if ($field != "" && $order != "") {
            $orderBy = " order by $field $order ";
        }
        
        if ($where != "") {
            $w = "  and u.nombre_usuario like '%$where%' ";
        }
        
        $amigos = array();
        try {
            $query = "select distinct * from t_usuario_usuario as uu
                inner join t_usuarios as u 
                on (u.id_usuario = uu.id_usuario_rec or u.id_usuario = uu.id_usuario_inv)
                where (uu.id_usuario_rec = '" . $id . "'
                or uu.id_usuario_inv = '" . $id . "')
                and uu.aceptada = true 
                and u.id_usuario != $id ";
            
            if (isset($w)) {
                $query = $query . $w;
            }
            if (isset($orderBy)) {
                $query = $query . $orderBy;
            }
            if (isset($l)) {
                $query = $query . $l;
            }
            
            $result = mysql_query($query, $conexion);
            
            while ($row = mysql_fetch_array($result)) {
                $u = new Usuario();
                if ($row["id_usuario_rec"] == $id) {
                    $u = GestionUsuarios::recuperarUsuario($row["id_usuario_inv"]);
                }
                if ($row["id_usuario_inv"] == $id) {
                    $u = GestionUsuarios::recuperarUsuario($row["id_usuario_rec"]);
                }
                
                $amigos[] = $u;
            }
            
            if (count($amigos) > 0) {
                
            } else {
                $_SESSION["noHay"] = "Todavía no sigues a ningún usuario de BookWorm. Puedes invitar a tus amigos desde el panel de la derecha o seguir navegando y seguir a los usuarios con los que tengas más afinidad.";
            }
            return $amigos;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar sus contactos. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalAmigos() {
        global $conexion;
        $u = new Usuario();
        $u = unserialize($_SESSION["usuario"]);
        $id = $u->getId();

        try {
            $query = "select count(*) as total
                from t_usuario_usuario as uu  
                where (uu.id_usuario_rec = '" . $id . "'
                    or uu.id_usuario_inv = '" . $id . "')
                    and uu.aceptada = true ";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            $total = $row["total"];

            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar sus contactos. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalFiltered($where) {
        global $conexion;
        $u = new Usuario();
        $u = unserialize($_SESSION["usuario"]);
        $id = $u->getId();

        if ($where != "") {
            $w = "  and u.nombre_usuario like '%$where%' ";
        }

        try {
            $query = "select count(*) as total 
                from t_usuario_usuario as uu inner join t_usuarios as u 
                on (u.id_usuario = uu.id_usuario_rec or u.id_usuario = uu.id_usuario_inv) 
                where (uu.id_usuario_rec = '" . $id . "' 
                or uu.id_usuario_inv = '" . $id . "') 
                and uu.aceptada = true 
                and u.id_usuario != $id $w";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            $total = $row["total"];

            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los libros. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalInvitaciones() {
        global $conexion;
        $u = new Usuario();
        $u = unserialize($_SESSION["usuario"]);
        $id = $u->getId();

        try {
            $query = "select count(*) as total from t_usuario_usuario 
                where id_usuario_rec = '" . $id . "'
                and (aceptada is null or aceptada = false)";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            $total = $row["total"];

            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar sus contactos. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalFilteredInv($where) {
        global $conexion;
        $u = new Usuario();
        $u = unserialize($_SESSION["usuario"]);
        $id = $u->getId();

        if ($where != "") {
            $w = "  and u.nombre_usuario like '%$where%' ";
        }

        try {
            $query = "select count(*) as total from t_usuario_usuario 
                inner join t_usuarios as u 
                on t_usuario_usuario.id_usuario_inv = u.id_usuario 
                where id_usuario_rec = '" . $id . "'
                and (aceptada is null or aceptada = false) $w";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            $total = $row["total"];

            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los libros. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function invitado($idUsuario) {
        global $conexion;
        
        $u = new Usuario();
        $u = unserialize($_SESSION["usuario"]);
        
        try {
            $query = "select * from t_usuario_usuario
                where ((id_usuario_rec = '" . $idUsuario . "'
                    and id_usuario_inv = '" . $u->getId() . "'))
                and aceptada is null";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            if (!$row) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar sus contactos. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function invitado2($idUsuario) {
        global $conexion;
        
        $u = new Usuario();
        $u = unserialize($_SESSION["usuario"]);
        
        try {
            $query = "select * from t_usuario_usuario
                where ((id_usuario_inv = '" . $idUsuario . "'
                    and id_usuario_rec = '" . $u->getId() . "'))
                and aceptada is null";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            if (!$row) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar sus contactos. Inténtelo de nuevo más tarde.";
        }
    }

}

?>
