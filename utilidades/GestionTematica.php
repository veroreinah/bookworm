<?php
require_once 'conexion.php';
require_once 'clases/Tematica.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GestionCitas
 *
 * @author Vero
 */
class GestionTematica {
    
    public static function crearTematica($tematica) {
        global $conexion;
        $t = new Tematica();
        $t = $tematica;

        try {
            $query = "insert into t_tematica
                (descripcion_tematica)
                    values ('" . $t->getDescripcion() . "')";
            $result = mysql_query($query, $conexion);
            $_SESSION["insertada"] = "La temática se ha insertado con éxito.";
            
            return mysql_insert_id($conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido crear la nueva temática. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function modificarTematica($tematica) {
        global $conexion;
        $t = new Tematica();
        $t = $tematica;

        try {
            $query = "update t_tematica
                set descripcion_tematica = '" . $t->getDescripcion() . "'
                    where id_tematica = '" . $t->getId() . "'";
            $result = mysql_query($query, $conexion);
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido modificar la temática. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function eliminarTematica($id) {
        global $conexion;

        try {
            $query = "delete from t_tematica
                where id_tematica = '" . $id . "'";
            $result = mysql_query($query, $conexion);
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido eliminar la temática. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalTematicas() {
        global $conexion;

        try {
            $query = "select count(*) as total
                from t_tematica";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            $total = $row["total"];
            
            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las temáticas. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalFiltered($where) {
        global $conexion;
        
        if ($where != "") {
            $w = " where descripcion_tematica like '%$where%' ";
        }

        try {
            $query = "select count(*) as total 
                from t_tematica 
                $w";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            $total = $row["total"];
            
            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las temáticas. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarTematicasL($begin, $limit, $field, $order, $where) {
        global $conexion;
        $tematicas = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }
        
        if ($field != "" && $order != "") {
            $orderBy = " order by $field $order ";
        }
        
        if ($where != "") {
            $w = " where descripcion_tematica like '%$where%' ";
        }

        try {
            $query = "select *
                from t_tematica";
            if (isset($w)) {
                $query = $query . $w;
            }
            if (isset($orderBy)) {
                $query = $query . $orderBy;
            }
            if (isset($l)) {
                $query = $query . $l;
            }
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $tematica = new Tematica();
                $tematica->setId($row["id_tematica"]);
                $tematica->setDescripcion($row["descripcion_tematica"]);
                
                $tematicas[] = $tematica;
            }
            
            return $tematicas;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las temáticas. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarTematicas() {
        global $conexion;
        $tematicas = array();

        try {
            $query = "select *
                from t_tematica
                order by descripcion_tematica";
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $tematica = new Tematica();
                $tematica->setId($row["id_tematica"]);
                $tematica->setDescripcion($row["descripcion_tematica"]);
                
                $tematicas[] = $tematica;
            }
            
            if ($tematicas != null && count($tematicas) > 0) {
                
            } else {
                $_SESSION["noHay"] = "No existe ninguna temática en BookWorm.";
            }
            return $tematicas;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar las temáticas. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarTematica($id) {
        global $conexion;

        try {
            $query = "select *
                from t_tematica
                where id_tematica = $id";
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $tematica = new Tematica();
                $tematica->setId($row["id_tematica"]);
                $tematica->setDescripcion($row["descripcion_tematica"]);
                
                return $tematica;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recupera la temática. Inténtelo de nuevo más tarde.";
        }
    }
}

?>
