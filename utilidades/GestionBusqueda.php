<?php

require_once 'conexion.php';
require_once 'GestionLibros.php';
require_once 'clases/Usuario.php';
require_once 'clases/TipoUsuario.php';
require_once 'clases/Libro.php';
require_once 'clases/Cita.php';
require_once 'clases/Autor.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gestionBusqueda
 *
 * @author Vero
 */
class GestionBusqueda {

    public static function buscarUsuario($begin, $limit, $nombre) {
        global $conexion;
        $usuarios = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }

        try {
            $query = "SELECT id_usuario, nombre_usuario, imagen, fecha_nacimiento, facebook, t_usuarios.id_tipo_usuario, descripcion_tipo_usuario 
                FROM t_usuarios inner join t_tipo_usuario 
                on t_usuarios.id_tipo_usuario = t_tipo_usuario.id_tipo_usuario 
                where nombre_usuario like '%" . trim($nombre) . "%' $l";
            $result = mysql_query($query, $conexion);
            
            while ($row = mysql_fetch_array($result)) {
                $usuario = new Usuario();
                $usuario->setId($row["id_usuario"]);
                $usuario->setNombre($row["nombre_usuario"]);
                
                $usuarios[] = $usuario;
            }
            
            $query2 = "SELECT count(id_usuario) as total 
                FROM t_usuarios inner join t_tipo_usuario 
                on t_usuarios.id_tipo_usuario = t_tipo_usuario.id_tipo_usuario 
                where nombre_usuario like '%" . trim($nombre) . "%'";
            $result2 = mysql_query($query2, $conexion);
            
            while ($row2 = mysql_fetch_array($result2)) {
                $usuarios["total"] = $row2["total"];
            }
            
            return $usuarios;
        } catch (Exception $e) {
            $_SESSION["error"] = "Se ha producido un error al realizar la búsqueda. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function buscarUsuarioPorEmail($begin, $limit, $email) {
        global $conexion;
        $usuarios = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }

        try {
            $query = "SELECT id_usuario, nombre_usuario, email, imagen, fecha_nacimiento, facebook, t_usuarios.id_tipo_usuario, descripcion_tipo_usuario 
                FROM t_usuarios inner join t_tipo_usuario 
                on t_usuarios.id_tipo_usuario = t_tipo_usuario.id_tipo_usuario 
                where email like '" . trim($email) . "' $l";
            $result = mysql_query($query, $conexion);
            
            while ($row = mysql_fetch_array($result)) {
                $usuario = new Usuario();
                $usuario->setId($row["id_usuario"]);
                $usuario->setNombre($row["nombre_usuario"]);
                
                $usuarios[] = $usuario;
            }
            
            $query2 = "SELECT count(id_usuario) as total 
                FROM t_usuarios inner join t_tipo_usuario 
                on t_usuarios.id_tipo_usuario = t_tipo_usuario.id_tipo_usuario 
                where email like '" . trim($email) . "'";
            $result2 = mysql_query($query2, $conexion);
            
            while ($row2 = mysql_fetch_array($result2)) {
                $usuarios["total"] = $row2["total"];
            }
            
            return $usuarios;
        } catch (Exception $e) {
            $_SESSION["error"] = "Se ha producido un error al realizar la búsqueda. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function buscarLibro($begin, $limit, $titulo) {
        global $conexion;
        $libros = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }

        try {
            $query = "SELECT isbn 
                FROM t_libros 
                where titulo like '%" . trim($titulo) . "%' $l";
            $result = mysql_query($query, $conexion);
            
            while ($row = mysql_fetch_array($result)) {
                $libro = new Libro();
                $libro = GestionLibros::recuperarLibro($row["isbn"]);
                
                $libros[] = $libro;
            }
            
            $query2 = "SELECT count(isbn) as total 
                FROM t_libros 
                where titulo like '%" . trim($titulo) . "%'";
            $result2 = mysql_query($query2, $conexion);
            
            while ($row2 = mysql_fetch_array($result2)) {
                $libros["total"] = $row2["total"];
            }
            
            return $libros;
        } catch (Exception $e) {
            $_SESSION["error"] = "Se ha producido un error al realizar la búsqueda. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function buscarLibroPorIsbn($begin, $limit, $isbn) {
        global $conexion;
        $libros = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }

        try {
            $query = "SELECT isbn 
                FROM t_libros 
                where isbn = '" . $isbn . "' $l";
            $result = mysql_query($query, $conexion);
            
            while ($row = mysql_fetch_array($result)) {
                $libro = new Libro();
                $libro = GestionLibros::recuperarLibro($row["isbn"]);
                
                $libros[] = $libro;
            }
            
            $query2 = "SELECT count(isbn) as total 
                FROM t_libros 
                where isbn = '" . $isbn . "'";
            $result2 = mysql_query($query2, $conexion);
            
            while ($row2 = mysql_fetch_array($result2)) {
                $libros["total"] = $row2["total"];
            }
            
            return $libros;
        } catch (Exception $e) {
            $_SESSION["error"] = "Se ha producido un error al realizar la búsqueda. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function buscarLibrosPorAutor($begin, $limit, $autor) {
        global $conexion;
        $libros = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }

        try {
            $query = "SELECT t_libros.isbn 
                FROM t_libros inner join t_libros_autores
                on t_libros.isbn = t_libros_autores.isbn 
                where id_autor = $autor $l";
            $result = mysql_query($query, $conexion);
            
            while ($row = mysql_fetch_array($result)) {
                $libro = new Libro();
                $libro = GestionLibros::recuperarLibro($row["isbn"]);
                
                $libros[] = $libro;
            }
            
            $query2 = "SELECT count(t_libros.isbn) as total
                FROM t_libros inner join t_libros_autores
                on t_libros.isbn = t_libros_autores.isbn 
                where id_autor = $autor";
            $result2 = mysql_query($query2, $conexion);
            
            while ($row2 = mysql_fetch_array($result2)) {
                $libros["total"] = $row2["total"];
            }
            
            return $libros;
        } catch (Exception $e) {
            $_SESSION["error"] = "Se ha producido un error al realizar la búsqueda. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function buscarLibrosPorTematica($begin, $limit, $tematica) {
        global $conexion;
        $libros = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }

        try {
            $query = "SELECT t_libros.isbn 
                FROM t_libros inner join t_libros_tematica
                on t_libros.isbn = t_libros_tematica.isbn 
                where id_tematica = $tematica $l";
            $result = mysql_query($query, $conexion);
            
            while ($row = mysql_fetch_array($result)) {
                $libro = new Libro();
                $libro = GestionLibros::recuperarLibro($row["isbn"]);
                
                $libros[] = $libro;
            }
            
            $query2 = "SELECT count(t_libros.isbn) as total
                FROM t_libros inner join t_libros_tematica
                on t_libros.isbn = t_libros_tematica.isbn 
                where id_tematica = $tematica";
            $result2 = mysql_query($query2, $conexion);
            
            while ($row2 = mysql_fetch_array($result2)) {
                $libros["total"] = $row2["total"];
            }
            
            return $libros;
        } catch (Exception $e) {
            $_SESSION["error"] = "Se ha producido un error al realizar la búsqueda. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function buscarLibrosAvanzada($begin, $limit, $titulo, $subtitulo, $anyo, $editorial, $tematicas, $autores) {
        global $conexion;
        $libros = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }

        try {
            //Primero "construimos" el where, comprobando los parámetros que nos han pasado.
            $where = "";
            if (isset($titulo) && $titulo != "") {
                $where .= "titulo like '%" . trim($titulo) . "%'";
            }
            if (isset($subtitulo) && $subtitulo != "") {
                if ($where == "") {
                    $where .= "subtitulo like '%" . trim($subtitulo) . "%'";
                } else {
                    $where .= " or subtitulo like '%" . trim($subtitulo) . "%'";
                }
            }
            if (isset($anyo) && $anyo != "") {
                if ($where == "") {
                    $where .= "anyo_publicacion = '" . $anyo . "'";
                } else {
                    $where .= " or anyo_publicacion = '" . $anyo . "'";
                }
            }
            if (isset($editorial) && intval($editorial) != 0) {
                if ($where == "") {
                    $where .= "id_editorial = $editorial";
                } else {
                    $where .= " or id_editorial = $editorial";
                }
            }
            if (isset($tematicas) && intval($tematicas) != 0) {
                $whereTematica = " where id_tematica = $tematicas ";
            }
            if (isset($autores) && intval($autores) != 0) {
                $whereAutor = " where id_autor = $autores ";
            }
            
            //Comenzamos a construir la consulta.
            if (isset($tematicas) && intval($tematicas) != 0) {
                $query1 = "(select distinct t_libros.isbn 
                    from t_libros inner join t_libros_tematica 
                    on t_libros.isbn = t_libros_tematica.isbn $whereTematica)";
            }
            if (isset($autores) && intval($autores) != 0) {
                $query2 = "(select distinct t_libros.isbn 
                    from t_libros inner join t_libros_autores 
                    on t_libros.isbn = t_libros_autores.isbn $whereAutor)";
            }
            if ($where != "") {
                $query3 = "(select distinct t_libros.isbn
                    from t_libros where $where)";
            }
            
            $query = "";
            if (isset($query1)) {
                $query .= $query1;
            }
            if (isset($query2)) {
                if ($query == "") {
                    $query .= $query2;
                } else {
                    $query .= " union " . $query2;
                }
            }
            if (isset($query3)) {
                if ($query == "") {
                    $query .= $query3;
                } else {
                    $query .= " union " . $query3;
                }
            }
            
            $query .= $l;
            $result = mysql_query($query, $conexion);
            
            $libros["total"] = 0;
            while ($row = mysql_fetch_array($result)) {
                $libro = new Libro();
                $libro = GestionLibros::recuperarLibro($row["isbn"]);
                
                $libros["total"] = $libros["total"] + 1;
                $libros[] = $libro;
            }
            
            return $libros;
            
        } catch (Exception $e) {
            $_SESSION["error"] = "Se ha producido un error al realizar la búsqueda. Inténtelo de nuevo más tarde.";
        }
    }

}

?>
