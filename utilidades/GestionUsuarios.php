<?php

require_once 'conexion.php';
require_once 'clases/Usuario.php';
require_once 'clases/TipoUsuario.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gestionUsuarios
 *
 * @author Vero
 */
class GestionUsuarios {
    
    public static function insertarUsuario($usuario) {
        global $conexion;
        $u = new Usuario();
        $u = $usuario;
        $tipo = new TipoUsuario();
        $tipo = $u->getTipoUsuario();

        try {
            $query = "insert into t_usuarios
                (nombre_usuario, email, passw, imagen, fecha_nacimiento, facebook, id_tipo_usuario)
                    values ('" . $u->getNombre() . "',
                            '" . $u->getEmail() . "',
                            '" . md5($u->getPassword()) . "',
                            '" . $u->getImagen() . "',
                            '" . $u->getFechaNac() . "',
                            '" . $u->getFacebook() . "',
                            '" . $tipo->getId() . "')";
            $result = mysql_query($query, $conexion);
            $_SESSION["insertada"] = "El usuario se ha insertado con éxito.";
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido insertar el usuario. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function modificarUuario($usuario) {
        global $conexion;
        $u = new Usuario();
        $u = $usuario;
        
        $pass = "";
        if ($u->getPassword() == "") {
            
        } else {
            $pass = " passw = '" . md5($u->getPassword()) . "',";
        }
        $imagen = "";
        if ($u->getImagen() == "") {
            
        } else {
            $imagen = " imagen = '" . $u->getImagen() . "',";
        }
        
        try {
            $query = "update t_usuarios
                set nombre_usuario = '" . $u->getNombre() . "',
                    email = '" . $u->getEmail() . "',
                    $pass
                    $imagen
                    fecha_nacimiento = '" . $u->getFechaNac() . "',
                    facebook = '" . $u->getFacebook() . "'
                where id_usuario = '" . $u->getId() . "'";
            $result = mysql_query($query, $conexion);
            
            $affected_rows = mysql_affected_rows();
            
            //Cambiamos los datos que había almacenados en la sesión si es el usuario registrado
            if ($affected_rows > 0 && isset($_SESSION["usuario"])) {
                $usuario = new Usuario();
                $usuario = unserialize($_SESSION["usuario"]);
                if ($usuario->getId() == $u->getId()) {
                    session_unregister("usuario");
                    
                    $user = new Usuario();
                    $user = GestionUsuarios::recuperarUsuario($u->getId());
                    $_SESSION["usuario"] = serialize($user);
                }
            }
            
            return $affected_rows;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido modificar el usuario. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function eliminarUsuario($id) {
        global $conexion;

        try {
            $query1 = "update t_libros set id_usuario = 11 where id_usuario = $id";
            $query2 = "delete from t_comentarios where id_usuario = $id";
            $query3 = "delete from t_denuncias where id_usuario = $id";
            $query4 = "delete from t_invitacion where id_usuario = $id";
            $query5 = "delete from t_usuario_libros where id_usuario = $id";
            $query6 = "delete from t_usuario_usuario where id_usuario_inv = $id or id_usuario_rec = $id";
            $query7 = "delete from t_usuarios where id_usuario = '" . $id . "'";
            
            $result = mysql_query($query1, $conexion);
            $result = mysql_query($query2, $conexion);
            $result = mysql_query($query3, $conexion);
            $result = mysql_query($query4, $conexion);
            $result = mysql_query($query5, $conexion);
            $result = mysql_query($query6, $conexion);
            $result = mysql_query($query7, $conexion);
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido eliminar el usuario. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarUsuario($id) {
        global $conexion;

        try {
            $query = "SELECT id_usuario, nombre_usuario, email, passw, imagen, fecha_nacimiento, facebook, t_usuarios.id_tipo_usuario, descripcion_tipo_usuario 
                FROM t_usuarios inner join t_tipo_usuario 
                on t_usuarios.id_tipo_usuario = t_tipo_usuario.id_tipo_usuario 
                where id_usuario = $id";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            if (!$row) {
                $_SESSION["noHay"] = "No existe ningún usuario con ese identificador en BookWorm.";
                return 0;
            } else {
                $usuario = new Usuario();
                $usuario->setId($row["id_usuario"]);
                $usuario->setNombre($row["nombre_usuario"]);
                $usuario->setEmail($row["email"]);
                //$usuario->setPassword($row["passw"]);
                $usuario->setImagen($row["imagen"]);
                $usuario->setFechaNac($row["fecha_nacimiento"]);
                $usuario->setFacebook($row["facebook"]);

                $tipo = new TipoUsuario();
                $tipo->setId($row["id_tipo_usuario"]);
                $tipo->setDescripcion($row["descripcion_tipo_usuario"]);

                $usuario->setTipoUsuario($tipo);
                
                return $usuario;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los datos del usuario. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarUsuarioPorEmail($email) {
        global $conexion;

        try {
            $query = "SELECT id_usuario, nombre_usuario, email, passw, imagen, fecha_nacimiento, facebook, t_usuarios.id_tipo_usuario, descripcion_tipo_usuario 
                FROM t_usuarios inner join t_tipo_usuario 
                on t_usuarios.id_tipo_usuario = t_tipo_usuario.id_tipo_usuario 
                where email = '$email'";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            if (!$row) {
                $_SESSION["noHay"] = "No existe ningún usuario con ese email en BookWorm.";
                return 0;
            } else {
                $usuario = new Usuario();
                $usuario->setId($row["id_usuario"]);
                $usuario->setNombre($row["nombre_usuario"]);
                $usuario->setEmail($row["email"]);
                //$usuario->setPassword($row["passw"]);
                $usuario->setImagen($row["imagen"]);
                $usuario->setFechaNac($row["fecha_nacimiento"]);
                $usuario->setFacebook($row["facebook"]);

                $tipo = new TipoUsuario();
                $tipo->setId($row["id_tipo_usuario"]);
                $tipo->setDescripcion($row["descripcion_tipo_usuario"]);

                $usuario->setTipoUsuario($tipo);
                
                return $usuario;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los datos del usuario. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarUsuariosL($begin, $limit, $field, $order, $where, $id = 0) {
        global $conexion;
        $usuarios = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }
        
        if ($field != "" && $order != "") {
            $orderBy = " order by $field $order ";
        }
        
        if ($where != "") {
            $w = " where nombre_usuario like '%$where%' or email like '%$where%' ";
        }
        
        if ($id != 0) {
            if (isset($w)) {
                $i = " and id_usuario != $id ";
            } else {
                $i = " where id_usuario != $id ";
            }
        }

        try {
            $query = "SELECT id_usuario, nombre_usuario, email
                FROM t_usuarios";
            if (isset($w)) {
                $query = $query . $w;
            }
            if (isset($i)) {
                $query = $query . $i;
            }
            if (isset($orderBy)) {
                $query = $query . $orderBy;
            }
            if (isset($l)) {
                $query = $query . $l;
            }
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $usuario = new Usuario();
                $usuario->setId($row["id_usuario"]);
                $usuario->setNombre($row["nombre_usuario"]);
                $usuario->setEmail($row["email"]);
                
                $usuarios[] = $usuario;
            }
            
            if (count($usuarios) > 0) {
                
            } else {
                $_SESSION["noHay"] = "No existe ningún usuario en Bookworm.";
            }
            return $usuarios;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los usuarios. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarUsuarios() {
        global $conexion;
        $usuarios = array();

        try {
            $query = "SELECT id_usuario, nombre_usuario, email
                FROM t_usuarios";
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $usuario = new Usuario();
                $usuario->setId($row["id_usuario"]);
                $usuario->setNombre($row["nombre_usuario"]);
                $usuario->setEmail($row["email"]);
                
                $usuarios[] = $usuario;
            }
            
            if (count($usuarios) > 0) {
                return $usuarios;
            } else {
                $_SESSION["noHay"] = "No existe ningún usuario en Bookworm.";
                return 0;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los usuarios. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalUsuarios($id = 0) {
        global $conexion;

        if ($id != 0) {
            $i = " where id_usuario != $id ";
        }
        
        try {
            $query = "select count(*) as total
                from t_usuarios $i";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            $total = $row["total"];
            
            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los usuarios. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalFiltered($where, $id = 0) {
        global $conexion;
        
        if ($where != "") {
            $w = " where nombre_usuario like '%$where%' or email like '%$where%' ";
        }
        if ($id != 0) {
            if (isset($w)) {
                $i = " and id_usuario != $id ";
            } else {
                $i = " where id_usuario != $id ";
            }
        }

        try {
            $query = "select count(*) as total 
                from t_usuarios 
                $w $i";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            $total = $row["total"];
            
            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los usuarios. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function existeUsuario($email) {
        global $conexion;
        try {
            $query = "SELECT * 
                FROM t_usuarios  
                where email like '" . $email . "'";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);

            if (!$row) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido recuperar el usuario. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function restablecerPassword($email) {
        global $conexion;
        $password = uniqid();
        try {
            $query = "UPDATE t_usuarios 
                set passw = '" . md5($password) . "'
                where email = '" . $email . "'";
            $result = mysql_query($query, $conexion);
            return $password;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido recuperar el usuario. Inténtelo de nuevo más tarde.";
        }
    }
}
?>
