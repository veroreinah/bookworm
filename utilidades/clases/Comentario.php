<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Comentario
 *
 * @author Vero
 */
class Comentario {
    
    private $m_fecha;
    private $m_titulo;
    private $m_comentario;
    private $m_spoiler;
    private $m_usuario;
    private $m_libro;
    
    public function setFecha($fecha) {
        $this->m_fecha = $fecha;
    }
    
    public function getFecha() {
        return $this->m_fecha;
    }

    public function setTitulo($titulo) {
        $this->m_titulo = $titulo;
    }

    public function getTitulo() {
        return $this->m_titulo;
    }
    
    public function setComentario($comentario) {
        $this->m_comentario = $comentario;
    }
    
    public function getComentario() {
        return $this->m_comentario;
    }
    
    public function setSpoiler($spoiler) {
        $this->m_spoiler = $spoiler;
    }
    
    public function getSpoiler() {
        return $this->m_spoiler;
    }
    
    public function setUsuario($usuario) {
        $this->m_usuario = $usuario;
    }

    public function getUsuario() {
        return $this->m_usuario;
    }
    
    public function setLibro($libro) {
        $this->m_libro = $libro;
    }
    
    public function getLibro() {
        return $this->m_libro;
    }
}

?>
