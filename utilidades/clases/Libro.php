<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Libro
 *
 * @author Vero
 */
class Libro {

    private $m_isbn;
    private $m_titulo;
    private $m_subtitulo;
    private $m_paginas;
    private $m_portada;
    private $m_sinopsis;
    private $m_anyo_publicacion;
    private $m_edicion;
    private $m_valoracion_media;
    private $m_valoracion_usuario;
    private $m_editorial;
    private $m_tipo_edicion;
    private $m_tematicas;
    private $m_autores;
    private $m_usuario;
    private $m_fecha;

    public function setIsbn($isbn) {
        $this->m_isbn = $isbn;
    }

    public function getIsbn() {
        return $this->m_isbn;
    }

    public function setTitulo($titulo) {
        $this->m_titulo = $titulo;
    }

    public function getTitulo() {
        return $this->m_titulo;
    }

    public function setSubtitulo($subtitulo) {
        $this->m_subtitulo = $subtitulo;
    }

    public function getSubtitulo() {
        return $this->m_subtitulo;
    }

    public function setPaginas($paginas) {
        $this->m_paginas = $paginas;
    }

    public function getPaginas() {
        return $this->m_paginas;
    }

    public function setPortada($portada) {
        $this->m_portada = $portada;
    }

    public function getPortada() {
        return $this->m_portada;
    }

    public function setSinopsis($sinopsis) {
        $this->m_sinopsis = $sinopsis;
    }

    public function getSinopsis() {
        return $this->m_sinopsis;
    }

    public function setAnyoPublicacion($anyo_publicacion) {
        $this->m_anyo_publicacion = $anyo_publicacion;
    }

    public function getAnyoPublicacion() {
        return $this->m_anyo_publicacion;
    }

    public function setEdicion($edicion) {
        $this->m_edicion = $edicion;
    }

    public function getEdicion() {
        return $this->m_edicion;
    }

    public function setValoracionMedia($valoracion) {
        $this->m_valoracion_media = $valoracion;
    }

    public function getValoracionMedia() {
        return $this->m_valoracion_media;
    }

    public function setValoracionUsuario($valoracion) {
        $this->m_valoracion_usuario = $valoracion;
    }

    public function getValoracionUsuario() {
        return $this->m_valoracion_usuario;
    }

    public function setEditorial($editorial) {
        $this->m_editorial = $editorial;
    }

    public function getEditorial() {
        return $this->m_editorial;
    }

    public function setTipoEdicion($tipo_edicion) {
        $this->m_tipo_edicion = $tipo_edicion;
    }

    public function getTipoEdicion() {
        return $this->m_tipo_edicion;
    }

    public function setTematicas($tematicas) {
        $this->m_tematicas = $tematicas;
    }

    public function getTematicas() {
        return $this->m_tematicas;
    }

    public function setAutores($autores) {
        $this->m_autores = $autores;
    }

    public function getAutores() {
        return $this->m_autores;
    }

    public function setUsuario($usuario) {
        $this->m_usuario = $usuario;
    }

    public function getUsuario() {
        return $this->m_usuario;
    }

    public function setFecha($fecha) {
        $this->m_fecha = $fecha;
    }

    public function getFecha() {
        return $this->m_fecha;
    }

}

?>
