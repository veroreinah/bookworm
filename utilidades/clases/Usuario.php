<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of usuario
 *
 * @author Vero
 */
class Usuario {
    
    private $m_id;
    private $m_nombre_usuario;
    private $m_email;
    private $m_password;
    private $m_imagen;
    private $m_fecha_nacimiento;
    private $m_facebook;
    private $m_tipo_usuario;
    
    public function setId($id) {
        $this->m_id = $id;
    }
    
    public function getId() {
        return $this->m_id;
    }
    
    public function setNombre($nombre) {
        $this->m_nombre_usuario = $nombre;
    }
    
    public function getNombre() {
        return $this->m_nombre_usuario;
    }
    
    public function setEmail($email) {
        $this->m_email = $email;
    }
    
    public function getEmail() {
        return $this->m_email;
    }
    
    public function setPassword($password) {
        $this->m_password = $password;
    }
    
    public function getPassword() {
        return $this->m_password;
    }
    
    public function setImagen($imagen) {
        $this->m_imagen = $imagen;
    }
    
    public function getImagen() {
        return $this->m_imagen;
    }
    
    public function setFechaNac($fechaNac) {
        $this->m_fecha_nacimiento = $fechaNac;
    }
    
    public function getFechaNac() {
        return $this->m_fecha_nacimiento;
    }
    
    public function setFacebook($facebook) {
        $this->m_facebook = $facebook;
    }
    
    public function getFacebook() {
        return $this->m_facebook;
    }
    
    public function setTipoUsuario($tipo_usuario) {
        $this->m_tipo_usuario = $tipo_usuario;
    }
    
    public function getTipoUsuario() {
        return $this->m_tipo_usuario;
    }
}

?>
