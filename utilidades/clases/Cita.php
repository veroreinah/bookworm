<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cita
 *
 * @author Vero
 */
class Cita {
    
    private $m_id;
    private $m_texto;
    private $m_autor;

    public function setId($id) {
        $this->m_id = $id;
    }

    public function getId() {
        return $this->m_id;
    }
    
    public function setTexto($texto) {
        $this->m_texto = $texto;
    }
    
    public function getTexto() {
        return $this->m_texto;
    }
    
    public function setAutor($autor) {
        $this->m_autor = $autor;
    }

    public function getAutor() {
        return $this->m_autor;
    }
    
    
}

?>
