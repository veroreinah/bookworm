<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Invitacion
 *
 * @author Vero
 */
class Invitacion {
    
    private $m_id;
    private $m_fecha;
    private $m_email;
    private $m_texto;
    private $m_aceptada;
    private $m_usuario;
    private $m_enlace;
    
    public function setId($id) {
        $this->m_id = $id;
    }
    
    public function getId() {
        return $this->m_id;
    }
    
    public function setFecha($fecha) {
        $this->m_fecha = $fecha;
    }
    
    public function getFecha() {
        return $this->m_fecha;
    }
    
    public function setEmail($email) {
        $this->m_email = $email;
    }
    
    public function getEmail() {
        return $this->m_email;
    }
    
    public function setTexto($texto) {
        $this->m_texto = $texto;
    }
    
    public function getTexto() {
        return $this->m_texto;
    }
    
    public function setAceptada($aceptada) {
        $this->m_aceptada = $aceptada;
    }
    
    public function getAceptada() {
        return $this->m_aceptada;
    }
    
    public function setUsuario($usuario) {
        $this->m_usuario = $usuario;
    }
    
    public function getUsuario() {
        return $this->m_usuario;
    }
    
    public function setEnlace($enlace) {
        $this->m_enlace = $enlace;
    }
    
    public function getEnlace() {
        return $this->m_enlace;
    }
}

?>
