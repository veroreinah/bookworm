<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TipoUsuario
 *
 * @author Vero
 */
class TipoUsuario {

    private $m_id;
    private $m_descripcion;

    public function setId($id) {
        $this->m_id = $id;
    }

    public function getId() {
        return $this->m_id;
    }

    public function setDescripcion($descripcion) {
        $this->m_descripcion = $descripcion;
    }

    public function getDescripcion() {
        return $this->m_descripcion;
    }

}

?>
