<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Denuncia
 *
 * @author Vero
 */
class Denuncia {
    
    private $m_fecha;
    private $m_texto;
    private $m_vista;
    private $m_enlace;
    private $m_usuario;
    private $m_libro;
    
    public function setFecha($fecha) {
        $this->m_fecha = $fecha;
    }
    
    public function getFecha() {
        return $this->m_fecha;
    }
    
    public function setTexto($texto) {
        $this->m_texto = $texto;
    }
    
    public function getTexto() {
        return $this->m_texto;
    }
    
    public function setVista($vista) {
        $this->m_vista = $vista;
    }
    
    public function getVista() {
        return $this->m_vista;
    }
    
    public function setEnlace($enlace) {
        $this->m_enlace = $enlace;
    }
    
    public function getEnlace() {
        return $this->m_enlace;
    }
    
    public function setUsuario($usuario) {
        $this->m_usuario = $usuario;
    }

    public function getUsuario() {
        return $this->m_usuario;
    }
    
    public function setLibro($libro) {
        $this->m_libro = $libro;
    }
    
    public function getLibro() {
        return $this->m_libro;
    }

}

?>
