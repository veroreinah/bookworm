<?php
require_once 'conexion.php';
require_once 'clases/TipoEdicion.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GestionCitas
 *
 * @author Vero
 */
class GestionTipoEdicion {
    
    public static function crearTipoEdicion($tipoEd) {
        global $conexion;
        $t = new TipoEdicion();
        $t = $tipoEd;

        try {
            $query = "insert into t_tipo_edicion
                (descripcion_edicion)
                    values ('" . $t->getDescripcion() . "')";
            $result = mysql_query($query, $conexion);
            $_SESSION["insertada"] = "El tipo de edición se ha insertado con éxito.";
            
            return mysql_insert_id($conexion);
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido crear el nuevo tipo de edición. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function modificarTipoEdicion($tipoEd) {
        global $conexion;
        $t = new TipoEdicion();
        $t = $tipoEd;

        try {
            $query = "update t_tipo_edicion
                set descripcion_edicion = '" . $t->getDescripcion() . "'
                    where id_tipo_edicion = '" . $t->getId() . "'";
            $result = mysql_query($query, $conexion);
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido modificar el tipo de edición. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function eliminarTipoEdicion($id) {
        global $conexion;

        try {
            $query = "delete from t_tipo_edicion
                where id_tipo_edicion = '" . $id . "'";
            $result = mysql_query($query, $conexion);
            return mysql_affected_rows();
        } catch (Exception $e) {
            $_SESSION["error"] = "No se ha podido eliminar el tipo de edición. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalTiposEdiciones() {
        global $conexion;

        try {
            $query = "select count(*) as total 
                from t_tipo_edicion";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            $total = $row["total"];
            
            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los tipos de ediciones. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function totalFiltered($where) {
        global $conexion;
        
        if ($where != "") {
            $w = " where descripcion_edicion like '%$where%' ";
        }

        try {
            $query = "select count(*) as total 
                from t_tipo_edicion 
                $w";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            $total = $row["total"];
            
            return $total;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los tipos de ediciones. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarTiposEdL($begin, $limit, $field, $order, $where) {
        global $conexion;
        $tipos = array();
        
        if ($begin != "" && $limit != "") {
            $l = " limit $begin, $limit ";
        }
        
        if ($field != "" && $order != "") {
            $orderBy = " order by $field $order ";
        }
        
        if ($where != "") {
            $w = " where descripcion_edicion like '%$where%' ";
        }

        try {
            $query = "select *
                from t_tipo_edicion";
            if (isset($w)) {
                $query = $query . $w;
            }
            if (isset($orderBy)) {
                $query = $query . $orderBy;
            }
            if (isset($l)) {
                $query = $query . $l;
            }
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $tipo = new TipoEdicion();
                $tipo->setId($row["id_tipo_edicion"]);
                $tipo->setDescripcion($row["descripcion_edicion"]);
                
                $tipos[] = $tipo;
            }
            
            return $tipos;
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los tipos de ediciones. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarTiposEd() {
        global $conexion;
        $tipos = array();

        try {
            $query = "select *
                from t_tipo_edicion
                order by id_tipo_edicion";
            $result = mysql_query($query, $conexion);
            
            while($row = mysql_fetch_array($result)) {
                $tipo = new TipoEdicion();
                $tipo->setId($row["id_tipo_edicion"]);
                $tipo->setDescripcion($row["descripcion_edicion"]);
                
                $tipos[] = $tipo;
            }
            
            if (count($tipos) > 0) {
                return $tipos;
            } else {
                $_SESSION["noHay"] = "No existe ningún tipo de edición en BookWorm.";
                return 0;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar los tipos de ediciones. Inténtelo de nuevo más tarde.";
        }
    }
    
    public static function recuperarTipoEdicion($id) {
        global $conexion;

        try {
            $query = "select *
                from t_tipo_edicion
                where id_tipo_edicion = $id";
            $result = mysql_query($query, $conexion);
            $row = mysql_fetch_array($result);
            
            if(!$row) {
                return 0;
            } else {
                $tipo = new TipoEdicion();
                $tipo->setId($row["id_tipo_edicion"]);
                $tipo->setDescripcion($row["descripcion_edicion"]);
                
                return $tipo;
            }
        } catch (Exception $e) {
            $_SESSION["error"] = "No se han podido recuperar el tipo de edición. Inténtelo de nuevo más tarde.";
        }
    }
}

?>
