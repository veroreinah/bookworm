<?php
require_once 'constant.php';
global $raiz;

$web = $_SERVER['REQUEST_URI'];

if (isset($_SESSION["usuario"])) {
    session_unregister("noHay");
    session_unregister("noHayE");
    session_unregister("noHayC");
    session_unregister("noHayL");
    session_unregister("noHayA");
    if ($_SESSION["tipoUsuario"] == "usuario") {
        switch ($web) {
            // DE LA PARTE PÚBLICA
            case $raiz . '/books.php':
            case $raiz . '/contact.php':
            case $raiz . '/error.php':
            case $raiz . '/':
            case $raiz . '/index.php':
            case $raiz . '/profile.php':
            case $raiz . '/search.php':
            case $raiz . '/search_results.php':
            
            // DE LA PARTE DE USUARIO
            case $raiz . '/user/book_modify.php?isbn=' . $_GET["isbn"]:
            case $raiz . '/user/books.php':
            case $raiz . '/user/complain.php':
            case $raiz . '/user/complain.php?isbn=' . $_GET["isbn"]:
            case $raiz . '/user/friends.php':
            case $raiz . '/user/invitations.php':
            case $raiz . '/user/modify_profile.php':
            case $raiz . '/user/new_book.php?isbn=' . $_GET["isbn"]:
            case $raiz . '/user/search_results.php':
            case $raiz . '/user/profile.php':
                session_unregister("data_users_page");
                session_unregister("data_books_page");
            case $raiz . '/book_details.php?isbn=' . $_GET["isbn"]:
            case $raiz . '/user/user_details.php':
            case $raiz . '/user/user_details.php?id=' . $_GET["id"]:
                break;
            default:
                header('location:' . $raiz . '/error.php');
                break;
        }
    } else {
        
    }
} else {
    switch ($web) {
        case $raiz . '/books.php':
        case $raiz . '/contact.php':
        case $raiz . '/error.php':
        case $raiz . '/forgot.php':
        case $raiz . '/':
        case $raiz . '/index.php':
        case $raiz . '/register.php':
        case $raiz . '/register.php?inv=' . $_GET["inv"]:
        case $raiz . '/search.php':
        case $raiz . '/search_results.php':
            session_unregister("data_books_page");
        case $raiz . '/book_details.php?isbn=' . $_GET["isbn"]:
            session_unregister("noHay");
            break;
        default:
            header('location:' . $raiz . '/error.php');
            break;
    }
}
?>
