<?php
session_start();
require_once '../utilidades/filter.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/clases/TipoUsuario.php';
require_once '../utilidades/constant.php';
global $raiz;

$user = new Usuario();
$user = unserialize($_SESSION["usuario"]);
$t = new TipoUsuario();
$t = $user->getTipoUsuario();

if ($user->getFechaNac() == '0000-00-00' || $user->getFechaNac() == 'null' || $user->getFechaNac() == '') {
    $date = '';
} else {
    $date2 = explode('-', $user->getFechaNac());
    $date = $date2[2] . '/' . $date2[1] . '/' . $date2[0];
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm - Tu perfil de usuario</title>
        <?php require_once '../parts/head.php'; ?>
        <script>
            function unregister() {
                $('#unregisterModal').modal({
                    backdrop: "static",
                    show: true
                });
            }
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "user/profile.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MODAL - DARSE DE BAJA -->
                <div id="unregisterModal" class="modal hide fade" >
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">×</a>
                        <h3>¿Deseas darte de baja?</h3>
                    </div>
                    <div class="modal-body">
                        Haciendo clic en "Confirmar", te darás de baja de BookWorm y todos tus datos serán eliminados.
                        Más adelante puedes volver a registrarte, pero tendrás que crear tu estantería de nuevo, tus comentarios, etc.<br /><br />
                        <strong>¿Estás seguro de que quieres darte de baja de BookWorm?</strong>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
                        <a id="removeBtn" href="<?php echo $raiz; ?>/forms/unregister.php" class="btn"><i class="icon-remove"></i> Confirmar</a>
                    </div> 
                </div>
                
                <!-- MAIN CONTENT -->
                <div class="span4">
                    <h2>Imagen de perfil</h2>
                    <ul class="thumbnails">
                        <li class="span4">
                            <div class="thumbnail">
                                <img src="<?php echo $raiz; ?>/img/users/<?php echo $user->getImagen(); ?>" alt="" style="margin-bottom: 0px;">
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="span8">
                    <h2>Datos en BookWorm</h2>
                    <?php if (isset($_SESSION["modify"])) { ?>
                        <div class="alert alert-success">
                            <a class="close" data-dismiss="alert">×</a>
                            <?php
                            echo $_SESSION["modify"];
                            session_unregister("modify");
                            ?>
                        </div>
                        <?php
                    }
                    if (isset($_SESSION["error"])) {
                        ?>
                        <div class="alert alert-error">
                            <a class="close" data-dismiss="alert">×</a>
                            <?php
                            echo "<strong>" . $_SESSION["error"] . "</strong>";
                            session_unregister("error");
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <div>
                        <dl>
                            <dt>Nombre de usuario</dt>
                            <dd><?php echo $user->getNombre(); ?></dd>
                            <dt>Email</dt>
                            <dd><?php echo $user->getEmail(); ?></dd>
                            <dt>Contraseña</dt>
                            <dd>********</dd>
                            <dt>Fecha de nacimiento</dt>
                            <dd><?php echo ($date == '') ? "<em>Todavía no has indicado tu fecha de nacimiento.</em>" : $date; ?></dd>
                            <dt>Facebook</dt>
                            <dd><?php echo ($user->getFacebook() == 'null' || $user->getFacebook() == '') ? "<em>Todavía no has indicado tu cuenta de Facebook.</em>" : "<a href='".$user->getFacebook()."' target='_blank'>".$user->getFacebook()."</a>"; ?></dd>
                            <?php if ($t->getId() == 1) { ?>
                                <dt>Tipo de cuenta</dt>
                                <dd>Administrador de BookWorm</dd>
                            <?php } ?>
                        </dl>
                    </div>
                    <p>
                        <a href="modify_profile.php" class="btn" style="margin-top:20px;"><i class="icon-pencil"></i> Modificar datos</a>
                        <?php if ($t->getId() == 2) { ?>
                            <a href="#" class="btn" style="margin-top:20px;" onclick="unregister()"><i class="icon-remove"></i> Darse de baja</a>
                        <?php } ?>
                    </p>
                </div>
            </div>

            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>