<?php
session_start();
require_once '../utilidades/filter.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm - Añadir libro</title>
        <?php require_once '../parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/book-validation.js"></script>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/new-book.js"></script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "user/books.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php $_SESSION["page"] = "user/new_book.php" ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Nuevo libro</a></li>
                        </ul>
                        <div class="tab-content">
                            <!-- PESTAÑA 1 -->
                            <div class="tab-pane active" id="1">
                                <?php if (isset($_SESSION["addBook"])) { ?>
                                    <div class="alert alert-success">
                                        <?php
                                        echo "<strong>" . $_SESSION["addBook"] . "</strong>";
                                        session_unregister("addBook");
                                        ?>
                                    </div>
                                <?php } ?>
                                <?php require_once '../parts/book_insert.php'; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>

            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>