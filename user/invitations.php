<?php
session_start();
require_once '../utilidades/GestionRedSocial.php';
require_once '../utilidades/filter.php';
require_once '../utilidades/constant.php';
global $raiz;

$userInv = new Usuario();
$userInv = unserialize($_SESSION["usuario"]);

$invitaciones = array();
$invitaciones = GestionRedSocial::recuperarInvitaciones($userInv->getId());
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm - Invitaciones</title>
        <?php require_once '../parts/head.php'; ?>
        <script>
            function accept(nombre, enlace) {
                jQuery.ajax({
                    dataType: "json",
                    type: "post",
                    url: "../ajax/response.php",
                    data: {
                        enlace: enlace,
                        respuesta: 1
                    },
                    success: function(data) {
                        if (data) {
                            alert("Has aceptado la invitacion de " + nombre);
                            location.reload();
                        }
                    }
                });
            }
            
            function reject(nombre, enlace) {
                jQuery.ajax({
                    dataType: "json",
                    type: "post",
                    url: "../ajax/response.php",
                    data: {
                        enlace: enlace,
                        respuesta: 0
                    },
                    success: function(data) {
                        if (data) {
                            alert("Has rechazado la invitacion de " + nombre);
                            location.reload();
                        }
                    }
                });
            }
            
            <?php if (count($invitaciones) != 0) { ?>
                jQuery(document).ready(function() {
                    jQuery('#inv_table').dataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "oLanguage": {
                            "sUrl": "../dataTableUtils/spanish"
                        },
                        "aoColumnDefs": [ 
                            { "bSortable": false, "aTargets": [ 2, 3 ] }
                        ],
                        "sPaginationType": "full_numbers",
                        "sAjaxSource": "../dataTableUtils/inv_processing.php"
                    } );
                });
            <?php } ?>
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "user/invitations.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Invitaciones pendientes</a></li>
                        </ul>
                        <div class="tab-content">
                            <!-- PESTAÑA 1 -->
                            <div class="tab-pane active" id="1">
                                <?php if (count($invitaciones) == 0) { ?>
                                    <div class="alert alert-error">
                                        No tienes ninguna invitación pendiente.
                                    </div>
                                <?php } else { ?>
                                    <table id="inv_table" class="table">
                                        <thead>
                                            <tr>
                                                <th id="main-column">Usuario</th>
                                                <th>Fecha</th>
                                                <th class="icons-column"></th>
                                                <th class="icons-column"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>

            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>