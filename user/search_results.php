<?php
session_start();
require_once '../utilidades/filter.php';
require_once '../utilidades/constant.php';
global $raiz;

if (!isset($_POST) || $_POST == null || count($_POST) == 0) {
    $_POST = $_SESSION["data_users"];
}

$filtro = $_POST["filtro"];
switch ($filtro) {
    case 'nombre':
        $campo = "Resultados: " . $_POST["nombre"] . " (nombre de usuario)";
        break;
    case 'email':
        $campo = "Resultados: " . $_POST["email"] . " (email)";
        break;
    default:
        break;
}

$_SESSION["data_users"] = $_POST;
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm - Resultados de búsqueda</title>
        <?php require_once '../parts/head.php'; ?>
        <script>
            jQuery(document).ready(function() {
                jQuery('#users_table').dataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": false,
                    "bInfo": false,
                    "oLanguage": {
                        "sUrl": "../dataTableUtils/spanish"
                    },
                    "sPaginationType": "full_numbers",
                    "sAjaxSource": "../dataTableUtils/search_users.php"
                } );
            });
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "search.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab"><?php echo $campo; ?></a></li>
                        </ul>
                        <div class="tab-content">
                            <!-- PESTAÑA 1 -->
                            <div class="tab-pane active" id="1">
                                <div align="right" style="margin: 15px;">
                                    <a href="<?php echo $raiz; ?>/search.php" class="btn">
                                        <i class="icon-arrow-left"></i> Volver</a>
                                </div>
                                <hr>
                                <table id="users_table" class="table">
                                    <thead>
                                        <tr>
                                            <th>Nombre de usuario</th>
                                            <th class="icons-column"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>

            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>