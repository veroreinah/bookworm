<?php
session_start();
require_once '../utilidades/GestionRedSocial.php';
require_once '../utilidades/filter.php';
require_once '../utilidades/constant.php';
global $raiz;

$u = new Usuario();
$u = unserialize($_SESSION["usuario"]);

$amigos = array();
$amigos = GestionRedSocial::recuperarAmigos($u->getId());
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm - Amigos</title>
        <?php require_once '../parts/head.php'; ?>
        <script>
            function unfollow(id, nombre) {
                jQuery.ajax({
                    dataType: "json",
                    type: "post",
                    url: "../ajax/unfollow.php",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        if (data) {
                            alert("Ya has dejado de seguir a " + nombre);
                            location.reload();
                        }
                    }
                });
            }
            
            <?php if (count($amigos) != 0) { ?>
                jQuery(document).ready(function() {
                    jQuery('#friends_table').dataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "oLanguage": {
                            "sUrl": "../dataTableUtils/spanish"
                        },
                        "aoColumnDefs": [ 
                            { "bSortable": false, "aTargets": [ 1, 2 ] }
                        ],
                        "sPaginationType": "full_numbers",
                        "sAjaxSource": "../dataTableUtils/friends_processing.php"
                    } );
                });
            <?php } ?>
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "user/friends.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Amigos</a></li>
                        </ul>
                        <div class="tab-content">
                            <!-- PESTAÑA 1 -->
                            <div class="tab-pane active" id="1">
                                <?php if (count($amigos) == 0) { ?>
                                    <div class="alert alert-error">
                                        <?php
                                        echo "<strong>" . $_SESSION["noHayA"] . "</strong>";
                                        session_unregister("noHayA");
                                        ?>
                                    </div>
                                <?php } else { ?>
                                    <table id="friends_table" class="table">
                                        <thead>
                                            <tr>
                                                <th>Nombre de usuario</th>
                                                <th class="icons-column"></th>
                                                <th class="icons-column"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>

            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>