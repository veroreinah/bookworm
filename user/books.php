<?php
session_start();
require_once '../utilidades/filter.php';
require_once '../utilidades/GestionLibros.php';
require_once '../utilidades/constant.php';
global $raiz;

$libros = array();
$libros = GestionLibros::recuperarLibros();

$estanteria = array();
$estanteria = GestionLibros::recuperarIsbnPorUsuario();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm - Estantería</title>
        <?php require_once '../parts/head.php'; ?>
        <script>
            jQuery(document).ready(function() {
                <?php if (count($estanteria) > 0) { ?>
                    jQuery('#shelf_table').dataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "oLanguage": {
                            "sUrl": "../dataTableUtils/spanish"
                        },
                        "aoColumnDefs": [ 
                            { "bSortable": false, "aTargets": [ 0, 4 ] }
                        ],
                        "sPaginationType": "full_numbers",
                        "sAjaxSource": "../dataTableUtils/shelf_processing.php"
                    } );
                <?php } ?>
                    
                <?php if (count($libros) > 0) { ?>
                    jQuery('#books_table').dataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "oLanguage": {
                            "sUrl": "../dataTableUtils/spanish"
                        },
                        "aoColumnDefs": [ 
                            { "bSortable": false, "aTargets": [ 0, 5 ] }
                        ],
                        "sPaginationType": "full_numbers",
                        "sAjaxSource": "../dataTableUtils/books_processing.php"
                    } );
                <?php } ?>
            });
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "user/books.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Mi estantería</a></li>
                            <li><a href="#2" data-toggle="tab">Todos los libros</a></li>
                        </ul>
                        <div class="tab-content">
                            <!-- PESTAÑA 1 -->
                            <div class="tab-pane active" id="1">
                                <?php if (isset($_SESSION["noHayE"])) { ?>
                                    <div class="alert alert-error">
                                        <?php
                                        echo "<strong>" . $_SESSION["noHayE"] . "</strong>";
                                        session_unregister("noHayE");
                                        ?>
                                    </div>
                                <?php } else { ?>
                                    <table id="shelf_table" class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>ISBN</th>
                                                <th>Título</th>
                                                <th>Autor/es</th>
                                                <th class="icons-column"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                <?php } ?>
                            </div>
                            <!-- PESTAÑA 2 -->
                            <div class="tab-pane" id="2">
                                <?php if (isset($_SESSION["noHayL"])) { ?>
                                    <div class="alert alert-error">
                                        <?php
                                        echo "<strong>" . $_SESSION["noHayL"] . "</strong>";
                                        session_unregister("noHayL");
                                        ?>
                                    </div>
                                <?php } else { ?>
                                    <table id="books_table" class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>ISBN</th>
                                                <th class="title-column">Título</th>
                                                <th class="title-column">Autor/es</th>
                                                <th class="title-column">Usuario</th>
                                                <th class="icons-column"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>

            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>