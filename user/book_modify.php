<?php
session_start();
require_once '../utilidades/filter.php';
require_once '../utilidades/GestionLibros.php';
require_once '../utilidades/GestionTematica.php';
require_once '../utilidades/clases/Libro.php';
require_once '../utilidades/clases/Autor.php';
require_once '../utilidades/clases/Editorial.php';
require_once '../utilidades/clases/TipoEdicion.php';
require_once '../utilidades/clases/Tematica.php';
require_once '../utilidades/constant.php';
global $raiz;

$libro = new Libro();
$libro = GestionLibros::recuperarLibro($_GET["isbn"]);
$autores = array();
$autores = $libro->getAutores();

$autoresId = "";
for ($i = 0; $i < count($autores); $i++) {
    $autor = new Autor();
    $autor = $autores[$i];
    
    $autoresId .= $autor->getId() . "-";
}

$editorial = new Editorial();
if ($libro->getEditorial() == null) {
    $editorial = 0;
} else {
    $editorial = $libro->getEditorial();
}

$tipoEdicion = new TipoEdicion();
if ($libro->getTipoEdicion() == null) {
    $tipoEdicion = 0;
} else {
    $tipoEdicion = $libro->getTipoEdicion();
}

$tematicas = array();
$tematicas = $libro->getTematicas();
$tematicasId = array();
if (count($tematicas) > 0) {
    for ($i = 0; $i < count($tematicas); $i++) {
        $t = new Tematica();
        $t = $tematicas[$i];
        $tematicasId[] = $t->getId();
    }
}

$tem = array();
$tem = GestionTematica::recuperarTematicas();
?>

<!DOCTYPE html>
<html>
    <head>
        <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'admin') { ?>
            <title>Libros - Panel de administración de BookWorm</title>
        <?php } else { ?>
            <title>BookWorm - Libros</title>
        <?php } ?>
        <?php require_once '../parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/book-validation.js"></script>
        <script>
            function soloNumeros(event){
                // 8 -> borrado
                // 9 -> tabulador
                // 37-40 -> flechas  
                if (event.keyCode == 8 || event.keyCode == 9 || (event.keyCode >= 37 && event.keyCode <= 40)) {
                    // permitimos determinadas teclas, no hacemos nada
                } else {
                    // Nos aseguramos que sea un numero, sino evitamos la accion
                    if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) 
                        && (event.keyCode != 88)) {
                        event.preventDefault();
                    }   
                }
            }
            
            function addAuthor() {
                if (jQuery('#input05').val() == '' || jQuery('#input05').val().trim() == '') {
                    jQuery('#input05').val('');
                    jQuery('#input05temp').val('0');
                } else {
                    jQuery('#empty').empty();
                    id = jQuery('#input05temp').val();
                    
                    if (id == 0) {
                        var k = Math.floor(Math.random() * 1000000);
                        id = '0_' + k + 'x';
                    }
                    
                    if (jQuery('#input05id').val().search(/(^id\-)|(\-id\-)/) == -1 
                        && $("#help-inline-text05").html().search(jQuery('#input05').val()) == -1) {
                        
                        jQuery('#input05new').val(jQuery('#input05new').val() + jQuery('#input05').val() + ';');
                        jQuery('#input05id').val(jQuery('#input05id').val() + id + '-');
                        jQuery('#help-inline-text05').html(jQuery('#help-inline-text05').html()
                            + '<span id="autor-' + id + '"><br /><a class="remove autores" href="#" onclick="deleteAuthor(\'' + id + '\')"><i class="icon-remove"></i></a> '
                            + '<span id="' + id + '">' + jQuery('#input05').val() + '</span></span>');

                        jQuery('#input05').val('');
                        jQuery('#input05temp').val('0');
                    } else {
                        jQuery('#input05').val('');
                        jQuery('#input05temp').val('0');
                    }
                }
            }
            
            function deleteAuthor(id) {
                nombre = jQuery('#' + id).html();
                jQuery('#autor-' + id).remove();
                
                jQuery('#input05id').val(jQuery('#input05id').val().replace(id + '-', ''));
                jQuery('#input05new').val(jQuery('#input05new').val().replace(nombre + ';', ''));
                
                if (jQuery('#input05id').val() == '') {
                    jQuery('#empty').html('todavía no has añadido ninguno...');
                }
            }
            
            function borrarId() {
                jQuery('#input05temp').val('0');
            }
            
            function borrarIdE() {
                jQuery('#input08id').val('0');
            }
            
            function borrarIdT() {
                jQuery('#input09id').val('0');
            }
            
            jQuery('#themeBtn').live("click",function(){
                $('#newTheme').modal({
                    backdrop: "static",
                    show: true
                });
            });
            
            function addTheme(raiz){
                tematica = jQuery('#input11').val();
                if (tematica == '') {
                    jQuery('#help-inline-text11').html('Campo obligatorio');
                    jQuery('#input11').attr("style", "border: 1px solid #D90416;");
                } else {
                    jQuery('#help-inline-text11').html('');
                    jQuery('#input11').attr("style", "border: 1px solid #CCC;");
                    jQuery.ajax({
                        dataType: "json",
                        type: "post",
                        url: raiz + "/ajax/themeInsert.php",
                        data: {
                            tematica: tematica
                        },
                        success: function(data) {
                            if (!data.exist) {
                                var option = "<option value='" + data.id + "'>" + data.descripcion + "</option>";
                                
                                $('#newTheme').modal('hide');
                                jQuery('#multiSelect').append(option);
                            } else {
                                $('#newTheme').modal('hide');
                            }
                        }
                    });
                }
            };
            
            jQuery(document).ready(function() {
                jQuery('#file').customFileInput({
                    feedback_text: 'Cambia la portada',
                    button_text: '...',
                    button_change_text: '...'
                });
                
                $("#textarea").wysiwyg({
                    rmUnusedControls: true,
                    controls: {
                        bold: { visible : true },
                        italic: { visible : true }
                    },
                    initialContent: ''
                });
                
                jQuery('#input04').keydown(function(event) {
                    soloNumeros(event);
                });
                
                jQuery('#input06').keydown(function(event) {
                    soloNumeros(event);
                });
                
                jQuery('#input05').typeahead({
                    source: function (typeahead, query) {
                        $.ajax({
                            dataType : 'json',
                            url: "../typeAheadUtils/authors.php?q="+query,
                            success: function (data) {
                                typeahead.process(data)
                            }
                        });
                    },
                    property: "nombre",
                    onselect: function (obj) {
                        $('#input05temp').val(obj.id);
                    }
                });
                
                jQuery('#input08').typeahead({
                    source: function (typeahead, query) {
                        $.ajax({
                            dataType : 'json',
                            url: "../typeAheadUtils/editorial.php?q="+query,
                            success: function (data) {
                                typeahead.process(data)
                            }
                        });
                    },
                    property: "nombre",
                    onselect: function (obj) {
                        $('#input08id').val(obj.id);
                    }
                });
                
                jQuery('#input09').typeahead({
                    source: function (typeahead, query) {
                        $.ajax({
                            dataType : 'json',
                            url: "../typeAheadUtils/editing.php?q="+query,
                            success: function (data) {
                                typeahead.process(data)
                            }
                        });
                    },
                    property: "nombre",
                    onselect: function (obj) {
                        $('#input09id').val(obj.id);
                    }
                });
            });
            
        </script>
    </head>
    <body>
        <div class="container">
            <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'admin') { ?>
                <?php $_SESSION["page"] = "admin/books.php" ?>
            <?php } else { ?>
                <?php $_SESSION["page"] = "books.php" ?>
            <?php } ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <div class="contact-content">
                        <!-- MODAL - NUEVA TEMÁTICA -->
                        <div id="newTheme" class="modal hide fade" >
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal">×</a>
                                <h3>Datos de la nueva temática</h3>
                            </div>
                            <form class="mod">
                                <fieldset>
                                    <div class="modal-body">
                                        <div class="control-group">
                                            <label class="control-label" for="input02">Descripción</label>
                                            <div class="controls">
                                                <input type="text" class="input-xlarge" id="input11" name="nombre" />
                                                <span id="help-inline-text11" class="help-inline" style="color: #B94A48;"></span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                            <div class="modal-footer">
                                <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
                                <a id="addThemeBtn" href="#" onclick="addTheme('<?php echo $raiz; ?>')" class="btn"><i class="icon-plus"></i> Añadir</a>
                            </div> 
                        </div>
                        <form class="form-horizontal" action="<?php echo $raiz; ?>/forms/m_books.php" method="post" name="modificar_form" enctype="multipart/form-data">
                            <fieldset>
                                <legend>Modificar datos</legend>
                                <div class="control-group">
                                    <label class="control-label" for="input01">ISBN</label>
                                    <div class="controls">
                                        <input type="text" class="input-xlarge" id="input01" name="isbn" readonly="readonly" value="<?php echo $libro->getIsbn(); ?>" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input02">Titulo *</label>
                                    <div class="controls">
                                        <input type="text" class="input-xlarge" id="input02" name="titulo" value="<?php echo $libro->getTitulo(); ?>" />
                                        <span id="help-inline-text02" class="help-inline" style="color: #B94A48;"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input03">Subtitulo</label>
                                    <div class="controls">
                                        <input type="text" class="input-xlarge" id="input03" name="subtitulo" value="<?php echo $libro->getSubtitulo(); ?>" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input05">Autores *</label>
                                    <div class="controls">
                                        <input id="input05temp" value="" type="hidden" />
                                        <input id="input05id" name="autores" value="<?php echo $autoresId; ?>" type="hidden" />
                                        <input id="input05new" name="nuevos_autores" value="" type="hidden" />
                                        <input class="input-xlarge" id="input05" type="text" onkeypress="borrarId()" />
                                        <button class="btn" type="button" onclick="addAuthor()"><i class="icon-plus-sign"></i> Añadir</button>
                                        <span id="help-inline-text05" class="help-inline" style="color: #BEBEC5; display: block;"><em>Autores: <span id="empty"></span></em>
                                            <?php for ($i = 0; $i < count($autores); $i++) { 
                                                $autor = new Autor();
                                                $autor = $autores[$i]; ?>
                                                <span id="autor-<?php echo $autor->getId(); ?>"><br />
                                                    <a class="remove autores" href="#" onclick="deleteAuthor('<?php echo $autor->getId(); ?>')"><i class="icon-remove"></i></a>
                                                    <span id="<?php echo $autor->getId(); ?>"><?php echo $autor->getNombre(); ?></span></span>
                                            <?php } ?>
                                        </span>
                                        <span id="help-inline-text05no" class="help-inline" style="color: #B94A48; display: block;"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="file">Portada</label>
                                    <div class="controls">
                                        <input type="file" name="portada" id="file" accept="image/*" />
                                    </div>
                                    <div class="controls" style="margin-top: 10px;">
                                        Portada actual: <img src="<?php echo $raiz; ?>/img/portadas/<?php echo $libro->getPortada(); ?>" alt="" width="80" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="textarea">Sinopsis</label>
                                    <div class="controls">
                                        <textarea class="input-xlarge" id="textarea" rows="6" name="sinopsis" style="resize: none; width:450px;" ><?php echo $libro->getSinopsis(); ?></textarea>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input04">Páginas</label>
                                    <div class="controls">
                                        <input type="text" class="input" id="input04" name="paginas" maxlength="6" value="<?php echo $libro->getPaginas() == "0" ? "" : $libro->getPaginas(); ?>" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input06">Año de publicación</label>
                                    <div class="controls">
                                        <input type="text" class="input" id="input06" name="publicacion" maxlength="4" value="<?php echo $libro->getAnyoPublicacion() == "0000" ? "" : $libro->getAnyoPublicacion(); ?>" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input08">Editorial</label>
                                    <div class="controls">
                                        <input id="input08id" name="editorial" value="<?php echo $editorial == 0 ? "" : $editorial->getId() ; ?>" type="hidden" />
                                        <input type="text" class="input-xlarge" id="input08" name="nombre_editorial" onkeypress="borrarIdE()" value="<?php echo $editorial == 0 ? "" : $editorial->getNombre() ; ?>" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input07">Edición</label>
                                    <div class="controls">
                                        <input type="text" class="input-xlarge" id="input07" name="edicion" value="<?php echo $libro->getEdicion(); ?>" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input09">Tipo de edición</label>
                                    <div class="controls">
                                        <input id="input09id" name="tipo_edicion" value="<?php echo $tipoEdicion == 0 ? "" : $tipoEdicion->getId() ; ?>" type="hidden" />
                                        <input type="text" class="input-xlarge" id="input09" name="nombre_tipo_edicion" onkeypress="borrarIdT()" value="<?php echo $tipoEdicion == 0 ? "" : $tipoEdicion->getDescripcion() ; ?>" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input11">Temáticas</label>
                                    <div class="controls">
                                        <select multiple="multiple" id="multiSelect" name="tematicas[]">
                                            <?php for ($i = 0; $i < count($tem); $i++) {
                                                $t = new Tematica();
                                                $t = $tem[$i]; ?>
                                            <option value="<?php echo $t->getId() ?>" <?php echo in_array($t->getId(), $tematicasId) ? "selected='selected'" : ""; ?>><?php echo $t->getDescripcion(); ?></option>
                                            <?php } ?>
                                        </select>
                                        <button class="btn" type="button" id="themeBtn" style="vertical-align: top;"><i class="icon-plus-sign"></i> Nueva temática</button>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <a href="#" class="btn" onclick="enviarMod()"><i class="icon-pencil"></i> Modificar</a>
                                    <a href="<?php echo $raiz; ?>/book_details.php?isbn=<?php echo $libro->getIsbn(); ?>" class="btn"><i class="icon-minus"></i> Cancelar</a>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>

            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>
