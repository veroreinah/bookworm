<?php
session_start();
require_once '../utilidades/filter.php';
require_once '../utilidades/GestionLibros.php';
require_once '../utilidades/clases/Libro.php';
require_once '../utilidades/constant.php';
global $raiz;

if (isset($_GET["isbn"])) {
    $libro = new Libro();
    $libro = GestionLibros::recuperarLibro($_GET["isbn"]);
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm - Denunciar libro</title>
        <?php require_once '../parts/head.php'; ?>
        <script>
            function enviarDenuncia() {
                error = false;
                if (jQuery("#textarea").val() == '') {
                    jQuery("#textarea").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text01").html("Campo obligatorio");
                    error = true;
                } else {
                    jQuery("#textarea").attr("style", "border: 1px solid #CCC;");
                    jQuery("#help-inline-text01").html("");
                }
                
                //Si todo "ok", enviar formulario
                if (!error) {
                    document.complain_form.submit();
                }
            }
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "user/books.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php $_SESSION["page"] = "user/new_book.php" ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Denuncia</a></li>
                        </ul>
                        <div class="tab-content">
                            <!-- PESTAÑA 1 -->
                            <div class="tab-pane active" id="1">
                                <?php if (isset($_GET["isbn"])) { ?>
                                    <form class="form-horizontal" action="<?php echo $raiz; ?>/forms/complain.php" method="post" name="complain_form">
                                        <fieldset>
                                            <legend>Denunciar: <?php echo $libro->getTitulo(); ?></legend>
                                            <em>Si crees que los datos de este libro son erróneos, incompletos, etc., infórmanos.</em>
                                            <div class="control-group" style="margin-top: 15px;">
                                                <label class="control-label" for="input01">ISBN *</label>
                                                <div class="controls">
                                                    <input type="text" class="input-xlarge" id="input01" name="isbn" readonly="readonly" value="<?php echo $libro->getIsbn(); ?>" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="textarea">Mensaje *</label>
                                                <div class="controls">
                                                    <textarea class="input-xlarge" id="textarea" rows="7" name="mensaje"></textarea>
                                                    <span id="help-inline-text01" class="help-inline" style="color: #B94A48;"></span>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <a href="#" class="btn" onclick="enviarDenuncia()"><i class="icon-envelope"></i> Enviar</a>
                                                <a href="<?php echo $raiz; ?>/book_details.php?isbn=<?php echo $libro->getIsbn(); ?>" class="btn"><i class="icon-minus"></i> Cancelar</a>
                                            </div>
                                        </fieldset>
                                    </form>
                                <?php } else { ?>
                                    <?php if (isset($_SESSION["thanksComp"])) { ?>
                                        <div class="alert alert-success">
                                            <?php
                                            echo $_SESSION["thanksComp"];
                                            session_unregister("thanksComp");
                                            ?>
                                        </div>
                                        <div align="right" style="margin: 15px;">
                                            <a href="<?php echo $raiz; ?>/book_details.php?isbn=<?php echo $_SESSION["isbnComp"]; ?>" class="btn">
                                                <i class="icon-arrow-left"></i> Volver</a>
                                        </div>
                                    <?php session_unregister("isbnComp");
                                    } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>

            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>