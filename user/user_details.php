<?php
session_start();
require_once '../utilidades/filter.php';
require_once '../utilidades/GestionUsuarios.php';
require_once '../utilidades/GestionLibros.php';
require_once '../utilidades/GestionRedSocial.php';
require_once '../utilidades/clases/Usuario.php';
require_once '../utilidades/constant.php';
global $raiz;

$user = new Usuario();
if (isset($_GET["id"])) {
    $user = GestionUsuarios::recuperarUsuario($_GET["id"]);
} elseif (isset($_SESSION["userEmail"])) {
    $user = GestionUsuarios::recuperarUsuarioPorEmail($_SESSION["userEmail"]);
}

$user2 = new Usuario();
$user2 = unserialize($_SESSION["usuario"]);

if ($user->getId() === $user2->getId()) {
    header('location:' . $raiz . '/user/profile.php');
}

$amigos = GestionRedSocial::recuperarAmigos($user2->getId());
$invitado = GestionRedSocial::invitado($user->getId());
$invitado2 = GestionRedSocial::invitado2($user->getId());

$isbn = array();
$isbn = GestionLibros::recuperarIsbnPorUsuario($user->getId());

if ($_SESSION["tipoUsuario"] == "admin") {
    if ($user->getFechaNac() == '0000-00-00' || $user->getFechaNac() == 'null' || $user->getFechaNac() == '') {
        $date = '';
    } else {
        $date2 = explode('-', $user->getFechaNac());
        $date = $date2[2] . '/' . $date2[1] . '/' . $date2[0];
    }
}

$t = new TipoUsuario();
$t = $user->getTipoUsuario();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm - Estantería de <?php echo $user->getNombre(); ?></title>
        <?php require_once '../parts/head.php'; ?>
        <script>
            <?php if ($_SESSION["tipoUsuario"] == "admin") { ?>
                jQuery("#delete_user").live("click",function(){
                    $('#removeModal').modal({
                        backdrop: "static",
                        show: true
                    });
                });
            <?php } ?>
            function follow(raiz) {
                id = jQuery('#userId').val();
                jQuery.ajax({
                    dataType: "json",
                    type: "post",
                    url: raiz + "/ajax/follow.php",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        if (data) {
                            alert("Tu invitación se ha enviado. Cuando el usuario la acepte, podrás seguir sus lecturas más fácilmente.");
                            location.reload();
                        }
                    }
                });
            }
            
            function unfollow(raiz) {
                id = jQuery('#userId').val();
                jQuery.ajax({
                    dataType: "json",
                    type: "post",
                    url: raiz + "/ajax/unfollow.php",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        if (data) {
                            alert("Ya has dejado de seguir a este usuario.");
                            location.reload();
                        }
                    }
                });
            }
            
            jQuery(document).ready(function() {
                <?php if (count($isbn) > 0) { ?>
                    jQuery('#shelf_table').dataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "oLanguage": {
                            "sUrl": "../dataTableUtils/spanish"
                        },
                        "aoColumnDefs": [ 
                            { "bSortable": false, "aTargets": [ 0, 4 ] }
                        ],
                        "sPaginationType": "full_numbers",
                        "sAjaxSource": "../dataTableUtils/shelf_processing.php",
                        "fnServerParams": function ( aoData ) {
                            aoData.push( { "name": "id_usuario", "value": jQuery("#userId").val() } );
                        }
                    } );
                <?php } ?>
            });
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "user/user_details.php" ?>
            <?php require_once '../parts/menu.php'; ?>
            <?php require_once '../parts/carousel.php'; ?>
            <?php require_once '../parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                     
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Estantería de <?php echo $user->getNombre(); ?></a></li>
                            <li><a href="#2" data-toggle="tab">Perfil de <?php echo $user->getNombre(); ?></a></li>
                        </ul>
                        <div class="tab-content">
                            <!-- PESTAÑA 1 -->
                            <div class="tab-pane active" id="1">
                                <div align="right" style="margin: 15px;">
                                    <?php if ($_SESSION["tipoUsuario"] == "admin") { ?>
                                        <a href="<?php echo $raiz; ?>/admin/user_modify.php?id=<?php echo $user->getId(); ?>" class="btn">
                                            <i class="icon-pencil"></i> Modificar</a>
                                        <a id="delete_user" href="#" class="btn">
                                            <i class="icon-remove"></i> Eliminar</a>
                                    <?php } else { ?>
                                        <?php if ($t->getId() == 1) { ?>
                                            <em>Este usuario no puede recibir invitaciones</em>
                                        <?php } else { ?>
                                            <?php if ($invitado) { ?>
                                                <em>Ya has enviado una invitación a este usuario. Por favor, espera a que responda.</em>
                                            <?php } elseif ($invitado2) { ?>
                                                <em>Tienes una invitación pendiente de este usuario. <a href="<?php echo $raiz ?>/user/invitations.php">Revisa tus invitaciones</a>.</em>
                                            <?php } else { ?>
                                                <?php if (in_array($user->getId(), $amigos)) { ?>
                                                    <a href="#" class="btn" onclick="unfollow('<?php echo $raiz; ?>')">
                                                        <i class="icon-remove"></i> Dejar de seguir</a>
                                                <?php } else { ?>
                                                    <a href="#" class="btn" onclick="follow('<?php echo $raiz; ?>')">
                                                        <i class="icon-plus"></i> Seguir en BookWorm</a>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <a href="<?php echo $_SESSION["data_users_page"] == "1" ? "$raiz/user/search_results.php" : "javascript:history.back()"?>" class="btn">
                                        <i class="icon-arrow-left"></i> Volver</a>
                                </div>
                                <hr>
                                <?php if ($_SESSION["tipoUsuario"] == "admin") { ?>
                                    <?php if (isset($_SESSION["modify"])) { ?>
                                        <div class="alert alert-success">
                                            <a class="close" data-dismiss="alert">×</a>
                                            <?php
                                            echo $_SESSION["modify"];
                                            session_unregister("modify");
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    if (isset($_SESSION["error"])) {
                                        ?>
                                        <div class="alert alert-error">
                                            <a class="close" data-dismiss="alert">×</a>
                                            <?php
                                            echo "<strong>" . $_SESSION["error"] . "</strong>";
                                            session_unregister("error");
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                <?php } ?>
                                <?php if (isset($_SESSION["userExists"])) { ?>
                                    <div class="alert alert-success">
                                        <?php
                                        echo "<strong>" . $_SESSION["userExists"] . "</strong>";
                                        session_unregister("userExists");
                                        ?>
                                    </div>
                                <?php } ?>
                                <?php if (count($isbn) == 0) { ?>
                                    <div class="alert alert-error">
                                        Este usuario todavía no tiene ningún libro en su estantería.
                                    </div>
                                <?php } else { ?>
                                <div style="clear: both;">
                                    <table id="shelf_table" class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>ISBN</th>
                                                <th>Título</th>
                                                <th>Autor/es</th>
                                                <th class="icons-column"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <?php } ?>
                            </div>
                            <!-- PESTAÑA 2 -->
                            <div class="tab-pane" id="2">
                                <div align="right" style="margin: 15px;">
                                    <?php if ($_SESSION["tipoUsuario"] == "admin") { ?>
                                        <a href="<?php echo $raiz; ?>/admin/user_modify.php?id=<?php echo $user->getId(); ?>" class="btn">
                                            <i class="icon-pencil"></i> Modificar</a>
                                        <a id="delete_user" href="#" class="btn">
                                            <i class="icon-remove"></i> Eliminar</a>
                                    <?php } else { ?>
                                        <?php if ($t->getId() == 1) { ?>
                                            <em>Este usuario no puede recibir invitaciones</em>
                                        <?php } else { ?>
                                            <?php if ($invitado) { ?>
                                                <em>Ya has enviado una invitación a este usuario. Por favor, espera a que responda.</em>
                                            <?php } elseif ($invitado2) { ?>
                                                <em>Tienes una invitación pendiente de este usuario. <a href="<?php echo $raiz ?>/user/invitations.php">Revisa tus invitaciones</a>.</em>
                                            <?php } else { ?>
                                                <?php if (in_array($user->getId(), $amigos)) { ?>
                                                    <a href="#" class="btn" onclick="unfollow('<?php echo $raiz; ?>')">
                                                        <i class="icon-remove"></i> Dejar de seguir</a>
                                                <?php } else { ?>
                                                    <a href="#" class="btn" onclick="follow('<?php echo $raiz; ?>')">
                                                        <i class="icon-plus"></i> Seguir en BookWorm</a>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <a href="<?php echo $_SESSION["data_users_page"] == "1" ? "$raiz/user/search_results.php" : "javascript:history.back()"?>" class="btn">
                                        <i class="icon-arrow-left"></i> Volver</a>
                                </div>
                                <hr>
                                <div class="span3">
                                    <ul class="thumbnails">
                                        <li class="span3">
                                            <div class="thumbnail">
                                                <img src="<?php echo $raiz; ?>/img/users/<?php echo $user->getImagen(); ?>" alt="" style="margin-bottom: 0px;">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="span5">
                                    <input type="hidden" id="userId" value="<?php echo $user->getId(); ?>" />
                                    <dl>
                                        <dt>Nombre de usuario</dt>
                                        <dd><?php echo $user->getNombre(); ?></dd>
                                        <?php if ($_SESSION["tipoUsuario"] == "admin") { ?>
                                            <dt>Email</dt>
                                            <dd><?php echo $user->getEmail(); ?></dd>
                                            <dt>Contraseña</dt>
                                            <dd>********</dd>
                                            <dt>Fecha de nacimiento</dt>
                                            <dd><?php echo ($date == '') ? "<em>Todavía no se ha indicado una fecha de nacimiento.</em>" : $date; ?></dd>
                                            <dt>Tipo de cuenta</dt>
                                            <?php if ($t->getId() == 1) { ?>
                                                <dd>Administrador de BookWorm</dd>
                                            <?php } else { ?>
                                                <dd>Usuario de BookWorm</dd>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if ($user->getFacebook() == 'null' || $user->getFacebook() == '') {
                                            
                                        } else { ?>
                                            <dt>Visita su perfil de Facebook</dt>
                                            <dd><a href="<?php echo $user->getFacebook(); ?>" target="_blank"><img src="<?php echo $raiz; ?>/img/facebook.jpg" /></a></dd>
                                        <?php } ?>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <?php if ($_SESSION["tipoUsuario"] == "admin") { ?>
                        <!-- MODAL - ELIMINAR -->
                        <div id="removeModal" class="modal hide fade" >
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal">×</a>
                                <h3>¿Desea eliminar este usuario?</h3>
                            </div>
                            <div class="modal-body">
                                <p id="nombre"><?php echo $user->getNombre(); ?></p>
                                <p id="email"><em><?php echo $user->getEmail(); ?></em></p>
                            </div>
                            <div class="modal-footer">
                                <a href="#" class="btn" data-dismiss="modal"><i class="icon-minus"></i> Cancelar</a>
                                <a id="removeBtn" href="../forms/e_users.php?id=<?php echo $user->getId(); ?>" class="btn"><i class="icon-remove"></i> Eliminar</a>
                            </div> 
                        </div>
                    <?php } ?>
                </div>

                <!-- SIDEBAR -->
                <?php require_once '../parts/sidebar.php'; ?>
            </div>

            <?php require_once '../parts/footer.php'; ?>
        </div>
    </body>
</html>
