<?php
session_start();
require_once '../utilidades/GestionLibros.php';
require_once '../utilidades/clases/Comentario.php';

$u = new Usuario();
$u = unserialize($_SESSION["usuario"]);

GestionLibros::agregarValoracion($u->getId(), $_POST["isbn"], $_POST["valoracion"]);
if (isset($_SESSION["ok"])) {
    $success = true;
} else {
    $success = false;
}

echo json_encode($success);
?>
