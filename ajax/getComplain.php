<?php
session_start();
require_once '../utilidades/GestionDenuncias.php';
require_once '../utilidades/clases/Denuncia.php';

$denuncia = new Denuncia();
$denuncia = GestionDenuncias::recuperarDenuncia($_POST["enlace"]);

$data["contenido"] = $denuncia->getTexto();

echo json_encode($data);
?>
