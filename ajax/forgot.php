<?php
session_start();
require_once '../utilidades/GestionUsuarios.php';
require_once '../utilidades/constant.php';
require_once '../lib/Swift-4.1.7/lib/swift_required.php';
global $email;

$pass = GestionUsuarios::restablecerPassword($_POST["email"]);

//Enviar email
set_time_limit(-1);

if ($_SERVER['SERVER_NAME'] == 'localhost') {
    $base = "http://localhost:8080/bookworm";
} else {
    $base = "http://bookworm.phpfogapp.com";
}

$body = "<html><body>";

$body .= "Esta es tu nueva contraseña:<br /><br />";
$body .= "<strong>$pass</strong><br /><br />";
$body .= "<em>Te recomendamos que la cambies en tu página de perfil cuando accedas de nuevo a <a href='$base/index.php'>BookWorm</a></em>";
$body .= "</body></html>";

// Create a message
$message = Swift_Message::newInstance()

  // Give the message a subject
  ->setSubject('BookWorm - Tu nueva contraseña')

  // Set the From address with an associative array
  ->setFrom(array($email))

  // Set the To addresses with an associative array
  ->setTo(array($_POST["email"]))

  // Give it a body
  ->setBody($body, 'text/html')
  ;
  
$password = getenv('EMAIL_PASSWORD');
$password = str_replace('\\', '', $password);

// Create the Transport
$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
  ->setUsername($email)
  ->setPassword($password)
  ;

// Create the Mailer using your created Transport
$mailer = Swift_Mailer::newInstance($transport);

// Send the message
$result = $mailer->send($message);

if ($result) {
    $success = true;
} else {
    $success = false;
}

$success = true;

echo json_encode($success);
?>
