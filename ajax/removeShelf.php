<?php
session_start();
require_once '../utilidades/GestionLibros.php';

$u = new Usuario();
$u = unserialize($_SESSION["usuario"]);

GestionLibros::quitarLibro($u->getId(), $_POST["isbn"]);
$success = true;

echo json_encode($success);
?>