<?php
session_start();
require_once '../utilidades/GestionLibros.php';
require_once '../utilidades/clases/Comentario.php';

$u = new Usuario();
$u = unserialize($_SESSION["usuario"]);

$libro = new Libro();
$libro->setIsbn($_POST["isbn"]);

$comentario = new Comentario();
$comentario->setLibro($libro);
$comentario->setUsuario($u);
$comentario->setComentario($_POST["comentario"]);
$comentario->setSpoiler($_POST["spoiler"]);

if ($_POST["titulo"] == '') {
    $comentario->setTitulo("");
} else {
    $comentario->setTitulo($_POST["titulo"]);
}

$comentario->setFecha(date("Y-m-d"));

GestionLibros::agregarComentario($comentario);
if (isset($_SESSION["ok"])) {
    $success = true;
    $_SESSION["comment"] = "OK";
} else {
    $success = false;
}

echo json_encode($success);
?>
