<?php
session_start();
require_once '../utilidades/GestionRedSocial.php';

if (intval($_POST["respuesta"]) === 1) {
    $respuesta = true;
} else {
    $respuesta = false;
}

GestionRedSocial::responderInvitacion($_POST["enlace"], $respuesta);
$success = true;

echo json_encode($success);
?>
