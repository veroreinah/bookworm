function enviarMod() {
    error = false;
    //Título
    if (jQuery("#input02").val() == '') {
        jQuery("#input02").attr("style", "border: 1px solid #D90416;");
        jQuery("#help-inline-text02").html("Campo obligatorio");
        error = true;
    } else {
        jQuery("#input02").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-text02").html("");
    }
    
    //Autores
    if (jQuery("#input05id").val() == '') {
        jQuery("#input05").attr("style", "border: 1px solid #D90416;");
        jQuery("#help-inline-text05no").html("Campo obligatorio");
        error = true;
    } else {
        jQuery("#input05").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-text05no").html("");
    }
    
    //Si todo "ok", enviar formulario
    if (!error) {
        document.modificar_form.submit();
    }
}

function enviarInsertar(raiz) {
    error = false;
    //ISBN
    if (jQuery("#input01").val() == '') {
        jQuery("#input01").attr("style", "border: 1px solid #D90416;");
        jQuery("#help-inline-text01").html("Campo obligatorio");
        error = true;
    } else {
        valor = jQuery("#input01").val();
        jQuery("#input01").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-text01").html("");
        
        if (valor.length == 10 || valor.length == 13) {
            jQuery.ajax({
                async: false,
                dataType: "json",
                type: "post",
                url: raiz + "/ajax/ajaxValidation.php",
                data: {
                    campo: 'isbn', 
                    tabla: 't_libros', 
                    valor: valor
                },
                success: function(data) {
                    if (data.exists) {
                        jQuery("#input01").attr("style", "border: 1px solid #D90416;");
                        jQuery("#help-inline-text01").html("ISBN existente");
                        error = true;
                    } else {
                        var total = 0;
                        if (valor.length == 10) {
                            for (i = 1; i < valor.length; i++) {
                                temp = i * valor.charAt(i-1);
                                total += temp;
                            }
                            total = total % 11;

                            if (total == 10) {
                                total = 'x';
                            }

                            if (total != valor.charAt(9)) {
                                jQuery("#input01").attr("style", "border: 1px solid #D90416;");
                                jQuery("#help-inline-text01").html("ISBN incorrecto");
                                error = true;
                            } else {
                                jQuery("#input01").attr("style", "border: 1px solid #CCC;");
                                jQuery("#help-inline-text01").html("");
                            }
                        } else {
                            var mul = '131313131313';
                            for (i = 1; i < valor.length; i++) {
                                temp = mul.charAt(i-1) * valor.charAt(i-1);
                                total += temp;
                            }
                            
                            var control = 0;
                            while (total % 10 != 0) {
                                total ++;
                                control++;
                            }
                            
                            if (control != valor.charAt(12)) {
                                jQuery("#input01").attr("style", "border: 1px solid #D90416;");
                                jQuery("#help-inline-text01").html("ISBN incorrecto");
                                error = true;
                            } else {
                                jQuery("#input01").attr("style", "border: 1px solid #CCC;");
                                jQuery("#help-inline-text01").html("");
                            }
                        }
                    }
                }
            });
        } else {
            jQuery("#input01").attr("style", "border: 1px solid #D90416;");
            jQuery("#help-inline-text01").html("ISBN incorrecto");
            error = true;
        }
    }
    //Título
    if (jQuery("#input02").val() == '') {
        jQuery("#input02").attr("style", "border: 1px solid #D90416;");
        jQuery("#help-inline-text02").html("Campo obligatorio");
        error = true;
    } else {
        jQuery("#input02").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-text02").html("");
    }
    
    //Autores
    if (jQuery("#input05id").val() == '') {
        jQuery("#input05").attr("style", "border: 1px solid #D90416;");
        jQuery("#help-inline-text05no").html("Campo obligatorio");
        error = true;
    } else {
        jQuery("#input05").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-text05no").html("");
    }
    
    //Si todo "ok", enviar formulario
    if (!error) {
        document.insertar_form.submit();
    }
}