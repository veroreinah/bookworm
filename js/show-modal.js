jQuery("a.pencil").live("click",function(){
    var id = jQuery(this).parent().parent().children().first().children().attr("class");
    var nombre = jQuery(this).parent().parent().children().first().children().html();
                
    jQuery("#input01M").val(nombre);
    jQuery("#idM").val(id);
                
    $('#modifyModal').modal({
        backdrop: "static",
        show: true
    });
                
});
            
jQuery("a.remove").live("click",function(){
    var id = jQuery(this).parent().parent().children().first().children().attr("class");
    var nombre = jQuery(this).parent().parent().children().first().children().html();
                
    jQuery("#nombre").html(nombre);
    jQuery("#removeBtn").attr("href", jQuery("#enlace").val() + id);
    
    jQuery.post("../ajax/in_use.php", 
        {filtro: jQuery("#enlace").val(), valor: id}, 
        function(data){
            if (data.use) {
                alert("El valor seleccionado no se puede eliminar porque está en uso");
            } else {
                $('#removeModal').modal({
                    backdrop: "static",
                    show: true
                });
            }
        }, 
        "json");
});
            
function enviarMod(campo, tabla) {
    error = false;
    if (jQuery("#input01M").val() == '') {
        jQuery("#input01M").attr("style", "border: 1px solid #D90416");
        jQuery("#help-inline-textM").html("Campo obligatorio");
        error = true;
    } else {
        valor = jQuery("#input01M").val();
        jQuery("#input01M").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-textM").html("");
        jQuery.post("../ajax/ajaxValidation.php", 
            {campo: campo, tabla: tabla, valor: valor}, 
            function(data){
                if (data.exists) {
                    jQuery("#input01M").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-textM").html("Valor existente");
                    error = true;
                } else {
                    jQuery("#input01M").attr("style", "border: 1px solid #CCC;");
                    jQuery("#help-inline-textM").html("");
                }
                
                if (!error) {
                    document.modificar_form.submit();
                }
            }, 
            "json");
    }
}

function enviarInsertar(campo, tabla) {
    error = false;
    if (jQuery("#input01").val() == '') {
        jQuery("#input01").attr("style", "border: 1px solid #D90416");
        jQuery("#help-inline-text").html("Campo obligatorio");
        error = true;
    } else {
        valor = jQuery("#input01").val();
        jQuery("#input01").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-text").html("");
        jQuery.post("../ajax/ajaxValidation.php", 
            {campo: campo, tabla: tabla, valor: valor}, 
            function(data){
                if (data.exists) {
                    jQuery("#input01").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text").html("Valor existente");
                    error = true;
                } else {
                    jQuery("#input01").attr("style", "border: 1px solid #CCC;");
                    jQuery("#help-inline-text").html("");
                }
                
                if (!error) {
                    document.insertar_form.submit();
                }
            }, 
            "json");
    }
}