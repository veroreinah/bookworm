function enviarMod(raiz) {
    error = false;
    //Nombre
    if (jQuery("#input01").val() == '') {
        jQuery("#input01").attr("style", "border: 1px solid #D90416");
        jQuery("#help-inline-text01").html("Campo obligatorio");
        error = true;
    } else {
        valor = jQuery("#input01").val();
        id = jQuery("#id").val();
        jQuery("#input01").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-text01").html("");
        jQuery.ajax({
            async: false,
            dataType: "json",
            type: "post",
            url: raiz + "/ajax/ajaxValidation.php",
            data: {campo: 'nombre_usuario', tabla: 't_usuarios', valor: valor, id: id},
            success: function(data) {
                if (data.exists) {
                    jQuery("#input01").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text01").html("Nombre de usuario existente");
                    error = true;
                } else {
                    jQuery("#input01").attr("style", "border: 1px solid #CCC;");
                    jQuery("#help-inline-text01").html("");
                }
            }
        });
    }
    //Email
    if (jQuery("#input02").val() == '') {
        jQuery("#input02").attr("style", "border: 1px solid #D90416");
        jQuery("#help-inline-text02").html("Campo obligatorio");
        error = true;
    } else {
        valor = jQuery("#input02").val();
        id = jQuery("#id").val();
        jQuery("#input02").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-text02").html("");
        jQuery.ajax({
            async: false,
            dataType: "json",
            type: "post",
            url: raiz + "/ajax/ajaxValidation.php",
            data: {campo: 'email', tabla: 't_usuarios', valor: valor, id: id},
            success: function(data) {
                if (data.exists) {
                    jQuery("#input02").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text02").html("Email existente");
                    error = true;
                } else {
                    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(jQuery("#input02").val())){
			jQuery("#input02").attr("style", "border: 1px solid #CCC;");
                        jQuery("#help-inline-text02").html("");
                    } else {
                        jQuery("#input02").attr("style", "border: 1px solid #D90416");
                        jQuery("#help-inline-text02").html("Email incorrecto");
                        error = true;
                    }
                }
            }
        });
    }
    
    if ((jQuery("#input03").val() == '' && jQuery("#input04").val() != '') ||
        (jQuery("#input04").val() == '' && jQuery("#input03").val() != '')) {
        alert ('Debe rellenar los tres campos de contraseña para modificarla.');
    }
    
    //Comprobamos que la contraseña es la actual
    oldPassword = true;
    if (jQuery("#input03").val() == '') {
        jQuery("#input03").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-text03").html("");
        oldPassword = false;
    } else {
        valor = jQuery("#input03").val();
        id = jQuery("#id").val();
        jQuery("#input03").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-text03").html("");
        jQuery.ajax({
            async: false,
            dataType: "json",
            type: "post",
            url: raiz + "/ajax/ajaxValidation.php",
            data: {campo: 'passw', tabla: 't_usuarios', valor: valor, id: id, password: true},
            success: function(data) {
                if (data.exists) {
                    jQuery("#input03").attr("style", "border: 1px solid #CCC;");
                    jQuery("#help-inline-text03").html("");
                } else {
                    jQuery("#input03").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text03").html("Contraseña incorrecta");
                    error = true;
                }
            }
        });
    }
    
    //Contraseña
    password = true;
    if (oldPassword) {
        if (jQuery("#input04").val() == '') {
            jQuery("#input04").attr("style", "border: 1px solid #D90416");
            jQuery("#help-inline-text04").html("Introduce la nueva contraseña");
            error = true;
            password = false;
        } else {
            if (jQuery("#input04").val().length < 6) {
                jQuery("#input04").attr("style", "border: 1px solid #D90416");
                jQuery("#help-inline-text04").html("Recuerda: mínimo 6 caracteres");
                error = true;
                password = false;
            } else {
                jQuery("#input04").attr("style", "border: 1px solid #CCC;");
                jQuery("#help-inline-text04").html("");
            }
        }
        //Confirmación de contraseña
        //Comprobamos este campo cuando se ha introducido el anterior
        if (password) {
                if (jQuery("#input05").val() == '') {
                jQuery("#input05").attr("style", "border: 1px solid #D90416");
                jQuery("#help-inline-text05").html("Confirma la contraseña");
                error = true;
                password = false;
            } else {
                jQuery("#input05").attr("style", "border: 1px solid #CCC;");
                jQuery("#help-inline-text05").html("");
            }
        }
        //Comprobar que las contraseñas son iguales
        //Sólo se comprueba cuando se han introducido las dos
        if (password) {
            if (jQuery("#input04").val() != jQuery("#input05").val()) {
                jQuery("#input04").attr("style", "border: 1px solid #D90416");
                jQuery("#input05").attr("style", "border: 1px solid #D90416");
                jQuery("#help-inline-text05").html("Las contraseñas no coinciden");
                error = true;
            } else {
                jQuery("#input04").attr("style", "border: 1px solid #CCC;");
                jQuery("#input05").attr("style", "border: 1px solid #CCC;");
                jQuery("#help-inline-text05").html("");
            }
        }
    }
    
    //Si todo "ok", enviar formulario
    if (!error) {
        document.modificar_form.submit();
    }
}

function enviarInsertar(raiz) {
    error = false;
    //Nombre
    if (jQuery("#input01").val() == '') {
        jQuery("#input01").attr("style", "border: 1px solid #D90416");
        jQuery("#help-inline-text01").html("Campo obligatorio");
        error = true;
    } else {
        valor = jQuery("#input01").val();
        jQuery("#input01").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-text01").html("");
        jQuery.ajax({
            async: false,
            dataType: "json",
            type: "post",
            url: raiz + "/ajax/ajaxValidation.php",
            data: {campo: 'nombre_usuario', tabla: 't_usuarios', valor: valor},
            success: function(data) {
                if (data.exists) {
                    jQuery("#input01").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text01").html("Nombre de usuario existente");
                    error = true;
                } else {
                    jQuery("#input01").attr("style", "border: 1px solid #CCC;");
                    jQuery("#help-inline-text01").html("");
                }
            }
        });
    }
    //Email
    if (jQuery("#input02").val() == '') {
        jQuery("#input02").attr("style", "border: 1px solid #D90416");
        jQuery("#help-inline-text02").html("Campo obligatorio");
        error = true;
    } else {
        valor = jQuery("#input02").val();
        jQuery("#input02").attr("style", "border: 1px solid #CCC;");
        jQuery("#help-inline-text02").html("");
        jQuery.ajax({
            async: false,
            dataType: "json",
            type: "post",
            url: raiz + "/ajax/ajaxValidation.php",
            data: {campo: 'email', tabla: 't_usuarios', valor: valor},
            success: function(data) {
                if (data.exists) {
                    jQuery("#input02").attr("style", "border: 1px solid #D90416");
                    jQuery("#help-inline-text02").html("Email existente");
                    error = true;
                } else {
                    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(jQuery("#input02").val())){
			jQuery("#input02").attr("style", "border: 1px solid #CCC;");
                        jQuery("#help-inline-text02").html("");
                    } else {
                        jQuery("#input02").attr("style", "border: 1px solid #D90416");
                        jQuery("#help-inline-text02").html("Email incorrecto");
                        error = true;
                    }
                }
            }
        });
    }
    password = true;
    //Contraseña
    if (jQuery("#input03").val() == '') {
        jQuery("#input03").attr("style", "border: 1px solid #D90416");
        jQuery("#help-inline-text03").html("Campo obligatorio");
        error = true;
        password = false;
    } else {
        if (jQuery("#input03").val().length < 6) {
            jQuery("#input03").attr("style", "border: 1px solid #D90416");
            jQuery("#help-inline-text03").html("Recuerda: mínimo 6 caracteres");
            error = true;
            password = false;
        } else {
            jQuery("#input03").attr("style", "border: 1px solid #CCC;");
            jQuery("#help-inline-text03").html("");
        }
    }
    //Confirmación de contraseña
    //Comprobamos este campo cuando se ha introducido el anterior
    if (password) {
            if (jQuery("#input04").val() == '') {
            jQuery("#input04").attr("style", "border: 1px solid #D90416");
            jQuery("#help-inline-text04").html("Confirma la contraseña");
            error = true;
            password = false;
        } else {
            jQuery("#input04").attr("style", "border: 1px solid #CCC;");
            jQuery("#help-inline-text04").html("");
        }
    }
    //Comprobar que las contraseñas son iguales
    //Sólo se comprueba cuando se han introducido las dos
    if (password) {
        if (jQuery("#input03").val() != jQuery("#input04").val()) {
            jQuery("#input03").attr("style", "border: 1px solid #D90416");
            jQuery("#input04").attr("style", "border: 1px solid #D90416");
            jQuery("#help-inline-text04").html("Las contraseñas no coinciden");
            error = true;
        } else {
            jQuery("#input03").attr("style", "border: 1px solid #CCC;");
            jQuery("#input04").attr("style", "border: 1px solid #CCC;");
            jQuery("#help-inline-text04").html("");
        }
    }
    
    if (jQuery("#optionsCheckbox").attr("checked") == 'checked') {
        jQuery("#help-inline-text05").html("");
    } else {
        jQuery("#help-inline-text05").html("Debe aceptar para poder continuar.");
        error = true;
    }
    
    //Si todo "ok", enviar formulario
    if (!error) {
        document.insertar_form.submit();
    }
}