jQuery(document).ready(function() {
    jQuery('#search_input02').keydown(function(event) {
        soloNumeros(event);
    });
    jQuery('#search_input06').keydown(function(event) {
        soloNumeros(event);
    });
});

function search_validate(opcion) {
    switch(opcion)
    {
        case 1:
            //título
            error = false;
            if (jQuery.trim(jQuery("#search_input01").val()) == '') {
                jQuery("#search_input01").attr("style", "border: 1px solid #D90416;");
                jQuery("#s-help-inline-text01").html("Campo obligatorio");
                error = true;
            } else {
                jQuery("#search_input01").attr("style", "border: 1px solid #CCC;");
                jQuery("#s-help-inline-text01").html("");
            }
            if (!error) {
                document.buscar_titulo_form.submit();
            }
            break;
        case 2:
            //búsqueda desde info-row
            error = false;
            if (jQuery.trim(jQuery("#search_info_row").val()) == '') {
                jQuery("#search_info_row").attr("style", "border: 1px solid #D90416;");
                error = true;
            } else {
                jQuery("#search_info_row").attr("style", "border: 1px solid #CCC;");
            }
            if (!error) {
                document.info_row_form.submit();
            }
            break;
        case 3:
            //isbn
            error = false;
            if (jQuery.trim(jQuery("#search_input02").val()) == '') {
                jQuery("#search_input02").attr("style", "border: 1px solid #D90416;");
                jQuery("#s-help-inline-text02").html("Campo obligatorio");
                error = true;
            } else {
                jQuery("#search_input02").attr("style", "border: 1px solid #CCC;");
                jQuery("#s-help-inline-text02").html("");
                
                valor = jQuery("#search_input02").val();
                if (valor.length == 10 || valor.length == 13) {
                    var total = 0;
                    if (valor.length == 10) {
                        for (i = 1; i < valor.length; i++) {
                            temp = i * valor.charAt(i-1);
                            total += temp;
                        }
                        total = total % 11;

                        if (total == 10) {
                            total = 'x';
                        }

                        if (total != valor.charAt(9)) {
                            jQuery("#search_input02").attr("style", "border: 1px solid #D90416;");
                            jQuery("#s-help-inline-text02").html("ISBN incorrecto");
                            error = true;
                        } else {
                            jQuery("#search_input02").attr("style", "border: 1px solid #CCC;");
                            jQuery("#s-help-inline-text02").html("");
                        }
                    } else {
                        var mul = '131313131313';
                        for (i = 1; i < valor.length; i++) {
                            temp = mul.charAt(i-1) * valor.charAt(i-1);
                            total += temp;
                        }

                        var control = 0;
                        while (total % 10 != 0) {
                            total ++;
                            control++;
                        }

                        if (control != valor.charAt(12)) {
                            jQuery("#search_input02").attr("style", "border: 1px solid #D90416;");
                            jQuery("#s-help-inline-text02").html("ISBN incorrecto");
                            error = true;
                        } else {
                            jQuery("#search_input02").attr("style", "border: 1px solid #CCC;");
                            jQuery("#s-help-inline-text02").html("");
                        }
                    }
                } else {
                    jQuery("#search_input02").attr("style", "border: 1px solid #D90416;");
                    jQuery("#s-help-inline-text02").html("ISBN incorrecto");
                    error = true;
                }
            }
            if (!error) {
                document.buscar_isbn_form.submit();
            }
            break;
        case 4:
            //avanzada
            error = false;
            if (jQuery.trim(jQuery("#search_input04").val()) == '' 
                && jQuery.trim(jQuery("#search_input05").val()) == ''
                && jQuery.trim(jQuery("#search_input06").val()) == ''
                && jQuery("#search_select01").val() == 0
                && jQuery("#search_select02").val() == 0
                && jQuery("#search_select03").val() == 0) {
                jQuery("#search_input04").attr("style", "border: 1px solid #D90416;");
                jQuery("#search_input05").attr("style", "border: 1px solid #D90416;");
                jQuery("#search_input06").attr("style", "border: 1px solid #D90416;");
                jQuery("#search_select01").attr("style", "border: 1px solid #D90416;");
                jQuery("#search_select02").attr("style", "border: 1px solid #D90416;");
                jQuery("#search_select03").attr("style", "border: 1px solid #D90416;");
                jQuery("#s-help-inline-text07").html("Rellene, al menos, un campo");
                error = true;
            } else {
                jQuery("#search_input04").attr("style", "border: 1px solid #CCC;");
                jQuery("#search_input05").attr("style", "border: 1px solid #CCC;");
                jQuery("#search_input06").attr("style", "border: 1px solid #CCC;");
                jQuery("#search_select01").attr("style", "border: 1px solid #CCC;");
                jQuery("#search_select02").attr("style", "border: 1px solid #CCC;");
                jQuery("#search_select03").attr("style", "border: 1px solid #CCC;");
                jQuery("#s-help-inline-text07").html("");
            }
            if (!error) {
                document.buscar_avanzada_form.submit();
            }
            break;
        case 5:
            //nombre
            error = false;
            if (jQuery.trim(jQuery("#search_input07").val()) == '') {
                jQuery("#search_input07").attr("style", "border: 1px solid #D90416;");
                jQuery("#s-help-inline-text05").html("Campo obligatorio");
                error = true;
            } else {
                jQuery("#search_input07").attr("style", "border: 1px solid #CCC;");
                jQuery("#s-help-inline-text05").html("");
            }
            if (!error) {
                document.buscar_nombre_form.submit();
            }
            break;
        case 6:
            //email
            error = false;
            if (jQuery.trim(jQuery("#search_input08").val()) == '') {
                jQuery("#search_input08").attr("style", "border: 1px solid #D90416;");
                jQuery("#s-help-inline-text06").html("Campo obligatorio");
                error = true;
            } else {
                jQuery("#search_input08").attr("style", "border: 1px solid #CCC;");
                jQuery("#s-help-inline-text06").html("");
                
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(jQuery("#search_input08").val())){
                    jQuery("#search_input08").attr("style", "border: 1px solid #CCC;");
                    jQuery("#s-help-inline-text06").html("");
                } else {
                    jQuery("#search_input08").attr("style", "border: 1px solid #D90416");
                    jQuery("#s-help-inline-text06").html("Email incorrecto");
                    error = true;
                }
            }
            if (!error) {
                document.buscar_email_form.submit();
            }
            break;
        default:
            break;
    }
}