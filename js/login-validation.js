function login(raiz) {
    error = false;
    //Nombre
    if (jQuery("#pass").val() == '') {
        jQuery("#pass").attr("style", "border: 1px solid #D90416");
        error = true;
    } else {
        jQuery("#pass").attr("style", "border: 1px solid #CCC;");
    }
    //Email
    if (jQuery("#email").val() == '') {
        jQuery("#email").attr("style", "border: 1px solid #D90416");
        error = true;
    } else {
        jQuery("#email").attr("style", "border: 1px solid #CCC;");
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(jQuery("#email").val())){
            jQuery("#email").attr("style", "border: 1px solid #CCC;");
        } else {
            jQuery("#email").attr("style", "border: 1px solid #D90416");
            error = true;
        }
    }
    
    //Si todo "ok", enviar formulario
    if (!error) {
        document.login_form.submit();
    }
}