function soloNumeros(event){
    // 8 -> borrado
    // 9 -> tabulador
    // 37-40 -> flechas  
    if (event.keyCode == 8 || event.keyCode == 9 || (event.keyCode >= 37 && event.keyCode <= 40)) {
        // permitimos determinadas teclas, no hacemos nada
    } else {
        // Nos aseguramos que sea un numero, sino evitamos la accion
        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) 
            && (event.keyCode != 88)) {
            event.preventDefault();
        }   
    }
}
            
function addAuthor() {
    if (jQuery('#input05').val() == '' || jQuery('#input05').val().trim() == '') {
        jQuery('#input05').val('');
        jQuery('#input05temp').val('0');
    } else {
        jQuery('#empty').empty();
        id = jQuery('#input05temp').val();
                    
        if (id == 0) {
            var k = Math.floor(Math.random() * 1000000);
            id = '0_' + k + 'x';
        }
                    
        if (jQuery('#input05id').val().search(/(^id\-)|(\-id\-)/) == -1 
            && $("#help-inline-text05").html().search(jQuery('#input05').val()) == -1) {
                        
            jQuery('#input05new').val(jQuery('#input05new').val() + jQuery('#input05').val() + ';');
            jQuery('#input05id').val(jQuery('#input05id').val() + id + '-');
            jQuery('#help-inline-text05').html(jQuery('#help-inline-text05').html()
                + '<span id="autor-' + id + '"><br /><a class="remove autores" href="#" onclick="deleteAuthor(\'' + id + '\')"><i class="icon-remove"></i></a> '
                + '<span id="' + id + '">' + jQuery('#input05').val() + '</span></span>');

            jQuery('#input05').val('');
            jQuery('#input05temp').val('0');
        } else {
            jQuery('#input05').val('');
            jQuery('#input05temp').val('0');
        }
    }
}
            
function deleteAuthor(id) {
    nombre = jQuery('#' + id).html();
    jQuery('#autor-' + id).remove();
                
    jQuery('#input05id').val(jQuery('#input05id').val().replace(id + '-', ''));
    jQuery('#input05new').val(jQuery('#input05new').val().replace(nombre + ';', ''));
                
    if (jQuery('#input05id').val() == '') {
        jQuery('#empty').html('todavía no has añadido ninguno...');
    }
}
            
function borrarId() {
    jQuery('#input05temp').val('0');
}
            
function borrarIdE() {
    jQuery('#input08id').val('0');
}
            
function borrarIdT() {
    jQuery('#input09id').val('0');
}
            
jQuery('#themeBtn').live("click",function(){
    $('#newTheme').modal({
        backdrop: "static",
        show: true
    });
});
            
function addTheme(raiz){
    tematica = jQuery('#input11').val();
    if (tematica == '') {
        jQuery('#help-inline-text11').html('Campo obligatorio');
        jQuery('#input11').attr("style", "border: 1px solid #D90416;");
    } else {
        jQuery('#help-inline-text11').html('');
        jQuery('#input11').attr("style", "border: 1px solid #CCC;");
        jQuery.ajax({
            dataType: "json",
            type: "post",
            url: raiz + "/ajax/themeInsert.php",
            data: {
                tematica: tematica
            },
            success: function(data) {
                if (data) {
                    var option = "<option value='" + data.id + "'>" + data.descripcion + "</option>";
                                
                    $('#newTheme').modal('hide');
                    jQuery('#multiSelect').append(option);
                }
            }
        });
    }
};

jQuery(document).ready(function() {   
    jQuery('#file').customFileInput({
        feedback_text: 'Elige una portada',
        button_text: '...',
        button_change_text: '...'
    });
    
    $("#textarea").wysiwyg({
        rmUnusedControls: true,
        controls: {
            bold: { visible : true },
            italic: { visible : true }
        },
        initialContent: ''
    });
                
    jQuery('#input01').keydown(function(event) {
        soloNumeros(event);
    });
                
    jQuery('#input04').keydown(function(event) {
        soloNumeros(event);
    });
                
    jQuery('#input06').keydown(function(event) {
        soloNumeros(event);
    });
                
    jQuery('#input05').typeahead({
        source: function (typeahead, query) {
            $.ajax({
                dataType : 'json',
                url: "../typeAheadUtils/authors.php?q="+query,
                success: function (data) {
                    typeahead.process(data)
                }
            });
        },
        property: "nombre",
        onselect: function (obj) {
            $('#input05temp').val(obj.id);
        }
    });
                
    jQuery('#input08').typeahead({
        source: function (typeahead, query) {
            $.ajax({
                dataType : 'json',
                url: "../typeAheadUtils/editorial.php?q="+query,
                success: function (data) {
                    typeahead.process(data)
                }
            });
        },
        property: "nombre",
        onselect: function (obj) {
            $('#input08id').val(obj.id);
        }
    });
                
    jQuery('#input09').typeahead({
        source: function (typeahead, query) {
            $.ajax({
                dataType : 'json',
                url: "../typeAheadUtils/editing.php?q="+query,
                success: function (data) {
                    typeahead.process(data)
                }
            });
        },
        property: "nombre",
        onselect: function (obj) {
            $('#input09id').val(obj.id);
        }
    });
});