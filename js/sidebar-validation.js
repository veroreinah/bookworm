jQuery(document).ready(function() {
    jQuery('#isbn').keydown(function(event) {
        soloNumeros(event);
    });
});

function soloNumeros(event){
    // 8 -> borrado
    // 9 -> tabulador
    // 37-40 -> flechas  
    if (event.keyCode == 8 || event.keyCode == 9 || (event.keyCode >= 37 && event.keyCode <= 40)) {
    // permitimos determinadas teclas, no hacemos nada
    } else {
        // Nos aseguramos que sea un numero, sino evitamos la accion
        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) 
            && (event.keyCode != 88)) {
            event.preventDefault();
        }   
    }
}

function addToShelf() {
    error = false;
    //ISBN
    if (jQuery("#isbn").val() == '') {
        jQuery("#isbn").attr("style", "border: 1px solid #D90416;");
        error = true;
    } else {
        valor = jQuery("#isbn").val();
        jQuery("#isbn").attr("style", "border: 1px solid #CCC;");
        
        if (valor.length == 10 || valor.length == 13) {
            var total = 0;
            if (valor.length == 10) {
                for (i = 1; i < valor.length; i++) {
                    temp = i * valor.charAt(i-1);
                    total += temp;
                }
                total = total % 11;

                if (total == 10) {
                    total = 'x';
                }

                if (total != valor.charAt(9)) {
                    jQuery("#isbn").attr("style", "border: 1px solid #D90416;");
                    error = true;
                } else {
                    jQuery("#isbn").attr("style", "border: 1px solid #CCC;");
                }
            } else {
                var mul = '131313131313';
                for (i = 1; i < valor.length; i++) {
                    temp = mul.charAt(i-1) * valor.charAt(i-1);
                    total += temp;
                }

                var control = 0;
                while (total % 10 != 0) {
                    total ++;
                    control++;
                }

                if (control != valor.charAt(12)) {
                    jQuery("#isbn").attr("style", "border: 1px solid #D90416;");
                    error = true;
                } else {
                    jQuery("#isbn").attr("style", "border: 1px solid #CCC;");
                }
            }
        } else {
            jQuery("#isbn").attr("style", "border: 1px solid #D90416;");
            error = true;
        }
    }
    
    //Si todo "ok", enviar formulario
    if (!error) {
        document.shelf_form.submit();
    }
}

function invite() {
    error = false;
    //Email
    if (jQuery("#email").val() == '') {
        jQuery("#email").attr("style", "border: 1px solid #D90416");
        error = true;
    } else {
        jQuery("#email").attr("style", "border: 1px solid #CCC;");
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(jQuery("#email").val())){
            jQuery("#email").attr("style", "border: 1px solid #CCC;");
        } else {
            jQuery("#email").attr("style", "border: 1px solid #D90416");
            error = true;
        }
    }
    
    //Si todo "ok", enviar formulario
    if (!error) {
        document.invite_form.submit();
    }
}