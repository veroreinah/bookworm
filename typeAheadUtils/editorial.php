<?php
require_once '../utilidades/GestionEditoriales.php';
require_once '../utilidades/clases/Editorial.php';

$editoriales = array();
$editoriales = GestionEditoriales::recuperarEditoriales();

$output = array();
for ($i = 0; $i < count($editoriales); $i++) {
    $e = new Editorial();
    $e = $editoriales[$i];

    $row = array();
    $row["id"] = $e->getId();
    $row["nombre"] = $e->getNombre();
    $output[] = $row;
}

echo json_encode($output);
?>
