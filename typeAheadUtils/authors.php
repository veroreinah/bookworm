<?php
require_once '../utilidades/GestionAutores.php';
require_once '../utilidades/clases/Autor.php';

$autores = array();
$autores = GestionAutores::recuperarAutores();

$output = array();
for ($i = 0; $i < count($autores); $i++) {
    $a = new Autor();
    $a = $autores[$i];

    $row = array();
    $row["id"] = $a->getId();
    $row["nombre"] = $a->getNombre();
    $output[] = $row;
}

echo json_encode($output);
?>
