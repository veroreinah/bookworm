<?php
require_once '../utilidades/GestionTipoEdicion.php';
require_once '../utilidades/clases/TipoEdicion.php';

$tipos = array();
$tipos = GestionTipoEdicion::recuperarTiposEd();

$output = array();
for ($i = 0; $i < count($tipos); $i++) {
    $t = new TipoEdicion();
    $t = $tipos[$i];

    $row = array();
    $row["id"] = $t->getId();
    $row["nombre"] = $t->getDescripcion();
    $output[] = $row;
}

echo json_encode($output);
?>
