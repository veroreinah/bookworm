<?php
session_start();
require_once 'utilidades/filter.php';
require_once 'utilidades/GestionLibros.php';

$libros = array();
$libros = GestionLibros::recuperarLibros();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm - Libros</title>
        <?php require_once 'parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/login-validation.js"></script>
        <script>
            jQuery(document).ready(function() {
                <?php if (count($libros) > 0) { ?>
                    jQuery('#books_table').dataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "oLanguage": {
                            "sUrl": "dataTableUtils/spanish"
                        },
                        "aoColumnDefs": [ 
                            { "bSortable": false, "aTargets": [ 0, 4 ] }
                        ],
                        "sPaginationType": "full_numbers",
                        "sAjaxSource": "dataTableUtils/books_processing.php"
                    } );
                <?php } ?>
            });
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "books.php" ?>
            <?php require_once 'parts/menu.php'; ?>
            <?php require_once 'parts/carousel.php'; ?>
            <?php require_once 'parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Todos los libros</a></li>
                        </ul>
                        <div class="tab-content">
                            <!-- PESTAÑA 1 -->
                            <div class="tab-pane active" id="1">
                                <?php if (isset($_SESSION["noHayL"])) { ?>
                                    <div class="alert alert-error">
                                        <?php
                                        echo "<strong>" . $_SESSION["noHayL"] . "</strong>";
                                        session_unregister("noHayL");
                                        ?>
                                    </div>
                                <?php } else { ?>
                                    <table id="books_table" class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>ISBN</th>
                                                <th>Título</th>
                                                <th>Autor/es</th>
                                                <th class="icons-column">Detalles</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SIDEBAR -->
                <?php require_once 'parts/sidebar.php'; ?>
            </div>

            <?php require_once 'parts/footer.php'; ?>
        </div>
    </body>
</html>