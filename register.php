<?php
session_start();
require_once 'utilidades/filter.php';
require_once 'utilidades/constant.php';
global $raiz;
?>

<!DOCTYPE html>
<html>
    <head>
        <title>BookWorm - Regístrate</title>
        <?php require_once 'parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/jquery.pstrength-min.1.2.js"></script>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/user-validation.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                $(function() {
                    $('#input03').pstrength();
                });
                
                $.datepicker.setDefaults($.datepicker.regional["es"]);
                jQuery('#input05').datepicker({
                    yearRange: "-100Y:+0Y",
                    maxDate: "+0D",
                    changeMonth: true,
                    changeYear: true
		});
                
                jQuery('#file').customFileInput({
                    feedback_text: 'Elige una imagen',
                    button_text: '...',
                    button_change_text: '...'
                });
            });
        </script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "" ?>
            <?php require_once 'parts/menu.php'; ?>
            <?php require_once 'parts/carousel.php'; ?>
            <?php require_once 'parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span12">
                    <div class="contact-content">
                        <?php require_once 'parts/reg.php'; ?>
                    </div>
                </div>
            </div>

            <?php require_once 'parts/footer.php'; ?>
        </div>
    </body>
</html>
