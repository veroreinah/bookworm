<?php
session_start();
require_once 'utilidades/filter.php';
require_once 'utilidades/GestionLibros.php';
require_once 'utilidades/clases/Libro.php';
require_once 'utilidades/clases/Autor.php';
require_once 'utilidades/constant.php';
global $raiz;

$librosPopulares = array();
$librosPopulares = GestionLibros::recuperarTop(6);
$librosRecientes = array();
$librosRecientes = GestionLibros::recuperarUltimos(6);
?>

<!DOCTYPE html>
<html>
    <head>
        <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 'admin') { ?>
            <title>Inicio - Panel de administración de BookWorm</title>
        <?php } else { ?>
            <title>BookWorm - Inicio</title>
        <?php } ?>
        <?php require_once 'parts/head.php'; ?>
        <script type="text/javascript" src="<?php echo $raiz; ?>/js/login-validation.js"></script>
    </head>
    <body>
        <div class="container">
            <?php $_SESSION["page"] = "index.php" ?>
            <?php require_once 'parts/menu.php'; ?>
            <?php require_once 'parts/carousel.php'; ?>
            <?php require_once 'parts/infoRow.php'; ?>

            <div class="row">
                <!-- MAIN CONTENT -->
                <div class="span9">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#1" data-toggle="tab">Sobre BookWorm</a></li>
                            <li><a href="#2" data-toggle="tab">Libros recientes</a></li>
                            <li><a href="#3" data-toggle="tab">Libros más populares</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="1">
                                <?php if (isset($_SESSION["bienvenido"])) { ?>
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <?php echo "<p><strong>" . $_SESSION["bienvenido"] . "</strong></p>";
                                        session_unregister("bienvenido");
                                        ?>
                                    </div>
                                <?php } ?>
                                <p>
                                <legend>BookWorm</legend>
                                BookWorm es un lugar de encuentro para amantes de la lectura, que he desarrollado
                                para el Proyecto de Integración del C.F.G.S. de Desarrollo de Aplicaciones Informáticas, 
                                tomando como inspiración la red social <a href="http://www.anobii.com/" target="_blank">aNobii</a>.
                                </p>
                                <p>
                                    En BookWorm podrás:
                                </p>
                                <ul>
                                    <li>Crear tu propia estantería con tus libros.</li>
                                    <li>Añadir comentarios y valoraciones a tus libros.</li>
                                    <li>Invitar a tus amigos a unirse a BookWorm.</li>
                                    <li>Estar al día de las lecturas de tus amigos.</li>
                                    <li>Y mucho más...</li>
                                </ul>
                                <p>
                                El fin de BookWorm es totalmente <strong>lectivo</strong>, por lo que animo a todos los usuarios 
                                que quieran a que <a href="<?php echo $raiz; ?>/contact.php">contacten</a> y me cuenten su experiencia en la web, 
                                qué cosas cambiarían, si creen que algo se puede mejorar (sí, soy consciente de que son muchas 
                                las cosas a mejorar, pero siempre está bien oírlas de boca de otros).
                                </p>

                                <p>
                                <legend>Mi cuaderno de programación</legend>
                                En <a href="http://cuaderno-de-programacion.blogspot.com/" target="_blank">"Mi cuaderno de programación"</a> iré colgando periódicamente 
                                gran parte del código de BookWorm, además de subirlo a Bitbucket, donde haré el proyecto público cuando 
                                esté (casi) finalizado.
                                </p>
                                
                                <p>
                                <em><a href="http://cuaderno-de-programacion.blogspot.com/" target="_blank">"Mi cuaderno de programación"</a> es mi blog de programación, tal como su 
                                nombre indica es una especie de cuaderno en el que apunto todo lo que voy aprendiendo, 
                                dejo ejemplos, enlaces a sitios interesantes y demás. Por las distintas páginas de BookWorm dejaré enlaces
                                a las entradas que tengan el código correspondiente.</em>
                                </p>
                                
                                <p>
                                <legend>Recursos</legend>
                                A continuación dejo un listado con los enlaces de los diferentes recursos empleados en 
                                el desarrollo de esta web:
                                </p>
                                <dl>
                                    <dt>Estilo</dt>
                                    <dd>Para el estilo de la web me he basado en <a href="http://twitter.github.com/bootstrap/index.html" target="_blank">Bootstrap</a>, 
                                        un conjunto de plantillas CSS y plugins jQuery ofrecido por Twitter y mantenido por la comunidad. Posteriormente he modificado 
                                        el estilo para ajustarme al esquema de color que quería y darle un aire más personal.</dd>
                                    <dt>Color</dt>
                                    <dd>En <a href="http://kuler.adobe.com" target="_blank">kuler</a> puedes encontrar miles de esquemas de colores, que te ayudarán 
                                        a dar a tu web, blog o incluso cualquier trabajo, un estilo más definido. Para BookWorm he elegido el esquema 
                                        <strong>Playskool</strong> de <em>nicebarajas</em>.</dd>
                                    <dt>Iconos</dt>
                                    <dd>Calendario (formulario de registro y de creación de usuarios). <a href="http://www.iconfinder.com/icondetails/35799/32/calendar_event_icon" target="_blank">Calendar by FatCow Web Hosting.</a></dd>
                                    <dd>Facebook (perfil de los usuarios). <a href="http://www.iconfinder.com/icondetails/49061/128/facebook_sticker_icon" target="_blank">Facebook, Sticker icon by Creative Nerds.</a></dd>
                                    <dt>Plugins jQuery</dt>
                                    <dd><a href="http://datatables.net/" target="_blank">DataTables</a>. Fantástico para mostrar datos tabulados.</dd>
                                    <dd><a href="https://github.com/akzhan/jwysiwyg" target="_blank">jWysiwyg</a>. Editor de texto.</dd>
                                    <dd><a href="http://simplythebest.net/scripts/ajax/ajax_password_strength.html" target="_blank">jQuery password strength meter</a>. Medidor de fortaleza de las contraseñas.</dd>
                                    <dd><a href="http://addyosmani.github.com/jquery-ui-bootstrap/" target="_blank">jQuery UI Bootstrap</a>. jQuery UI + plantilla para adaptarlo al estilo de Bootstrap.</dd>
                                    <dd><a href="http://www.fyneworks.com/jquery/star-rating/" target="_blank">Fyneworks Star-Rating Plugin</a>. Para las valoraciones de los libros.</dd>
                                    <dd><a href="http://twitter.github.com/bootstrap/index.html" target="_blank">Bootstrap</a>. Tal como comente un poco más arriba, también he utilizado algunos de los plugins que incorpora Bootstrap 
                                        (carrusel, autocompletado en formularios, ventanas modales...)</dd>
                                    <dt>Librerías PHP</dt>
                                    <dd><a href="http://swiftmailer.org/" target="_blank">Swift Mailer</a>. Potente librería PHP para el envío de correo electrónico.</dd>
                                </dl>
                            </div>
                            <div class="tab-pane" id="2">
                                <?php if (count($librosRecientes) > 0) { 
                                    echo "<div>";
                                    for ($i = 0; $i < count($librosRecientes); $i++) {
                                        $l = new Libro();
                                        $l = $librosRecientes[$i];
                                        $autores = array();
                                        $autores = $l->getAutores(); ?>
                                        <div style="margin: 10px; float: left; width: 320px;">
                                            <legend style="margin-bottom: 10px;"><a href="book_details.php?isbn=<?php echo $l->getIsbn(); ?>"><?php echo $l->getTitulo(); ?></a></legend>
                                            <div><?php if ($l->getValoracionMedia() != null) { 
                                                for ($k = 1; $k < 11; $k++) { ?>
                                                    <input name="star<?php echo $i; ?>" value="<?php echo $k; ?>" type="radio" class="star" disabled="disabled" <?php echo intval($l->getValoracionMedia()) == $k ? "checked='checked'" : "" ?>/>
                                                <?php }
                                            } else { ?>
                                                    <em>Todavía no ha sido valorado.</em>
                                            <?php }?><br /></div>
                                            <div style="float: left; border: 1px solid #D90416; padding: 2px;">
                                                <a href="book_details.php?isbn=<?php echo $l->getIsbn(); ?>"><img src="<?php echo $raiz; ?>/img/portadas/<?php echo $l->getPortada(); ?>" alt="" width="150px" /></a>
                                            </div>
                                            <div style="float: left; max-width: 170px; overflow: hidden;">
                                                <dl style="padding: 5px;">
                                                    <dt>ISBN</dt>
                                                    <dd><?php echo $l->getIsbn(); ?></dd>
                                                    <?php if (count($autores) == 1) { ?>
                                                        <dt>Autor</dt>
                                                        <?php 
                                                            $a = new Autor();
                                                            $a = $autores[0];
                                                        ?>
                                                        <dd><?php echo $a->getNombre(); ?></dd>
                                                    <?php } else { ?>
                                                        <dt>Autores</dt>
                                                        <?php for ($j = 0; $j < count($autores); $j++) { 
                                                            $a = new Autor();
                                                            $a = $autores[$j]; ?>
                                                            <dd><?php echo $a->getNombre(); ?></dd>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    
                                                </dl>
                                            </div>
                                        </div>
                                        <?php echo $i % 2 != 0 ? "</div><div style= 'clear: both;'>" : "" ?>
                                    <?php }
                                    echo "</div>";
                                } else { ?>
                                    <div class="alert alert-error">
                                        <p><strong>Parece que todavía no hay libros en BookWorm.</strong></p>
                                        <p>¡<a href="<?php echo $raiz; ?>/register.php">Únete</a> y ayúdanos a llenar nuestra biblioteca!</p>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="tab-pane" id="3">
                                <?php if (count($librosPopulares) > 0) { 
                                    echo "<div>";
                                    for ($i = 0; $i < count($librosPopulares); $i++) {
                                        $l = new Libro();
                                        $l = $librosPopulares[$i];
                                        $autores = array();
                                        $autores = $l->getAutores(); ?>
                                        <div style="margin: 10px; float: left; width: 320px;">
                                            <legend style="margin-bottom: 10px;"><a href="book_details.php?isbn=<?php echo $l->getIsbn(); ?>"><?php echo $l->getTitulo(); ?></a></legend>
                                            <div><?php if ($l->getValoracionMedia() != null) { 
                                                for ($k = 1; $k < 11; $k++) { ?>
                                                    <input name="star2<?php echo $i; ?>" value="<?php echo $k; ?>" type="radio" class="star" disabled="disabled" <?php echo intval($l->getValoracionMedia()) == $k ? "checked='checked'" : "" ?>/>
                                                <?php }
                                            } ?><br /></div>
                                            <div style="float: left; border: 1px solid #D90416; padding: 2px;">
                                                <a href="book_details.php?isbn=<?php echo $l->getIsbn(); ?>"><img src="<?php echo $raiz; ?>/img/portadas/<?php echo $l->getPortada(); ?>" alt="" width="150px" /></a>
                                            </div>
                                            <div style="float: left; max-width: 170px; overflow: hidden;">
                                                <dl style="padding: 5px;">
                                                    <dt>ISBN</dt>
                                                    <dd><?php echo $l->getIsbn(); ?></dd>
                                                    <?php if (count($autores) == 1) { ?>
                                                        <dt>Autor</dt>
                                                        <?php 
                                                            $a = new Autor();
                                                            $a = $autores[0];
                                                        ?>
                                                        <dd><?php echo $a->getNombre(); ?></dd>
                                                    <?php } else { ?>
                                                        <dt>Autores</dt>
                                                        <?php for ($j = 0; $j < count($autores); $j++) { 
                                                            $a = new Autor();
                                                            $a = $autores[$j]; ?>
                                                            <dd><?php echo $a->getNombre(); ?></dd>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    
                                                </dl>
                                            </div>
                                        </div>
                                        <?php echo $i % 2 != 0 ? "</div><div style= 'clear: both;'>" : "" ?>
                                    <?php }
                                    echo "</div>";
                                } else { ?>
                                    <div class="alert alert-error">
                                        <p><strong>Parece que nadie ha valorado nuestros libros.</strong></p>
                                        <p>¡<a href="<?php echo $raiz; ?>/register.php">Únete</a> a BookWorm y ayúdanos a llenar nuestra biblioteca!</p>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SIDEBAR -->
                <?php require_once 'parts/sidebar.php'; ?>
            </div>

            <?php require_once 'parts/footer.php'; ?>
        </div>
    </body>
</html>
